#pragma once
#include "main_dataType.h"

const si32 nQ2D = 9;
const si32 nQ3D = 19;

struct Cell3D
{
	fl64 f[nQ3D];
};