#pragma once
#include <iostream>
#include <iomanip>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#define APIPREFIX __host__ __device__ 

using namespace std;
class flfl
{
	APIPREFIX void two_sum_quick(const float x, const float y,float ret[2]) const
	{
		ret[0] = x + y;
		ret[1] = y - (ret[0] - x);
	}
	APIPREFIX void two_sum(const float x, const float y, float ret[2]) const
	{
		ret[0] = x + y;
		float t = ret[0] - x;
		ret[1] = (x - (ret[0] - t)) + (y - t);
	}
	APIPREFIX void two_difference(const float x, const float y, float ret[2]) const
	{
		ret[0] = x - y;
		float t = ret[0] - x;
		ret[1] = (x - (ret[0] - t)) - (y + t);
	}
	APIPREFIX void two_product(const float x, const float y, float ret[2]) const
	{
		float u = x * 8193.0;
		float v = y * 8193.0;

		float s = u - (u - x);
		float t = v - (v - y);
		float f = x - s;
		float g = y - t;
		float r = x * y;
		float e = ((s * t - r) + s * g + f * t) + f * g;

		ret[0] = r;
		ret[1] = e;
	}

public:
	float v[2];

	APIPREFIX flfl() : v{ 0, 0 }
	{
	}
	APIPREFIX flfl(const float& t) :v{ t, 0 }
	{
	}
	APIPREFIX flfl& operator=(const flfl& rhs)
	{
		v[0] = rhs.v[0];
		v[1] = rhs.v[1];
		return *this;
	}
	APIPREFIX flfl operator+(const flfl& rhs) const
	{
		flfl ret;
		two_sum(v[0], rhs.v[0], ret.v);
		ret.v[1] += v[1] + rhs.v[1];
		two_sum_quick(ret.v[0], ret.v[1], ret.v);
		return ret;
	}
	APIPREFIX flfl operator-(const flfl& rhs) const
	{
		flfl ret;
		two_difference(v[0], rhs.v[0], ret.v);
		ret.v[1] += v[1] - rhs.v[1];
		two_sum_quick(ret.v[0], ret.v[1], ret.v);
		return ret;
	}
	APIPREFIX flfl operator*(const flfl& rhs) const
	{
		flfl ret;
		two_product(v[0], rhs.v[0], ret.v);
		ret.v[1] += v[0] * rhs.v[1] + v[1] * rhs.v[0];
		two_sum_quick(ret.v[0], ret.v[1], ret.v);
		return ret;
	}
	APIPREFIX flfl operator/(const flfl& rhs) const
	{
		flfl ret;
		ret.v[0] = v[0] / rhs.v[0];

		flfl tmp;
		two_product(ret.v[0], rhs.v[0], tmp.v);
		ret.v[1] = (v[0] - tmp.v[0] - tmp.v[1] + v[1] - ret.v[0] * rhs.v[1]) / rhs.v[0];
		two_sum_quick(ret.v[0], ret.v[1], ret.v);
		return ret;
	}

	APIPREFIX flfl& operator+=(const flfl& rhs)
	{
		*this = *this + rhs;
		return *this;
	}
	APIPREFIX flfl& operator-=(const flfl& rhs)
	{
		*this = *this - rhs;
		return *this;
	}
	APIPREFIX flfl& operator*=(const flfl& rhs)
	{
		*this = *this * rhs;
		return *this;
	}
	APIPREFIX flfl& operator/=(const flfl& rhs)
	{
		*this = *this / rhs;
		return *this;
	}

	APIPREFIX bool operator==(const flfl& rhs)
	{
		return (v[0] == rhs.v[0] && v[1] == rhs.v[1]);
	}
	
	APIPREFIX bool operator!=(const flfl& rhs)
	{
		return !(*this == rhs);
	}

	APIPREFIX friend ostream& operator<< (ostream& os, const flfl& val)
	{
		os << "(" << setw(10) << val.v[0] << setw(10) << val.v[1] << ")";
		return os;
	}
};
