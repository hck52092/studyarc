#include "DataSet3D.h"
#include <cuda_runtime_api.h>
DataSet3D::DataSet3D(const si32 _nNode, const si32 _threadsPerBlock, const fl64 _s_v, const si32 _nCore) :
	nNode(_nNode), 
	threadsPerBlock(_threadsPerBlock), blocksPerGrid((nNode + threadsPerBlock - 1) / threadsPerBlock),
	s_v(_s_v),
	nCore(_nCore)
{
	dataCPU_AoS.resize(nNode);
	dataCPU_SoA.resize(19);
	for (si32 d = 0; d < 19; ++d)
	{
		dataCPU_SoA[d].resize(nNode);
	}
	buffer_CPU.resize(nNode);

	const fl64  w_rho[3] = {
							12.0 / 36,
							2.0 / 36,
							1.0 / 36 };

	Cell3D base;
	base.f[0] = w_rho[0];

	base.f[1] = w_rho[1];	base.f[2] = w_rho[1];
	base.f[3] = w_rho[1];	base.f[4] = w_rho[1];
	base.f[5] = w_rho[1];	base.f[6] = w_rho[1];

	base.f[7] = w_rho[2];	base.f[8] = w_rho[2];	base.f[9] = w_rho[2];	base.f[10] = w_rho[2];
	base.f[11] = w_rho[2];	base.f[12] = w_rho[2];	base.f[13] = w_rho[2];	base.f[14] = w_rho[2];
	base.f[15] = w_rho[2];	base.f[16] = w_rho[2];	base.f[17] = w_rho[2];	base.f[18] = w_rho[2];
	
	for (si32 i = 0 ; i < nNode; ++i)
	{
		dataCPU_AoS[i] = base;

		for (si32 d = 0; d < 19; ++d)
		{
			dataCPU_SoA[d][i] = base.f[d];
		}		
	}

	cudaMalloc((void**)&dataGPU_AoS, nNode * sizeof(Cell3D));
	cudaMalloc((void**)&buffer_GPU, nNode * sizeof(fl64));
	dataGPU_SoA.resize(19);
	for (si32 d = 0; d < 19; ++d)
	{
		cudaMalloc((void**)&dataGPU_SoA[d], nNode * sizeof(fl64));
	}

	cudaMemcpy(dataGPU_AoS, dataCPU_AoS.data(), nNode * sizeof(Cell3D), cudaMemcpyHostToDevice);
	for (si32 d = 0; d < 19; ++d)
	{
		cudaMemcpy(dataGPU_SoA[d], dataCPU_SoA[d].data(), nNode * sizeof(fl64), cudaMemcpyHostToDevice);
	}
}
DataSet3D::~DataSet3D()
{
	cudaFree(dataGPU_AoS);
	cudaFree(buffer_GPU);
	for (si32 d = 0; d < 19; ++d)
	{
		cudaFree(dataGPU_SoA[d]);
	}
}