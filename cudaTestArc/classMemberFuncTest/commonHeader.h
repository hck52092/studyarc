#pragma once
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <array>
#include <tuple>
#include <algorithm>
#include <chrono>

using namespace std;

using ui08 = unsigned char;
using si32 = int;
using si64 = long long;
using fl32 = float;
using fl64 = double;