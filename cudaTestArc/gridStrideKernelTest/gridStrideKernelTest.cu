#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <array>
#include <tuple>
#include <algorithm>
#include <chrono>

using namespace std;

using ui08 = unsigned char;
using si32 = int;
using si64 = long long;
using fl32 = float;
using fl64 = double;

template<typename T>
__device__ T calcFunc(const T tmp)
{   
    //T res = tmp;
    //res *= tmp;
    //res += tmp;
    //res /= tmp;
    //res -= tmp;    
    //res *= tmp;

    T res = tmp;

    for (si32 s = 0; s < 512; ++s)
    {
        res *= tmp;
        res += tmp;
        res /= tmp;
        res -= tmp;    
        res *= tmp;
    }

    return res;
}
template<typename T>
__global__ void saxpy1D(const si32 n, const T a, const T* src, T* dst)
{
    si32 i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < n)
    {   
        dst[i] = a * calcFunc(src[i]);
    }
}

template<typename T>
__global__ void saxpy2D(const si32 n, const T a, const T* src, T* dst)
{
    si32 i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < n)
    {
        dst[i] = a * calcFunc(src[i]);
    }
}

template<typename T>
__global__ void saxpyGrid(const si32 n, const T a, const T* src, T* dst)
{
    const si32 offset = blockDim.x * gridDim.x;

    for (si32 i = blockIdx.x * blockDim.x + threadIdx.x; i < n; i += offset)
    {
        dst[i] = a * calcFunc(src[i]);
    }
}

using ResultItem = tuple<fl64, fl64, bool>;

template<typename T>
ResultItem execFuncTest(decltype(saxpy1D<T>) funcPtr,
    const si32 iterMax, const si32 totalLen, const si32 nThread,
    const si32 nBlock, const si32 kernelThread,
    const T multiflier,
    const vector<T>& src_h, vector<T>& dst_h,
    const T* src_d, T* dst_d)
{    
    chrono::system_clock::time_point start, end;

    fill(dst_h.begin(), dst_h.end(), T(0));

    //for warm-up
    for (si32 iter = 0; iter < iterMax; ++iter)
    {
        funcPtr << <nBlock, kernelThread >> > (totalLen, multiflier, src_d, dst_d);
    }
    cudaDeviceSynchronize();

    start = chrono::system_clock::now();
    for (si32 iter = 0; iter < iterMax; ++iter)
    {
        funcPtr << <nBlock, kernelThread >> > (totalLen, multiflier, src_d, dst_d);        
    }
    cudaDeviceSynchronize();
    end = chrono::system_clock::now();

    cudaMemcpy(dst_h.data(), dst_d, totalLen * sizeof(T), cudaMemcpyDeviceToHost);

    bool isValid = true;
    for (si32 i = 0; i < totalLen; ++i)
    {
        if (dst_h[i] != src_h[i] * multiflier)
        {
            cout 
                <<"ERROR : "
                << setw(20) << src_h[i]
                << setw(20) << multiflier
                << setw(20) << multiflier * src_h[i]
                << setw(20) << dst_h[i]
                << endl;
            isValid = false;
            break;
        }
    }

    auto timeElap = chrono::duration_cast<chrono::nanoseconds>(end - start).count() / 10e9;

    return make_tuple(timeElap, timeElap / iterMax, isValid);
}

template<typename T>
vector<array<ResultItem, 3>> execSettingItemTest(const si32 dx, const si32 dy,const si32 iterMax, const si32 numSMs, const si32 warpSize, const T multiflier)
{
    const si32 nTMax = 11;
    const si32 totalLen = dx * dy;
    vector<T> src_h(totalLen);
    vector<T> out_h(totalLen);

    vector<array<ResultItem, 3>> ret(nTMax);

    T* src_d;
    T* out_d;

    cudaMalloc(&src_d, sizeof(T) * totalLen);
    cudaMalloc(&out_d, sizeof(T) * totalLen);

    for (si32 i = 0; i < totalLen; ++i)
    {
        src_h[i] = i % 4096 + 1;
    }

    cudaMemcpy(src_d, src_h.data(), sizeof(T) * totalLen, cudaMemcpyHostToDevice);

    for (si32 nT = 0; nT < nTMax; ++nT)
    {
        si32 nThread = pow(2, nT);

        ret[nT][0] = execFuncTest(saxpy1D<T>, iterMax, totalLen, nThread,
            getBlockSize(totalLen, nThread), nThread,
            multiflier,
            src_h, out_h, src_d, out_d);

        ret[nT][1] = execFuncTest(saxpy2D<T>, iterMax, totalLen, nThread,
            dy, dx,
            multiflier,
            src_h, out_h, src_d, out_d);

        ret[nT][2] = execFuncTest(saxpyGrid<T>, iterMax, totalLen, nThread,
            warpSize * numSMs, dx,
            multiflier,
            src_h, out_h, src_d, out_d);
    }

    cudaFree(src_d);
    cudaFree(out_d);

    return ret;
}

template<typename T>
void execTypeTest(const string& typeName,const si32 powMax, const si32 iterMax, const si32 numSMs, const si32 warpSize, const T multiflier)
{
    cout << typeName << endl;
    for (si32 s = 0; s <= powMax; ++s)
    {
        const si32 dx = pow(2, s);
        const si32 dy = pow(2, powMax - s);

        if (dx > 1024)
        {
            continue;
        }

        auto ret = execSettingItemTest<T>(dx, dy, iterMax, numSMs, warpSize, multiflier);

        for (si32 n = 0; n < ret.size(); ++n)
        {
            cout << setw(15) << dx << setw(15) << dy
                << setw(15) << si32(pow(2, n))
                << setw(20) << get<0>(ret[n][0])
                << setw(20) << get<0>(ret[n][1])
                << setw(20) << get<0>(ret[n][2])
                << setw(10) << get<2>(ret[n][0])
                << setw(10) << get<2>(ret[n][1])
                << setw(10) << get<2>(ret[n][2])
                << setw(40) << 1.0 * dx * dy / get<1>(ret[n][0]) / 10e9
                << setw(40) << 1.0 * dx * dy / get<1>(ret[n][1]) / 10e9
                << setw(40) << 1.0 * dx * dy / get<1>(ret[n][2]) / 10e9
                << endl;
        }
    }
}

si32 getBlockSize(const si32 totalSize,const si32 stepSize)
{
    return (totalSize + stepSize - 1) / stepSize;
}

int main()
{
    cout << boolalpha;
    cout << fixed;
    cout << setprecision(12); 
     
    si32 nDevices;
    cudaGetDeviceCount(&nDevices);

    cout << "num Devices = " << nDevices << endl;

    si32 devId = nDevices - 1;
    cudaSetDevice(devId);

    si32 numSMs;
    si32 warpSize;    
    cudaDeviceGetAttribute(&numSMs, cudaDevAttrMultiProcessorCount, devId);
    cudaDeviceGetAttribute(&warpSize, cudaDevAttrWarpSize, devId);

    cout << "numSMs = " << numSMs << endl;
    cout << "warpSz = " << warpSize << endl;

    const si32 iterMax = 1;
    const si32 powMax = 24;

    {
        const si32 multiflier = 3;
        execTypeTest("si32", powMax, iterMax, numSMs, warpSize, multiflier);
    }
    {
        const si64 multiflier = 3;
        execTypeTest("si64", powMax, iterMax, numSMs, warpSize, multiflier);
    }
    {
        const fl32 multiflier = 3;
        execTypeTest("fl32", powMax, iterMax, numSMs, warpSize, multiflier);
    }
    {
        const fl64 multiflier = 3;
        execTypeTest("fl64", powMax, iterMax, numSMs, warpSize, multiflier);
    }
}