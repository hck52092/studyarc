#include "baseHeader.h"
tuple<fl64, fl64> baseTest(const si32 nCore, const si32 nThread, const si32 testLen, const si32 iterMax)
{
	const si32 padding = 10;

	const si64 checkSum = si64(testLen) * (si64(testLen) - 1) / 2;
		
	chrono::system_clock::time_point start, end;
	chrono::microseconds microSec;

	vector<si64> sumArr(nCore * padding, 0);

	start = chrono::system_clock::now();

	for (si32 iter = 0; iter < iterMax; ++iter)
	{
		#pragma omp parallel for schedule(guided) num_threads(nCore)
		for (si64 i = 0; i < testLen; ++i)
		{
			si32 th = omp_get_thread_num();

			sumArr[th * padding] += i;
		}

		for (si32 th = 1; th < nCore; ++th)
		{
			sumArr[0] += sumArr[th * padding];
			sumArr[th * padding] = 0;
		}
	}
	end = chrono::system_clock::now();
	microSec = chrono::duration_cast<chrono::microseconds>(end - start);

	if (checkSum * iterMax != sumArr[0])
	{
		cerr << "baseTest Failed" << endl;
		cerr << checkSum*iterMax << endl;
		cerr << sumArr[0] << endl;

	}

	return make_tuple(microSec.count() / 1000000.0, microSec.count() / 1000000.0 / iterMax);
}