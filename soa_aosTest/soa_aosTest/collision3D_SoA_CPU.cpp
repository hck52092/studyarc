#include "DataSet3D.h"
fl64 genRhoSoA_CPU(const vector<vector<fl64>>& dataCPU_SoA, const si32 ind)
{
	fl64 rho = 0;

	rho += dataCPU_SoA[0][ind];
	rho += dataCPU_SoA[1][ind];
	rho += dataCPU_SoA[2][ind];
	rho += dataCPU_SoA[3][ind];
	rho += dataCPU_SoA[4][ind];
	rho += dataCPU_SoA[5][ind];
	rho += dataCPU_SoA[6][ind];
	rho += dataCPU_SoA[7][ind];
	rho += dataCPU_SoA[8][ind];
	rho += dataCPU_SoA[9][ind];
	rho += dataCPU_SoA[10][ind];
	rho += dataCPU_SoA[10][ind];
	rho += dataCPU_SoA[11][ind];
	rho += dataCPU_SoA[12][ind];
	rho += dataCPU_SoA[13][ind];
	rho += dataCPU_SoA[14][ind];
	rho += dataCPU_SoA[15][ind];
	rho += dataCPU_SoA[16][ind];
	rho += dataCPU_SoA[17][ind];

	return rho;
}
fl64 genUSoA_CPU(const vector<vector<fl64>>& dataCPU_SoA, const si32 ind, const fl64 rho)
{
	fl64 u = 0;
	u += (dataCPU_SoA[1][ind] + dataCPU_SoA[7][ind] + dataCPU_SoA[9][ind] + dataCPU_SoA[11][ind] + dataCPU_SoA[3][ind]);
	u -= (dataCPU_SoA[2][ind] + dataCPU_SoA[8][ind] + dataCPU_SoA[10][ind] + dataCPU_SoA[12][ind] + dataCPU_SoA[14][ind]);
	u /= rho;
	return u;
}
fl64 genVSoA_CPU(const vector<vector<fl64>>& dataCPU_SoA, const si32 ind, const fl64 rho)
{
	fl64 v = 0;
	v += (dataCPU_SoA[3][ind] + dataCPU_SoA[7][ind] + dataCPU_SoA[8][ind] + dataCPU_SoA[15][ind] + dataCPU_SoA[17][ind]);
	v -= (dataCPU_SoA[4][ind] + dataCPU_SoA[9][ind] + dataCPU_SoA[10][ind] + dataCPU_SoA[16][ind] + dataCPU_SoA[18][ind]);
	v /= rho;
	return v;
}
fl64 genWSoA_CPU(const vector<vector<fl64>>& dataCPU_SoA, const si32 ind, const fl64 rho)
{
	fl64 w = 0;
	w += (dataCPU_SoA[5][ind] + dataCPU_SoA[11][ind] + dataCPU_SoA[12][ind] + dataCPU_SoA[15][ind] + dataCPU_SoA[16][ind]);
	w -= (dataCPU_SoA[6][ind] + dataCPU_SoA[13][ind] + dataCPU_SoA[14][ind] + dataCPU_SoA[17][ind] + dataCPU_SoA[18][ind]);
	w /= rho;
	return w;
}

void DataSet3D::collision3D_SoA_CPU()
{
	const fl64  w_rho[3] = {
				12.0 / 36,
				2.0 / 36,
				1.0 / 36 };

	const si32 direcCategory[19] = { 0,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2 };

	const fl64 direcC[19][3] = {
		{0.0,0.0,0.0},
		{1.0,0.0,0.0 },
		{-1.0, 0.0,0.0},
		{0.0,1.0,0.0},
		{0.0,-1.0,0.0 },
		{0.0, 0.0,1.0},
		{0.0, 0.0,-1.0},

		{1.0, 1.0,0.0 },
		{-1.0,1.0,0.0 },
		{1.0,-1.0,0.0 },
		{-1.0,-1.0,0.0 },

		{1.0,0.0,1.0 },
		{-1.0,0.0,1.0 },
		{1.0,0.0,-1.0 },
		{-1.0,0.0,-1.0 },

		{0.0,1.0,1.0 },
		{0.0,-1.0,1.0 },
		{0.0,1.0,-1.0 },
		{0.0,-1.0,-1.0 }
	};

	const fl64 csSq = 1.0 / 3;

#pragma omp parallel for schedule(guided) num_threads(nCore)
	for (si32 indCur = 0; indCur < nNode; ++indCur)
	{
		const si32 indOffset = indCur * 19;

		const fl64 rho = genRhoSoA_CPU(dataCPU_SoA, indCur);

		fl64 vel[3] = { genUSoA_CPU(dataCPU_SoA, indCur,rho), genVSoA_CPU(dataCPU_SoA, indCur,rho), genWSoA_CPU(dataCPU_SoA, indCur,rho) };

		const fl64 rhoRecipocal = 1.0 / rho;

		fl64 u_dot_u = vel[0] * vel[0] + vel[1] * vel[1] + vel[2] * vel[2];

		for (si32 i = 0; i < 19; ++i)
		{
			const fl64 wTemp = w_rho[direcCategory[i]];
			const fl64 c[3] = { direcC[i][0],direcC[i][1],direcC[i][2] };
			const fl64 dotVal = (c[0] * vel[0] + c[1] * vel[1] + c[2] * vel[2]);

			const fl64 eq = wTemp * rho * (1. + 3.0 * dotVal + 4.5 * (dotVal * dotVal) - 1.5 * u_dot_u);

			dataCPU_SoA[i][indCur] = (1 - s_v) * eq + s_v * dataCPU_SoA[i][indCur];
		}
	}
}
