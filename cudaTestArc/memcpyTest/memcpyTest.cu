#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <iostream>
#include <iomanip>
#include <vector>
#include <chrono>
#include <string>

using namespace std;

using si32 = long;
using fl64 = double;

__global__ void copyKernel1D(fl64* dst, const fl64* src, const int2 dimFull, const int2 dimData, const int2 offset, const si32 nTotal)
{
    const si32 indCur = blockDim.x * blockIdx.x + threadIdx.x;

    if (indCur < nTotal)
    {
        const si32 i = indCur % dimData.x + offset.x;
        const si32 j = indCur / dimData.x + offset.y;
        const si32 indFull = i + dimFull.x * j;

        dst[indFull] = src[indFull];
    }
}
__global__ void copyKernel2D(fl64* dst, const fl64* src, const int2 dimFull,const int2 offset, const si32 nX, const si32 nY)
{
    const si32 i = blockDim.x * blockIdx.x + threadIdx.x;
    const si32 j = blockDim.y * blockIdx.y + threadIdx.y;

    if (i < nX && j < nY)
    {
        const si32 indFull = i + offset.x + dimFull.x * (j + offset.y);

        dst[indFull] = src[indFull];
    }
}

void testHToD(const si32 nX, const si32 nY, const si32 iterMax)
{
    cout << setw(30) << "testHToD";
    vector<fl64> src(nX * nY);
    vector<fl64> dst(nX * nY);

    fl64 sumSrc = 0;
    for (si32 s = 0; s < nX * nY; ++s)
    {
        src[s] = s;
        sumSrc += src[s];
    }

    fl64* src_dev;
    fl64* dst_dev;

    cudaMalloc(&src_dev, sizeof(fl64) * nX * nY);
    cudaMalloc(&dst_dev, sizeof(fl64) * nX * nY);

    cudaMemcpy(src_dev, src.data(), sizeof(fl64) * nX * nY, cudaMemcpyHostToDevice);

    chrono::system_clock::time_point start = chrono::system_clock::now();
    chrono::system_clock::time_point end;
    chrono::microseconds microSec;

    for (si32 iter = 0; iter < iterMax; ++iter)
    {
        for (si32 j = 0; j < nY; ++j)
        {
            //host to dev
            cudaMemcpy(src_dev + j * nX, src.data() + j * nX, sizeof(fl64) * nX, cudaMemcpyHostToDevice);
            cudaDeviceSynchronize();
        }
    }

    end = chrono::system_clock::now();
    microSec = chrono::duration_cast<chrono::microseconds>(end - start);

    cudaMemcpy(dst.data(), dst_dev, sizeof(fl64) * nX * nY, cudaMemcpyDeviceToHost);

    fl64 sumDst = 0;
    for (si32 s = 0; s < nX * nY; ++s)
    {
        sumDst += dst[s];
    }

    cout << " calc time elap : " << setw(12) << (microSec.count() / 1000000.0);
    cout << " | iter per sec : " << setw(10) << iterMax / (microSec.count() / 1000000.0);
    cout << endl;

    cudaFree(src_dev);
    cudaFree(dst_dev);
}
void testDToD(const si32 nX, const si32 nY, const si32 iterMax)
{
    cout << setw(30) << "testDToD";
    vector<fl64> src(nX * nY);
    vector<fl64> dst(nX * nY);

    fl64 sumSrc = 0;
    for (si32 s = 0; s < nX * nY; ++s)
    {
        src[s] = s;
        sumSrc += src[s];
    }

    fl64* src_dev;
    fl64* dst_dev;

    cudaMalloc(&src_dev, sizeof(fl64) * nX * nY);
    cudaMalloc(&dst_dev, sizeof(fl64) * nX * nY);

    cudaMemcpy(src_dev, src.data(), sizeof(fl64) * nX * nY, cudaMemcpyHostToDevice);

    chrono::system_clock::time_point start = chrono::system_clock::now();
    chrono::system_clock::time_point end;
    chrono::microseconds microSec;

    for (si32 iter = 0; iter < iterMax; ++iter)
    {
        for (si32 j = 0; j < nY; ++j)
        {
            //dev to dev (self)
            cudaMemcpy(dst_dev + j * nX, src_dev + j * nX, sizeof(fl64) * nX, cudaMemcpyDeviceToDevice);
            cudaDeviceSynchronize();
        }
    }

    end = chrono::system_clock::now();
    microSec = chrono::duration_cast<chrono::microseconds>(end - start);

    cudaMemcpy(dst.data(), dst_dev, sizeof(fl64) * nX * nY, cudaMemcpyDeviceToHost);

    fl64 sumDst = 0;
    for (si32 s = 0; s < nX * nY; ++s)
    {
        sumDst += dst[s];
    }

    cout << " calc time elap : " << setw(12) << (microSec.count() / 1000000.0);
    cout << " | iter per sec : " << setw(10) << iterMax / (microSec.count() / 1000000.0);
    cout << endl;

    cudaFree(src_dev);
    cudaFree(dst_dev);
}
void testDToD_kernel1D(const si32 nX, const si32 nY, const si32 iterMax)
{
    vector<fl64> src(nX * nY);
    vector<fl64> dst(nX * nY);

    fl64 sumSrc = 0;
    for (si32 s = 0; s < nX * nY; ++s)
    {
        src[s] = s;
        sumSrc += src[s];
    }

    fl64* src_dev;
    fl64* dst_dev;

    cudaMalloc(&src_dev, sizeof(fl64) * nX * nY);
    cudaMalloc(&dst_dev, sizeof(fl64) * nX * nY);

    cudaMemcpy(src_dev, src.data(), sizeof(fl64) * nX * nY, cudaMemcpyHostToDevice);

    for (si32 nThread = 32; nThread <= 1024; nThread *= 2)
    {
        cout << setw(30) << ("testDToD1D_th_" + to_string(nThread));

        chrono::system_clock::time_point start = chrono::system_clock::now();
        chrono::system_clock::time_point end;
        chrono::microseconds microSec;

        const int2 dimFull{ nX,nY };
        const int2 dimData{ nX,nY };
        const int2 offset{ 0,0 };
        const si32 nBlock = (nX * nY + nThread - 1) / nThread;

        for (si32 iter = 0; iter < iterMax; ++iter)
        {
            //dev to dev (self)
            copyKernel1D << <nBlock, nThread >> > (dst_dev, src_dev, dimFull, dimData, offset, nX * nY);
            cudaDeviceSynchronize();
        }

        end = chrono::system_clock::now();
        microSec = chrono::duration_cast<chrono::microseconds>(end - start);

        cudaMemcpy(dst.data(), dst_dev, sizeof(fl64) * nX * nY, cudaMemcpyDeviceToHost);

        fl64 sumDst = 0;
        for (si32 s = 0; s < nX * nY; ++s)
        {
            sumDst += dst[s];
        }

        cout << " calc time elap : " << setw(12) << (microSec.count() / 1000000.0);
        cout << " | iter per sec : " << setw(10) << iterMax / (microSec.count() / 1000000.0);
        cout << endl;
    }

    cudaFree(src_dev);
    cudaFree(dst_dev);
}
void testDToD_kernel2D(const si32 nX, const si32 nY, const si32 iterMax)
{
    vector<fl64> src(nX * nY);
    vector<fl64> dst(nX * nY);

    fl64 sumSrc = 0;
    for (si32 s = 0; s < nX * nY; ++s)
    {
        src[s] = s;
        sumSrc += src[s];
    }

    fl64* src_dev;
    fl64* dst_dev;

    cudaMalloc(&src_dev, sizeof(fl64) * nX * nY);
    cudaMalloc(&dst_dev, sizeof(fl64) * nX * nY);

    cudaMemcpy(src_dev, src.data(), sizeof(fl64) * nX * nY, cudaMemcpyHostToDevice);

    for (si32 nThread = 4; nThread * nThread <= 1024; nThread *= 2)
    {
        cout << setw(30) << ("testDToD2D_th_" + to_string(nThread));

        chrono::system_clock::time_point start = chrono::system_clock::now();
        chrono::system_clock::time_point end;
        chrono::microseconds microSec;

        const int2 dimFull{ nX,nY };
        const int2 offset{ 0,0 };
        const dim3 threadDim( nThread,nThread,1 );
        const dim3 nBlock( (nX + nThread - 1) / nThread,(nY + nThread - 1) / nThread,1 );

        for (si32 iter = 0; iter < iterMax; ++iter)
        {
            //dev to dev (self)
            copyKernel2D << <nBlock, threadDim >> > (dst_dev, src_dev, dimFull, offset, nX, nY);
            cudaDeviceSynchronize();
        }

        end = chrono::system_clock::now();
        microSec = chrono::duration_cast<chrono::microseconds>(end - start);

        cudaMemcpy(dst.data(), dst_dev, sizeof(fl64) * nX * nY, cudaMemcpyDeviceToHost);

        fl64 sumDst = 0;
        for (si32 s = 0; s < nX * nY; ++s)
        {
            sumDst += dst[s];
        }

        cout << " calc time elap : " << setw(12) << (microSec.count() / 1000000.0);
        cout << " | iter per sec : " << setw(10) << iterMax / (microSec.count() / 1000000.0);
        cout << endl;
    }

    cudaFree(src_dev);
    cudaFree(dst_dev);
}
void testHToH(const si32 nX, const si32 nY, const si32 iterMax)
{
    cout << setw(30) << "testHToH";
    vector<fl64> src(nX * nY);
    vector<fl64> dst(nX * nY);

    fl64 sumSrc = 0;
    for (si32 s = 0; s < nX * nY; ++s)
    {
        src[s] = s;
        sumSrc += src[s];
    }

    fl64* src_dev;
    fl64* dst_dev;

    cudaMalloc(&src_dev, sizeof(fl64) * nX * nY);
    cudaMalloc(&dst_dev, sizeof(fl64) * nX * nY);

    cudaMemcpy(src_dev, src.data(), sizeof(fl64) * nX * nY, cudaMemcpyHostToDevice);

    chrono::system_clock::time_point start = chrono::system_clock::now();
    chrono::system_clock::time_point end;
    chrono::microseconds microSec;

    for (si32 iter = 0; iter < iterMax; ++iter)
    {
        for (si32 j = 0; j < nY; ++j)
        {
            //host to host
            cudaMemcpy(dst.data() + j * nX, src.data() + j * nX, sizeof(fl64) * nX, cudaMemcpyHostToHost);
            cudaDeviceSynchronize();
        }
    }

    end = chrono::system_clock::now();
    microSec = chrono::duration_cast<chrono::microseconds>(end - start);

    cudaMemcpy(dst.data(), dst_dev, sizeof(fl64) * nX * nY, cudaMemcpyDeviceToHost);

    fl64 sumDst = 0;
    for (si32 s = 0; s < nX * nY; ++s)
    {
        sumDst += dst[s];
    }

    cout << " calc time elap : " << setw(12) << (microSec.count() / 1000000.0);
    cout << " | iter per sec : " << setw(10) << iterMax / (microSec.count() / 1000000.0);
    cout << endl;

    cudaFree(src_dev);
    cudaFree(dst_dev);
}
void testDToH(const si32 nX, const si32 nY, const si32 iterMax)
{
    cout << setw(30) << "testDToH";
    vector<fl64> src(nX * nY);
    vector<fl64> dst(nX * nY);

    fl64 sumSrc = 0;
    for (si32 s = 0; s < nX * nY; ++s)
    {
        src[s] = s;
        sumSrc += src[s];
    }

    fl64* src_dev;
    fl64* dst_dev;

    cudaMalloc(&src_dev, sizeof(fl64) * nX * nY);
    cudaMalloc(&dst_dev, sizeof(fl64) * nX * nY);

    cudaMemcpy(src_dev, src.data(), sizeof(fl64) * nX * nY, cudaMemcpyHostToDevice);

    chrono::system_clock::time_point start = chrono::system_clock::now();
    chrono::system_clock::time_point end;
    chrono::microseconds microSec;

    for (si32 iter = 0; iter < iterMax; ++iter)
    {
        for (si32 j = 0; j < nY; ++j)
        {
            //dev to host
            cudaMemcpy(src.data() + j * nX, src_dev + j * nX, sizeof(fl64) * nX, cudaMemcpyDeviceToHost);
            cudaDeviceSynchronize();
        }
    }

    end = chrono::system_clock::now();
    microSec = chrono::duration_cast<chrono::microseconds>(end - start);

    cudaMemcpy(dst.data(), dst_dev, sizeof(fl64) * nX * nY, cudaMemcpyDeviceToHost);

    fl64 sumDst = 0;
    for (si32 s = 0; s < nX * nY; ++s)
    {
        sumDst += dst[s];
    }

    cout << " calc time elap : " << setw(12) << (microSec.count() / 1000000.0);
    cout << " | iter per sec : " << setw(10) << iterMax / (microSec.count() / 1000000.0);
    cout << endl;

    cudaFree(src_dev);
    cudaFree(dst_dev);
}

int main()
{
    const si32 iterMax = 1;

    const si32 nMax = 134217728;
    const si32 baseSize = 16;
    for (si32 nX = baseSize; nX <= nMax / baseSize; nX *= 2)
    {
        si32 nY = nMax / nX;
        cout << setw(10) << nX << setw(10) << nY << endl;
        testHToH(nX, nY, iterMax);
        testHToD(nX, nY, iterMax);
        testDToH(nX, nY, iterMax);
        testDToD(nX, nY, iterMax);
        testDToD_kernel1D(nX, nY, iterMax);
        testDToD_kernel2D(nX, nY, iterMax);
    }    

    return 0;
}
