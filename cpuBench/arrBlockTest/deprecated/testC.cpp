#include "Arr3D.h"

void testC(const si32 nMaxCore)
{
	const si32 arrSizeMax = 512;

	ofstream fout;
	fout.open("testC.txt");

	fout << fixed << setprecision(15);
	fout
		<< setw(5) << "nCore"
		<< setw(10) << "arrSize"
		<< setw(15) << "totalLen"
		<< setw(25) << "dt"
		<< setw(25) << "dt/item"
		<< setw(20) << "sum"
		<< setw(20) << "checkSum"
		<< setw(10) << "diff" << endl;

	Arr3D<fl64> gridBase;
	for (si32 arrSize = arrSizeMax; arrSize > 0; --arrSize)
	{		
		gridBase.resize(arrSize, arrSize, arrSize);
				
		for (si32 nCore = 1; nCore <= nMaxCore; ++nCore)
		{
			si32 totalLen = gridBase.byteData.size();	
			si32 iterMax = 1;

			chrono::system_clock::time_point start, end;
			chrono::nanoseconds nanoSec;
			start = chrono::system_clock::now();			

			for (si32 iter = 0; iter < iterMax; ++iter)
			{
				#pragma omp parallel for schedule(guided) num_threads(nCore)
				for (si32 ind = 0; ind < totalLen; ++ind)
				{
					si32 iTmp = ind;
					const si32 i = iTmp % arrSize; iTmp /= arrSize;
					const si32 j = iTmp % arrSize; iTmp /= arrSize;
					const si32 k = iTmp;

					gridBase(i, j, k) =
						(i)
						+ (j) * arrSize
						+ (k) * arrSize * arrSize;
				}
			}
			
			end = chrono::system_clock::now();
			nanoSec = chrono::duration_cast<chrono::nanoseconds>(end - start);
			const fl64 dt = nanoSec.count() / 1000000000.0;

			si64 sum = 0;
			for (const auto& v : gridBase.byteData)
			{
				sum += v;
			}

			ui64 checkSum = pow(ui64(arrSize), 3) * (pow(ui64(arrSize), 3) - 1) / 2;

			fout
				<< setw(5) << nCore
				<< setw(10) << arrSize
				<< setw(15) << totalLen
				<< setw(25) << dt / iterMax
				<< setw(25) << dt / totalLen / iterMax
				<< setw(20) << sum
				<< setw(20) << checkSum
				<< setw(10) << checkSum - sum << endl;
		}		
	}
}