
import time

arrSize = 1
for a in range(10):
    
    iterMax = 1
    if(a < 2):
        iterMax = 100000000
    if(a < 4):
        iterMax = 1000000
    if(a < 6):
        iterMax = 1000

    start = time.time_ns()
    for t in range(iterMax):    
        arr = [[[None] * arrSize for j in range(arrSize)] for i in range(arrSize)]
    dtAlloc = time.time_ns() - start
    dtAlloc /= iterMax
    dtAlloc /= 1e9

    checkSum = (arrSize*arrSize*arrSize)*(arrSize*arrSize*arrSize-1)/2
    
    start = time.time_ns()
    for t in range(iterMax):    
        for i in range(arrSize):
            for j in range(arrSize):
                for k in range(arrSize):
                    arr[i][j][k] = 0
    dtInit = time.time_ns() - start
    dtInit /= iterMax
    dtInit /= 1e9

    start = time.time_ns()
    for t in range(iterMax):    
        for i in range(arrSize):
            for j in range(arrSize):
                for k in range(arrSize):
                    arr[i][j][k] = i+j*arrSize + k*arrSize*arrSize
    dtIJK = time.time_ns() - start
    dtIJK /= iterMax
    dtIJK /= 1e9

    start = time.time_ns()    
    for t in range(iterMax):    
        countA = 0
        for i in range(arrSize):
            for j in range(arrSize):
                for k in range(arrSize):
                    countA+=arr[i][j][k]
    dtCheckA = time.time_ns() - start
    dtCheckA /= iterMax
    dtCheckA /= 1e9

    start = time.time_ns()
    for t in range(iterMax):    
        for k in range(arrSize):
            for j in range(arrSize):
                for i in range(arrSize):
                    arr[i][j][k] = i+j*arrSize + k*arrSize*arrSize
    dtKJI = time.time_ns() - start
    dtKJI /= iterMax
    dtKJI /= 1e9

    start = time.time_ns()    
    for t in range(iterMax):    
        countB = 0
        for i in range(arrSize):
            for j in range(arrSize):
                for k in range(arrSize):
                    countB+=arr[i][j][k]
    dtCheckB = time.time_ns() - start
    dtCheckB /= iterMax
    dtCheckB /= 1e9

    print("% 4d %15.12f %15.12f %15.12f %15.12f %15.12f %15.12f %d %d" % (arrSize,dtAlloc,dtInit,dtIJK,dtCheckA,dtKJI,dtCheckB,checkSum==countA,checkSum==countB))

    arrSize *= 2

