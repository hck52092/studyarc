#include "baseHeader.h"
void taskTest(const si32 nCore)
{
    const si32 iterMax = nCore;

    vector<si32> a(iterMax);
    vector<si32> b(iterMax);
    for (si32 i = 0; i < iterMax; ++i)
    {
        a[i] = i * i;
        b[i] = i + 1;
    }

    #pragma omp parallel num_threads(nCore)
    #pragma omp single nowait
    {
        for (si32 i = 0; i < iterMax; ++i)
        {
            #ifdef LLVM
            #pragma omp task
            #endif
            {
                //taskA 

                const si32 th = omp_get_thread_num();
                const si32 ret = a[i] + b[i];
                char str[1000];
                sprintf(str, "thread # %02d taskA : Add -> % 5d + % 5d = % 8d", th, a[i], b[i], ret);
                cout << str << endl;
            }

            #ifdef LLVM
            #pragma omp task
            #endif
            {
                //taskB 

                const si32 th = omp_get_thread_num();
                const si32 ret = a[i] - b[i];
                char str[1000];
                sprintf(str, "thread # %02d taskB : Sub -> % 5d - % 5d = % 8d", th, a[i], b[i], ret);
                cout << str << endl;
            }

            #ifdef LLVM
            #pragma omp task
            #endif
            {
                //taskC 

                const si32 th = omp_get_thread_num();
                const si32 ret = a[i] * b[i];
                char str[1000];
                sprintf(str, "thread # %02d taskC : Mul -> % 5d * % 5d = % 8d", th, a[i], b[i], ret);
                cout << str << endl;
            }

            #ifdef LLVM
            #pragma omp task
            #endif
            {
                //taskD 

                const si32 th = omp_get_thread_num();
                const si32 ret = a[i] / b[i];
                char str[1000];
                sprintf(str, "thread # %02d taskD : Div -> % 5d / % 5d = % 8d", th, a[i], b[i], ret);
                cout << str << endl;
            }
        }

        #ifdef LLVM
        #pragma omp taskwait
        #endif
        
        cout << "finished" << endl;
    }
}