#include "baseHeader.h"
void collapseTest(const si32 nCore)
{
	#pragma omp parallel for collapse(3) num_threads(si32(pow(nCore,3)))
	for (si32 i = 0; i < nCore; ++i)
	{
		for (si32 j = 0; j < nCore; ++j)
		{
			for (si32 k = 0; k < nCore; ++k)
			{
				si32 th = omp_get_thread_num();
				printf("%02d %02d %02d by thread #%04d\n", i, j, k, th);
			}
		}
	}
}