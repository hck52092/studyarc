#include "DataSet3D.h"
#include "device_launch_parameters.h"

__device__ fl64 genRhoAoS_GPU(const Cell3D& cell)
{
	fl64 rho = 0;


	rho += cell.f[0];
	rho += cell.f[1];
	rho += cell.f[2];
	rho += cell.f[3];
	rho += cell.f[4];
	rho += cell.f[5];
	rho += cell.f[6];
	rho += cell.f[7];
	rho += cell.f[8];
	rho += cell.f[9];
	rho += cell.f[10];
	rho += cell.f[10];
	rho += cell.f[11];
	rho += cell.f[12];
	rho += cell.f[13];
	rho += cell.f[14];
	rho += cell.f[15];
	rho += cell.f[16];
	rho += cell.f[17];

	return rho;
}
__device__ fl64 genUAoS_GPU(const Cell3D& cell, const fl64 rho)
{
	fl64 u = 0;
	u += (cell.f[1] + cell.f[7] + cell.f[9] + cell.f[11] + cell.f[3]);
	u -= (cell.f[2] + cell.f[8] + cell.f[10] + cell.f[12] + cell.f[14]);
	u /= rho;
	return u;
}
__device__ fl64 genVAoS_GPU(const Cell3D& cell, const fl64 rho)
{
	fl64 v = 0;
	v += (cell.f[3] + cell.f[7] + cell.f[8] + cell.f[15] + cell.f[17]);
	v -= (cell.f[4] + cell.f[9] + cell.f[10] + cell.f[16] + cell.f[18]);
	v /= rho;
	return v;
}
__device__ fl64 genWAoS_GPU(const Cell3D& cell, const fl64 rho)
{
	fl64 w = 0;
	w += (cell.f[5] + cell.f[11] + cell.f[12] + cell.f[15] + cell.f[16]);
	w -= (cell.f[6] + cell.f[13] + cell.f[14] + cell.f[17] + cell.f[18]);
	w /= rho;
	return w;
}

__global__ void collision3D_AoS_kernel(Cell3D* fValArr, const fl64 s_v, const si32 nNode)	
{
	const fl64  w_rho[3] = {
				12.0 / 36,
				2.0 / 36,
				1.0 / 36 };

	const si32 direcCategory[19] = { 0,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2 };

	const fl64 direcC[19][3] = {
		{0.0,0.0,0.0},
		{1.0,0.0,0.0 },
		{-1.0, 0.0,0.0},
		{0.0,1.0,0.0},
		{0.0,-1.0,0.0 },
		{0.0, 0.0,1.0},
		{0.0, 0.0,-1.0},

		{1.0, 1.0,0.0 },
		{-1.0,1.0,0.0 },
		{1.0,-1.0,0.0 },
		{-1.0,-1.0,0.0 },

		{1.0,0.0,1.0 },
		{-1.0,0.0,1.0 },
		{1.0,0.0,-1.0 },
		{-1.0,0.0,-1.0 },

		{0.0,1.0,1.0 },
		{0.0,-1.0,1.0 },
		{0.0,1.0,-1.0 },
		{0.0,-1.0,-1.0 }
	};

	const fl64 csSq = 1.0 / 3;

	si32 indCur = blockDim.x * blockIdx.x + threadIdx.x;

	if (indCur < nNode)
	{
		auto cellCur = fValArr[indCur];

		const fl64 rho = genRhoAoS_GPU(cellCur);

		fl64 vel[3] = { genUAoS_GPU(cellCur,rho), genVAoS_GPU(cellCur,rho), genWAoS_GPU(cellCur,rho) };

		const fl64 rhoRecipocal = 1.0 / rho;

		fl64 u_dot_u = vel[0] * vel[0] + vel[1] * vel[1] + vel[2] * vel[2];

		for (si32 i = 0; i < 19; ++i)
		{
			const fl64 wTemp = w_rho[direcCategory[i]];
			const fl64 c[3] = { direcC[i][0],direcC[i][1],direcC[i][2] };
			const fl64 dotVal = (c[0] * vel[0] + c[1] * vel[1] + c[2] * vel[2]);

			const fl64 eq = wTemp * rho * (1. + 3.0 * dotVal + 4.5 * (dotVal * dotVal) - 1.5 * u_dot_u);

			cellCur.f[i] = (1 - s_v) * eq + s_v * cellCur.f[i];
		}

		fValArr[indCur].f[0] = cellCur.f[0];
		fValArr[indCur].f[1] = cellCur.f[1];
		fValArr[indCur].f[2] = cellCur.f[2];
		fValArr[indCur].f[3] = cellCur.f[3];
		fValArr[indCur].f[4] = cellCur.f[4];
		fValArr[indCur].f[5] = cellCur.f[5];
		fValArr[indCur].f[6] = cellCur.f[6];
		fValArr[indCur].f[7] = cellCur.f[7];
		fValArr[indCur].f[8] = cellCur.f[8];
		fValArr[indCur].f[9] = cellCur.f[9];
		fValArr[indCur].f[10] = cellCur.f[10];
		fValArr[indCur].f[11] = cellCur.f[11];
		fValArr[indCur].f[12] = cellCur.f[12];
		fValArr[indCur].f[13] = cellCur.f[13];
		fValArr[indCur].f[14] = cellCur.f[14];
		fValArr[indCur].f[15] = cellCur.f[15];
		fValArr[indCur].f[16] = cellCur.f[16];
		fValArr[indCur].f[17] = cellCur.f[17];
		fValArr[indCur].f[18] = cellCur.f[18];
	}
}

void DataSet3D::collision3D_AoS_GPU()
{
	collision3D_AoS_kernel << <blocksPerGrid, threadsPerBlock >> >
		(dataGPU_AoS, s_v, nNode);
}