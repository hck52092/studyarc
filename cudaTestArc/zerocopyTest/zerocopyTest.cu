#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <array>
#include <tuple>
#include <algorithm>
#include <chrono>
#include <omp.h>

using namespace std;

using ui08 = unsigned char;
using si16 = short;
using si32 = int;
using si64 = long long;
using fl32 = float;
using fl64 = double;

#define OPS_0001(a,b) a = a * a + a;// b = b * b + b;
#define OPS_0002(a,b) OPS_0001(a,b) OPS_0001(b,a)
#define OPS_0004(a,b) OPS_0002(a,b) OPS_0002(b,a)
#define OPS_0008(a,b) OPS_0004(a,b) OPS_0004(b,a)
#define OPS_0016(a,b) OPS_0008(a,b) OPS_0008(b,a)
#define OPS_0032(a,b) OPS_0016(a,b) OPS_0016(b,a)
#define OPS_0064(a,b) OPS_0032(a,b) OPS_0032(b,a)
#define OPS_0128(a,b) OPS_0064(a,b) OPS_0064(b,a)
#define OPS_0256(a,b) OPS_0128(a,b) OPS_0128(b,a)
#define OPS_0512(a,b) OPS_0256(a,b) OPS_0256(b,a)
#define OPS_1024(a,b) OPS_0512(a,b) OPS_0512(b,a)
#define OPS_2048(a,b) OPS_1024(a,b) OPS_1024(b,a)
#define OPS_4096(a,b) OPS_2048(a,b) OPS_2048(b,a)
#define OPS_8192(a,b) OPS_4096(a,b) OPS_4096(b,a)

//#define TARGETOPS(a,b) OPS_8192(a,b)
//#define OPS_COEF 8192 * 2
#define TARGETOPS(a,b) OPS_1024(a,b)
#define OPS_COEF 1024 * 2

si32 getBlockSize(const si32 totalSize, const si32 stepSize)
{
    return (totalSize + stepSize - 1) / stepSize;
}

template<typename T>
__global__ void testKernel(const si32 n, const si32 iter, const T* src, T* dst)
{
    si32 i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < n)
    {
        T val1 = src[i];
        T val2 = T(i);

        TARGETOPS(val1, val2);

        dst[i] = val1 + val2;
    }
}

using ResultItem = tuple<fl64, fl64>;

template<typename T>
class FuncTester
{
    si32 totalSize;
    si32 threadSize;
    si32 nBlock;
    si32 streamSize;
    si32 iterMax;

    vector<ResultItem> ret;
    vector<cudaStream_t> streamSet;
    vector<vector<T>> src_h;
    vector<vector<T>> out_h;
    vector<T*> src_d;
    vector<T*> out_d;
    vector<T*> src_z;
    vector<T*> out_z;

    void initMem()
    {
        streamSet.resize(streamSize);
        for (si32 n = 0; n < streamSize; ++n)
        {
            cudaStreamCreate(&streamSet[n]);
        }

        src_h.resize(streamSize);
        out_h.resize(streamSize);
        src_d.resize(streamSize);
        out_d.resize(streamSize);
        src_z.resize(streamSize);
        out_z.resize(streamSize);

        for (si32 n = 0; n < streamSize; ++n)
        {
            src_h[n].resize(totalSize);
            out_h[n].resize(totalSize);
            cudaMalloc(&src_d[n], sizeof(T) * totalSize);
            cudaMalloc(&out_d[n], sizeof(T) * totalSize);

            for (si32 i = 0; i < totalSize; ++i)
            {
                src_h[n][i] = i % 4096 + 1;
            }

            cudaHostRegister((void*)src_h[n].data(), sizeof(T) * totalSize, cudaHostRegisterDefault);
            cudaHostRegister((void*)out_h[n].data(), sizeof(T) * totalSize, cudaHostRegisterDefault);

            cudaHostGetDevicePointer(&src_z[n], src_h[n].data(), 0);
            cudaHostGetDevicePointer(&out_z[n], out_h[n].data(), 0);

            cudaMemcpy(src_d[n], src_h[n].data(), sizeof(T) * totalSize, cudaMemcpyHostToDevice);
        }
    }
    void clearMem()
    {
        for (si32 n = 0; n < streamSize; ++n)
        {
            cudaStreamDestroy(streamSet[n]);
        }

        for (si32 n = 0; n < streamSize; ++n)
        {
            cudaHostUnregister((void*)src_h[n].data());
            cudaHostUnregister((void*)out_h[n].data());
        
            cudaFree(src_d[n]);
            cudaFree(out_d[n]);
        }
    }
    void runFunc()
    {
        ret.emplace_back(execFuncTestOnMemory(testKernel<T>));
        ret.emplace_back(execFuncTestStream(testKernel<T>));
        ret.emplace_back(execFuncTestZerocopy(testKernel<T>));
        ret.emplace_back(execFuncTestStreamWithZero(testKernel<T>));

        checkGPUStatus();
    }
    void checkGPUStatus()
    {
        cudaError_t cuda_status = cudaGetLastError();
        if (cudaSuccess != cuda_status)
        {
            printf("Error: cudaStatus fails, %s \n", cudaGetErrorString(cuda_status));
            system("PAUSE");
            exit(0);
        }
    }
    ResultItem execFuncTestStream(decltype(testKernel<T>) funcPtr)
    {
        chrono::system_clock::time_point start, end;

        start = chrono::system_clock::now();

        //#pragma omp parallel for schedule(dynamic) num_threads(streamSize)
        //for (si32 iter = 0; iter < iterMax* streamSize; ++iter)
        //{
        //    const si32 n = omp_get_thread_num();            
        //    {
        //        cudaMemcpyAsync(src_d[n], src_h[n].data(), totalSize * sizeof(T), cudaMemcpyHostToDevice, streamSet[n]);
        //        funcPtr << <nBlock, threadSize, 0, streamSet[n] >> > (totalSize, iterMax, src_d[n], out_d[n]);
        //        cudaMemcpyAsync(out_h[n].data(), out_d[n], totalSize * sizeof(T), cudaMemcpyDeviceToHost, streamSet[n]);
        //    }
        //    //cudaStreamSynchronize(streamSet[n]);
        //}

        for (si32 iter = 0; iter < iterMax; ++iter)
        {
            for (si32 n = 0; n < streamSize; ++n)
            {
                cudaMemcpyAsync(src_d[n], src_h[n].data(), totalSize * sizeof(T), cudaMemcpyHostToDevice, streamSet[n]);
                funcPtr << <nBlock, threadSize, 0, streamSet[n] >> > (totalSize, iterMax, src_d[n], out_d[n]);
                cudaMemcpyAsync(out_h[n].data(), out_d[n], totalSize * sizeof(T), cudaMemcpyDeviceToHost, streamSet[n]);
            }
        }        

        cudaDeviceSynchronize();
        end = chrono::system_clock::now();

        auto timeElap = chrono::duration_cast<chrono::nanoseconds>(end - start).count() / 10e9;

        return make_tuple(timeElap, timeElap / iterMax);
    }
    ResultItem execFuncTestOnMemory(decltype(testKernel<T>) funcPtr)
    {
        chrono::system_clock::time_point start, end;

        start = chrono::system_clock::now();

        for (si32 iter = 0; iter < iterMax; ++iter)
        {
            for (si32 n = 0; n < streamSize; ++n)
            {
                funcPtr << <nBlock, threadSize >> > (totalSize, iterMax, src_d[n], out_d[n]);
            }
        }

        cudaDeviceSynchronize();
        end = chrono::system_clock::now();

        auto timeElap = chrono::duration_cast<chrono::nanoseconds>(end - start).count() / 10e9;

        return make_tuple(timeElap, timeElap / iterMax);
    }
    ResultItem execFuncTestZerocopy(decltype(testKernel<T>) funcPtr)
    {
        chrono::system_clock::time_point start, end;

        start = chrono::system_clock::now();

        for (si32 iter = 0; iter < iterMax; ++iter)
        {
            for (si32 n = 0; n < streamSize; ++n)
            {
                funcPtr << <nBlock, threadSize >> > (totalSize, iterMax, src_z[n], out_z[n]);
            }
        }        

        cudaDeviceSynchronize();
        end = chrono::system_clock::now();

        auto timeElap = chrono::duration_cast<chrono::nanoseconds>(end - start).count() / 10e9;

        return make_tuple(timeElap, timeElap / iterMax);
    }
    ResultItem execFuncTestStreamWithZero(decltype(testKernel<T>) funcPtr)
    {
        chrono::system_clock::time_point start, end;

        start = chrono::system_clock::now();

        //#pragma omp parallel for schedule(dynamic) num_threads(streamSize)
        //for (si32 iter = 0; iter < iterMax * streamSize; ++iter)
        //{
        //    const si32 n = omp_get_thread_num();
        //    {
        //        funcPtr << <nBlock, threadSize, 0, streamSet[n] >> > (totalSize, iterMax, src_z[n], out_z[n]);
        //    }
        //    cudaStreamSynchronize(streamSet[n]);
        //}

        for (si32 iter = 0; iter < iterMax; ++iter)
        {
            for (si32 n = 0; n < streamSize; ++n)
            {                
                funcPtr << <nBlock, threadSize, 0, streamSet[n] >> > (totalSize, iterMax, src_z[n], out_z[n]);                
            }
        }

        cudaDeviceSynchronize();
        end = chrono::system_clock::now();

        auto timeElap = chrono::duration_cast<chrono::nanoseconds>(end - start).count() / 10e9;

        return make_tuple(timeElap, timeElap / iterMax);
    }

public:
    FuncTester(const string& typeName, const si32 _totalSize, const si32 _threadSize, const si32 _streamSize, const si32 _iterMax)
        : totalSize(_totalSize), threadSize(_threadSize),
        streamSize(_streamSize),
        iterMax(_iterMax)
    {
        nBlock = totalSize / threadSize;
        if (threadSize * nBlock < totalSize)
        {
            nBlock++;
        }

        initMem();
        runFunc();
        clearMem();

        cout
            << setw(10) << typeName
            << setw(10) << totalSize
            << setw(10) << nBlock
            << setw(5) << threadSize
            << setw(4) << streamSize
            << setw(5) << iterMax;

        for (si32 n = 0; n < ret.size(); ++n)
        {
            cout << setw(25) << OPS_COEF * 1.0 * totalSize * streamSize / get<1>(ret[n]) / 10e9;
        }

        cout << endl;
    }
};

int main(int argc, char** argv)
{
    const si32 powMax = 24;

    bool targetType[5] = { true,true,true,true,true };
    si32 sStart = 0;
    si32 sEnd = 5;
    si32 iStart = 8;
    si32 iEnd = 8;
    si32 tStart = 4;
    si32 tEnd = 10;

    if (argc > 1)
    {
        si32 tmpVal;
        {
            tmpVal = atoi(argv[1]);
            targetType[0] = tmpVal % 2; tmpVal /= 2;
            targetType[1] = tmpVal % 2; tmpVal /= 2;
            targetType[2] = tmpVal % 2; tmpVal /= 2;
            targetType[3] = tmpVal % 2; tmpVal /= 2;
            targetType[4] = tmpVal % 2; tmpVal /= 2;
        }
        {
            tmpVal = atoi(argv[2]);
            sStart = tmpVal;

            tmpVal = atoi(argv[3]);
            sEnd = tmpVal;
        }
        {
            tmpVal = atoi(argv[4]);
            iStart = tmpVal;

            tmpVal = atoi(argv[5]);
            iEnd = tmpVal;
        }
        {
            tmpVal = atoi(argv[6]);
            tStart = tmpVal;

            tmpVal = atoi(argv[7]);
            tEnd = tmpVal;
        }
    }


    cout << boolalpha;
    cout << fixed;
    cout << setprecision(12);

    si32 nDevices;
    cudaGetDeviceCount(&nDevices);

    cout << "num Devices = " << nDevices << endl;

    si32 devId = nDevices - 1;
    cudaSetDevice(devId);

    si32 numSMs;
    si32 warpSize;
    cudaDeviceGetAttribute(&numSMs, cudaDevAttrMultiProcessorCount, devId);
    cudaDeviceGetAttribute(&warpSize, cudaDevAttrWarpSize, devId);

    cout << "numSMs = " << numSMs << endl;
    cout << "warpSz = " << warpSize << endl;


    if (targetType[0])
    {
        for (si32 s = sStart; s <= sEnd; ++s)
        {
            const si32 nStream = pow(2, s);
            const si32 totalSize = pow(2, powMax - s);

            for (si32 i = iStart; i <= iEnd; ++i)
            {
                si32 iterMax = pow(2, i);

                for (si32 t = tStart; t <= tEnd; ++t)
                {
                    si32 threadSize = pow(2, t);
                    //FuncTester <si16>("si16", totalSize, threadSize, nStream, iterMax, isStreamBased, isPinned, isOnmemory);
                    FuncTester <si16>("si16", totalSize, threadSize, nStream, iterMax);
                }
            }
        }
    }

    if (targetType[1])
    {
        for (si32 s = sStart; s <= sEnd; ++s)
        {
            const si32 nStream = pow(2, s);
            const si32 totalSize = pow(2, powMax - s);

            for (si32 i = iStart; i <= iEnd; ++i)
            {
                si32 iterMax = pow(2, i);

                for (si32 t = tStart; t <= tEnd; ++t)
                {
                    si32 threadSize = pow(2, t);
                    //FuncTester <si32>("si32", totalSize, threadSize, nStream, iterMax, isStreamBased, isPinned, isOnmemory);
                    FuncTester <si32>("si32", totalSize, threadSize, nStream, iterMax);
                }
            }
        }
    }

    if (targetType[2])
    {
        for (si32 s = sStart; s <= sEnd; ++s)
        {
            const si32 nStream = pow(2, s);
            const si32 totalSize = pow(2, powMax - s);

            for (si32 i = iStart; i <= iEnd; ++i)
            {
                si32 iterMax = pow(2, i);

                for (si32 t = tStart; t <= tEnd; ++t)
                {
                    si32 threadSize = pow(2, t);
                    //FuncTester <si64>("si64", totalSize, threadSize, nStream, iterMax, isStreamBased, isPinned, isOnmemory);
                    FuncTester <si64>("si64", totalSize, threadSize, nStream, iterMax);
                }
            }
        }
    }

    if (targetType[3])
    {
        for (si32 s = sStart; s <= sEnd; ++s)
        {
            const si32 nStream = pow(2, s);
            const si32 totalSize = pow(2, powMax - s);

            for (si32 i = iStart; i <= iEnd; ++i)
            {
                si32 iterMax = pow(2, i);

                for (si32 t = tStart; t <= tEnd; ++t)
                {
                    si32 threadSize = pow(2, t);
                    //FuncTester <fl32>("fl32", totalSize, threadSize, nStream, iterMax, isStreamBased, isPinned, isOnmemory);
                    FuncTester <fl32>("fl32", totalSize, threadSize, nStream, iterMax);
                }
            }
        }
    }

    if (targetType[4])
    {
        for (si32 s = sStart; s <= sEnd; ++s)
        {
            const si32 nStream = pow(2, s);
            const si32 totalSize = pow(2, powMax - s);

            for (si32 i = iStart; i <= iEnd; ++i)
            {
                si32 iterMax = pow(2, i);

                for (si32 t = tStart; t <= tEnd; ++t)
                {
                    si32 threadSize = pow(2, t);
                    //FuncTester <fl64>("fl64", totalSize, threadSize, nStream, iterMax, isStreamBased, isPinned, isOnmemory);
                    FuncTester <fl64>("fl64", totalSize, threadSize, nStream, iterMax);
                }
            }
        }
    }
}