#include "DataSet3D.h"
void DataSet3D::streaming3D_SoA_CPU()
{
	const array<si32, 19> D3Q19CounterArr
	{
		0,
		2,1,4,3,6,5,
		10,9,8,7,
		14,13,12,11,
		18,17,16,15
	};

	for (si32 d = 0; d < 19; ++d)
	{	
		swap(buffer_CPU, dataCPU_SoA[d]);

		const si32 direcCounter = D3Q19CounterArr[d];
		const si32 offset = genDirecOffset(d);

#pragma omp parallel for schedule(guided) num_threads(nCore)
		for (si32 s = 0; s < nNode; ++s)
		{
			if (isValidInd(s + offset))
			{
				dataCPU_AoS[s].f[d] = buffer_CPU[s + offset];
			}
			else
			{
				dataCPU_AoS[s].f[d] = buffer_CPU[s];
			}
		}
	}
}
