#include "DataSet3D.h"
#include "device_launch_parameters.h"
__device__ bool isValidInd_AoS_GPU(const si32 ind, const si32 nNode)
{
	if (ind < 0 || ind >= nNode)
	{
		return false;
	}

	return true;
}
__global__ void swap_kernel(Cell3D* fValArr, fl64* fTmpArr, const si32 direc, const si32 nNode)
{
	si32 indCur = blockDim.x * blockIdx.x + threadIdx.x;

	if (indCur < nNode)
	{
		fTmpArr[indCur] = fValArr[indCur].f[direc];
	}
}
__global__ void streaming3D_AoS_kernel(Cell3D* fValArr, fl64* fTmpArr, const si32 offset, const si32 direc, const si32 nNode)
{
	si32 indCur = blockDim.x * blockIdx.x + threadIdx.x;

	if (indCur < nNode)
	{
		if (isValidInd_AoS_GPU(indCur + offset, nNode))
		{
			fValArr[indCur].f[direc] = fTmpArr[indCur + offset];
		}
		else
		{
			fValArr[indCur].f[direc] = fTmpArr[indCur];
		}
	}
}
void DataSet3D::streaming3D_AoS_GPU()
{
	const array<si32, 19> D3Q19CounterArr
	{
		0,
		2,1,4,3,6,5,
		10,9,8,7,
		14,13,12,11,
		18,17,16,15
	};

	for (si32 d = 0; d < 19; ++d)
	{
		swap_kernel << <blocksPerGrid, threadsPerBlock >> > (dataGPU_AoS, buffer_GPU, d, nNode);

		const si32 direcCounter = D3Q19CounterArr[d];
		const si32 offset = genDirecOffset(d);

		streaming3D_AoS_kernel << <blocksPerGrid, threadsPerBlock >> > (dataGPU_AoS, buffer_GPU, offset, d, nNode);
	}
}
