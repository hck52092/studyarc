#pragma once
#include <omp.h>
#include <vector>
#include <iostream>
#include <iomanip>
#include <string>
#include <chrono>
#include <tuple>
#include <cmath>
using namespace std;

using si64 = long long;
using si32 = long;
using ui08 = unsigned char;
using fl64 = double;

#ifdef __clang__
#pragma comment(lib,"libiomp5md.lib")
#pragma comment(lib,"libomp.lib")
#else

#endif

tuple<fl64, fl64> baseTest(const si32 nCore, const si32 nThread, const si32 testLen, const si32 iterMax);
void nestedTest(const si32 nCore);
void collapseTest(const si32 nCore);
void uniqueJobTest(const si32 nCore, const si32 iterMax);
void taskTest(const si32 nCore);