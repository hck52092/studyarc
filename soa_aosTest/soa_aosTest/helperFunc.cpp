#include "DataSet3D.h"
si32 DataSet3D::genDirecOffset(const si32 direc)
{
	const array< array<si32, 3>, 19> direcOffset = {
		array<si32, 3>{0,0,0},

		array<si32, 3>{1,0,0 },
		array<si32, 3>{-1, 0,0},
		array<si32, 3>{0,1,0},
		array<si32, 3>{0,-1,0 },
		array<si32, 3>{0, 0,1},
		array<si32, 3>{0, 0,-1},

		array<si32, 3>{1, 1,0 },
		array<si32, 3>{-1,1,0 },
		array<si32, 3>{1,-1,0 },
		array<si32, 3>{-1,-1,0 },

		array<si32, 3>{1,0,1 },
		array<si32, 3>{-1,0,1 },
		array<si32, 3>{1,0,-1 },
		array<si32, 3>{-1,0,-1 },

		array<si32, 3>{0,1,1 },
		array<si32, 3>{0,-1,1 },
		array<si32, 3>{0,1,-1 },
		array<si32, 3>{0,-1,-1 }
	};

	si32 n = pow(nNode, 1.0 / 3);

	return direcOffset[direc][0] + direcOffset[direc][1] * n + direcOffset[direc][2] * n * n;
}
bool DataSet3D::isValidInd(const si32 ind)
{
	if (ind < 0 || ind >= nNode)
	{
		return false;
	}

	return true;
}