#include "func.h"
void unrollTest2D(const vector<si32>& nCoreList)
{
    cout << "unrollTest2D" << endl;

    const si32 k = 0;

    Arr3D<fl64> gridA;
    Arr3D<fl64> gridB;
    vector<decltype(&copyOp)> opArr;
    vector<string> opStrArr;
    init2D(gridA, gridB, opArr, opStrArr);

    for (si32 tt = 0; tt < opArr.size(); ++tt)
    {
        cout << opStrArr[tt] << endl;
        {
            cout << setw(10) << "#core";
            cout << setw(15) << "IJ";
            cout << setw(15) << "JI";
            cout << endl;
        }

        auto& op = opArr[tt];
        for (si32 nC = 0; nC < nCoreList.size(); ++nC)
        {
            const si32 nCore = nCoreList[nC];
            cout << setw(10) << nCore;

            const si32 nTotal = gridA.nX() * gridA.nY();
            {
                chrono::system_clock::time_point start, end;
                chrono::nanoseconds nanoSec;
                start = chrono::system_clock::now();

                #pragma omp parallel for schedule(guided) num_threads(nCore)
                for (si32 s = 0; s < nTotal; ++s)
                {
                    si32 sTmp = s;
                    const si32 i = sTmp % gridA.nX(); sTmp /= gridA.nX();
                    const si32 j = sTmp;
                    
                    op(gridA, gridB, i, j, k);
                }

                end = chrono::system_clock::now();
                nanoSec = chrono::duration_cast<chrono::nanoseconds>(end - start);
                const fl64 dt = nanoSec.count() / 1000000000.0;
                cout << setw(15) << dt;
            }

            {
                chrono::system_clock::time_point start, end;
                chrono::nanoseconds nanoSec;
                start = chrono::system_clock::now();

                #pragma omp parallel for schedule(guided) num_threads(nCore)
                for (si32 s = 0; s < nTotal; ++s)
                {
                    si32 sTmp = s;
                    const si32 j = sTmp % gridA.nY(); sTmp /= gridA.nY();
                    const si32 i = sTmp;

                    op(gridA, gridB, i, j, k);
                }

                end = chrono::system_clock::now();
                nanoSec = chrono::duration_cast<chrono::nanoseconds>(end - start);
                const fl64 dt = nanoSec.count() / 1000000000.0;
                cout << setw(15) << dt;
            }

            cout << endl;
        }

        cout << endl;
    }

}