#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <iostream>
#include <iomanip>
#include <vector>
#include <chrono>
#include <string>

using namespace std;

using si32 = int;
using fl64 = double;

using lineArr = vector<fl64>;
void testBase(const si32 nSize, const si32 nGPU, const si32 iterMax)
{
    cout << setw(30) << "testBase";
    vector<vector<lineArr>> src(nGPU);
    vector<vector<lineArr>> dst(nGPU);

    vector<vector<fl64*>> src_dev(nGPU);
    vector<vector<fl64*>> dst_dev(nGPU);

#pragma omp parallel for num_threads(nGPU)
    for (si32 n = 0; n < nGPU; ++n)
    {
        src[n].resize(nGPU);
        dst[n].resize(nGPU);

        src_dev[n].resize(nGPU);
        dst_dev[n].resize(nGPU);

        cudaSetDevice(n);
        for (si32 nn = 0; nn < nGPU; ++nn)
        {
            src[n][nn].resize(nSize);
            dst[n][nn].resize(nSize);

            cudaMalloc(&src_dev[n][nn], sizeof(fl64) * nSize);
            cudaMalloc(&dst_dev[n][nn], sizeof(fl64) * nSize);
        }
    }

    chrono::system_clock::time_point start = chrono::system_clock::now();
    chrono::system_clock::time_point end;
    chrono::microseconds microSec;

    for (si32 iter = 0; iter < iterMax; ++iter)
    {
#pragma omp parallel for num_threads(nGPU)
        for (si32 n = 0; n < nGPU; ++n)
        {
            cudaSetDevice(n);
            for (si32 nn = 0; nn < nGPU; ++nn)
            {
                cudaMemcpy(dst_dev[nn][n], src_dev[n][nn], sizeof(fl64) * nSize, cudaMemcpyDeviceToDevice);
            }
        }
        cudaDeviceSynchronize();
    }

    end = chrono::system_clock::now();
    microSec = chrono::duration_cast<chrono::microseconds>(end - start);

    cout << " calc time elap : " << setw(12) << (microSec.count() / 1000000.0);
    cout << " | iter per sec : " << setw(10) << iterMax / (microSec.count() / 1000000.0);
    cout << " | GB/sec : " << setw(10) << (nSize * sizeof(fl64) * nGPU * iterMax) / 1073741824.0 / (microSec.count() / 1000000.0);
    cout << endl;

#pragma omp parallel for num_threads(nGPU)
    for (si32 n = 0; n < nGPU; ++n)
    {
        cudaSetDevice(n);
        for (si32 nn = 0; nn < nGPU; ++nn)
        {
            cudaFree(src_dev[n][nn]);
            cudaFree(dst_dev[n][nn]);
        }
    }
}
void testBaseAsync(const si32 nSize, const si32 nGPU, const si32 iterMax)
{
    cout << setw(30) << "testBaseAsync";
    vector<vector<lineArr>> src(nGPU);
    vector<vector<lineArr>> dst(nGPU);

    vector<vector<fl64*>> src_dev(nGPU);
    vector<vector<fl64*>> dst_dev(nGPU);

#pragma omp parallel for num_threads(nGPU)
    for (si32 n = 0; n < nGPU; ++n)
    {
        src[n].resize(nGPU);
        dst[n].resize(nGPU);

        src_dev[n].resize(nGPU);
        dst_dev[n].resize(nGPU);

        cudaSetDevice(n);
        for (si32 nn = 0; nn < nGPU; ++nn)
        {
            src[n][nn].resize(nSize);
            dst[n][nn].resize(nSize);

            cudaMalloc(&src_dev[n][nn], sizeof(fl64) * nSize);
            cudaMalloc(&dst_dev[n][nn], sizeof(fl64) * nSize);
        }
    }

    chrono::system_clock::time_point start = chrono::system_clock::now();
    chrono::system_clock::time_point end;
    chrono::microseconds microSec;

    for (si32 iter = 0; iter < iterMax; ++iter)
    {
#pragma omp parallel for num_threads(nGPU)
        for (si32 n = 0; n < nGPU; ++n)
        {
            cudaSetDevice(n);
            for (si32 nn = 0; nn < nGPU; ++nn)
            {
                cudaMemcpyAsync(dst_dev[nn][n], src_dev[n][nn], sizeof(fl64) * nSize, cudaMemcpyDeviceToDevice);
            }
        }
        cudaDeviceSynchronize();
    }

    end = chrono::system_clock::now();
    microSec = chrono::duration_cast<chrono::microseconds>(end - start);

    cout << " calc time elap : " << setw(12) << (microSec.count() / 1000000.0);
    cout << " | iter per sec : " << setw(10) << iterMax / (microSec.count() / 1000000.0);
    cout << " | GB/sec : " << setw(10) << (nSize * sizeof(fl64) * nGPU * iterMax) / 1073741824.0 / (microSec.count() / 1000000.0);
    cout << endl;

#pragma omp parallel for num_threads(nGPU)
    for (si32 n = 0; n < nGPU; ++n)
    {
        cudaSetDevice(n);
        for (si32 nn = 0; nn < nGPU; ++nn)
        {
            cudaFree(src_dev[n][nn]);
            cudaFree(dst_dev[n][nn]);
        }
    }
}
void testBaseAsyncWithStreamA(const si32 nSize, const si32 nGPU, const si32 iterMax)
{
    cout << setw(30) << "testBaseAsyncWithStreamA";
    vector<vector<lineArr>> src(nGPU);
    vector<vector<lineArr>> dst(nGPU);

    vector<vector<fl64*>> src_dev(nGPU);
    vector<vector<fl64*>> dst_dev(nGPU);

    vector<vector<cudaStream_t>> streams(nGPU);

#pragma omp parallel for num_threads(nGPU)
    for (si32 n = 0; n < nGPU; ++n)
    {
        src[n].resize(nGPU);
        dst[n].resize(nGPU);
        
        src_dev[n].resize(nGPU);
        dst_dev[n].resize(nGPU);
        streams[n].resize(nSize);

        cudaSetDevice(n);
        for (si32 nn = 0; nn < nGPU; ++nn)
        {
            src[n][nn].resize(nSize);
            dst[n][nn].resize(nSize);

            cudaMalloc(&src_dev[n][nn], sizeof(fl64) * nSize);
            cudaMalloc(&dst_dev[n][nn], sizeof(fl64) * nSize);

            cudaStreamCreate(&streams[n][nn]);
        }
    }

    chrono::system_clock::time_point start = chrono::system_clock::now();
    chrono::system_clock::time_point end;
    chrono::microseconds microSec;

    for (si32 iter = 0; iter < iterMax; ++iter)
    {
#pragma omp parallel for num_threads(nGPU)
        for (si32 n = 0; n < nGPU; ++n)
        {
            cudaSetDevice(n);
            for (si32 nn = 0; nn < nGPU; ++nn)
            {
                cudaMemcpyAsync(dst_dev[nn][n], src_dev[n][nn], sizeof(fl64) * nSize, cudaMemcpyDeviceToDevice, streams[n][0]);
            }
        }
        cudaDeviceSynchronize();
    }
    

    end = chrono::system_clock::now();
    microSec = chrono::duration_cast<chrono::microseconds>(end - start);

    cout << " calc time elap : " << setw(12) << (microSec.count() / 1000000.0);
    cout << " | iter per sec : " << setw(10) << iterMax / (microSec.count() / 1000000.0);
    cout << " | GB/sec : " << setw(10) << (nSize * sizeof(fl64) * nGPU * iterMax) / 1073741824.0 / (microSec.count() / 1000000.0);
    cout << endl;

#pragma omp parallel for num_threads(nGPU)
    for (si32 n = 0; n < nGPU; ++n)
    {
        cudaSetDevice(n);
        for (si32 nn = 0; nn < nGPU; ++nn)
        {
            cudaFree(src_dev[n][nn]);
            cudaFree(dst_dev[n][nn]);

            cudaStreamDestroy(streams[n][nn]);
        }
    }
}
void testBaseAsyncWithStreamB(const si32 nSize, const si32 nGPU, const si32 iterMax)
{
    cout << setw(30) << "testBaseAsyncWithStreamB";
    vector<vector<lineArr>> src(nGPU);
    vector<vector<lineArr>> dst(nGPU);

    vector<vector<fl64*>> src_dev(nGPU);
    vector<vector<fl64*>> dst_dev(nGPU);

    vector<vector<cudaStream_t>> streams(nGPU);

#pragma omp parallel for num_threads(nGPU)
    for (si32 n = 0; n < nGPU; ++n)
    {
        src[n].resize(nGPU);
        dst[n].resize(nGPU);

        src_dev[n].resize(nGPU);
        dst_dev[n].resize(nGPU);
        streams[n].resize(nSize);

        cudaSetDevice(n);
        for (si32 nn = 0; nn < nGPU; ++nn)
        {
            src[n][nn].resize(nSize);
            dst[n][nn].resize(nSize);

            cudaMalloc(&src_dev[n][nn], sizeof(fl64) * nSize);
            cudaMalloc(&dst_dev[n][nn], sizeof(fl64) * nSize);

            cudaStreamCreate(&streams[n][nn]);
        }
    }

    chrono::system_clock::time_point start = chrono::system_clock::now();
    chrono::system_clock::time_point end;
    chrono::microseconds microSec;

    for (si32 iter = 0; iter < iterMax; ++iter)
    {
#pragma omp parallel for num_threads(nGPU)
        for (si32 n = 0; n < nGPU; ++n)
        {
            cudaSetDevice(n);
            for (si32 nn = 0; nn < nGPU; ++nn)
            {
                cudaMemcpyAsync(dst_dev[nn][n], src_dev[n][nn], sizeof(fl64) * nSize, cudaMemcpyDeviceToDevice, streams[n][nn]);
            }
        }
        cudaDeviceSynchronize();
    }

    end = chrono::system_clock::now();
    microSec = chrono::duration_cast<chrono::microseconds>(end - start);

    cout << " calc time elap : " << setw(12) << (microSec.count() / 1000000.0);
    cout << " | iter per sec : " << setw(10) << iterMax / (microSec.count() / 1000000.0);
    cout << " | GB/sec : " << setw(10) << (nSize * sizeof(fl64) * nGPU * iterMax) / 1073741824.0 / (microSec.count() / 1000000.0);
    cout << endl;

#pragma omp parallel for num_threads(nGPU)
    for (si32 n = 0; n < nGPU; ++n)
    {
        cudaSetDevice(n);
        for (si32 nn = 0; nn < nGPU; ++nn)
        {
            cudaFree(src_dev[n][nn]);
            cudaFree(dst_dev[n][nn]);

            cudaStreamDestroy(streams[n][nn]);
        }
    }
}

int main()
{
    si32 nDevices;
    cudaGetDeviceCount(&nDevices);

    const si32 nMax = 134217728;

    for (si32 nSize = 32; nSize <= nMax; nSize *= 2)
    {
        si32 nGPU;
        const si32 iterMax = 10 * pow(log2(nMax / nSize) + 1, 2);

        cout << nSize << endl;

        for (si32 nGPU = 1; nGPU <= nDevices; ++nGPU)
        {
            testBase(nSize, nGPU, iterMax);
            testBaseAsync(nSize, nGPU, iterMax);
            testBaseAsyncWithStreamA(nSize, nGPU, iterMax);
            testBaseAsyncWithStreamB(nSize, nGPU, iterMax);
        }

        cout << endl;
    }

    return 0;
}
