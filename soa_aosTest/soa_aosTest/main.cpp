#include "DataSet3D.h"

int main()
{
	cout << setprecision(15);

	bool runGPU = true;
	bool runCPU = false;

	const si32 base = 40000 * 10000;

	vector<si32> nNodeArr{ 10000,20000,40000,80000,160000 };
	vector<si32> maxIterCPU;
	vector<si32> maxIterGPU;
	for (si32 n = 0; n < nNodeArr.size(); ++n)
	{
		maxIterCPU.emplace_back(base / nNodeArr[n]);
		maxIterGPU.emplace_back(base / nNodeArr[n]);
	}

	const fl64 s_v = 1.2;
	
	if (runGPU)
	{
		const si32 nCore = 1;
		cout << setw(10) << "#node" << setw(10) << "#Thr/Blk"
			<< setw(20) << "GPU_coll_AoS"
			<< setw(20) << "GPU_coll_SoA"
			<< setw(20) << "GPU_strm_AoS"
			<< setw(20) << "GPU_strm_SoA"

			<< setw(20) << "GPU_AoS"
			<< setw(20) << "GPU_SoA"
			<< endl;

		for (si32 n = 0; n < nNodeArr.size(); ++n)
		{
			const si32 nNode = nNodeArr[n];

			for (si32 tpb = 16; tpb <= 512; tpb *= 2) //1024 fails
			{
				DataSet3D data3D = DataSet3D(nNode, tpb, s_v, nCore);

				fl64 dtCPU_coll_AoS;
				fl64 dtGPU_coll_AoS;

				fl64 dtCPU_coll_SoA;
				fl64 dtGPU_coll_SoA;

				fl64 dtCPU_strm_AoS;
				fl64 dtGPU_strm_AoS;

				fl64 dtCPU_strm_SoA;
				fl64 dtGPU_strm_SoA;

				{
					chrono::system_clock::time_point start, end;
					chrono::nanoseconds nanoSec;
					start = chrono::system_clock::now();

					for (si32 iter = 0; iter < maxIterGPU[n]; ++iter)
					{
						data3D.collision3D_AoS_GPU();
					}

					end = chrono::system_clock::now();
					nanoSec = chrono::duration_cast<chrono::nanoseconds>(end - start);
					dtGPU_coll_AoS = (nanoSec.count() / 1000000000.0) / maxIterGPU[n];					
				}

				{
					chrono::system_clock::time_point start, end;
					chrono::nanoseconds nanoSec;
					start = chrono::system_clock::now();

					for (si32 iter = 0; iter < maxIterGPU[n]; ++iter)
					{
						data3D.collision3D_SoA_GPU();
					}

					end = chrono::system_clock::now();
					nanoSec = chrono::duration_cast<chrono::nanoseconds>(end - start);
					dtGPU_coll_SoA = (nanoSec.count() / 1000000000.0) / maxIterGPU[n];
				}

				{
					chrono::system_clock::time_point start, end;
					chrono::nanoseconds nanoSec;
					start = chrono::system_clock::now();

					for (si32 iter = 0; iter < maxIterGPU[n]; ++iter)
					{
						data3D.streaming3D_AoS_GPU();
					}

					end = chrono::system_clock::now();
					nanoSec = chrono::duration_cast<chrono::nanoseconds>(end - start);
					dtGPU_strm_AoS = (nanoSec.count() / 1000000000.0) / maxIterGPU[n];
				}

				{
					chrono::system_clock::time_point start, end;
					chrono::nanoseconds nanoSec;
					start = chrono::system_clock::now();

					for (si32 iter = 0; iter < maxIterGPU[n]; ++iter)
					{
						data3D.streaming3D_SoA_GPU();
					}

					end = chrono::system_clock::now();
					nanoSec = chrono::duration_cast<chrono::nanoseconds>(end - start);
					dtGPU_strm_SoA = (nanoSec.count() / 1000000000.0) / maxIterGPU[n];
				}

				cout << setw(10) << nNode << setw(10) << tpb
					<< setw(20) << dtGPU_coll_AoS
					<< setw(20) << dtGPU_coll_SoA
					<< setw(20) << dtGPU_strm_AoS
					<< setw(20) << dtGPU_strm_SoA

					<< setw(20) << dtGPU_coll_AoS + dtGPU_strm_AoS
					<< setw(20) << dtGPU_coll_SoA + dtGPU_strm_SoA
					<< endl;
			}
		}
	}

	if (runCPU)
	for (si32 n = 0; n < nNodeArr.size(); ++n)
	{
		const si32 nNode = nNodeArr[n];
		cout << nNode << endl;

		cout << setw(10) << "#nCore"
			<< setw(20) << "CPU_coll_AoS" 
			<< setw(20) << "CPU_coll_SoA" 
			<< setw(20) << "CPU_strm_AoS" 
			<< setw(20) << "CPU_strm_SoA" 

			<< setw(20) << "CPU_AoS" 
			<< setw(20) << "CPU_SoA" 
			<< endl;

		for (si32 nCore = 1; nCore <= 16; nCore *= 2)
		{			
			const si32 tpb = 256;
			DataSet3D data3D = DataSet3D(nNode, tpb, s_v, nCore);

			fl64 dtCPU_coll_AoS;
			fl64 dtGPU_coll_AoS;

			fl64 dtCPU_coll_SoA;
			fl64 dtGPU_coll_SoA;

			fl64 dtCPU_strm_AoS;
			fl64 dtGPU_strm_AoS;

			fl64 dtCPU_strm_SoA;
			fl64 dtGPU_strm_SoA;

			{
				chrono::system_clock::time_point start, end;
				chrono::nanoseconds nanoSec;
				start = chrono::system_clock::now();

				for (si32 iter = 0; iter < maxIterCPU[n]; ++iter)
				{
					data3D.collision3D_AoS_CPU();
				}

				end = chrono::system_clock::now();
				nanoSec = chrono::duration_cast<chrono::nanoseconds>(end - start);
				dtCPU_coll_AoS = (nanoSec.count() / 1000000000.0) / maxIterCPU[n];
			}

			{
				chrono::system_clock::time_point start, end;
				chrono::nanoseconds nanoSec;
				start = chrono::system_clock::now();

				for (si32 iter = 0; iter < maxIterCPU[n]; ++iter)
				{
					data3D.collision3D_SoA_CPU();
				}

				end = chrono::system_clock::now();
				nanoSec = chrono::duration_cast<chrono::nanoseconds>(end - start);
				dtCPU_coll_SoA = (nanoSec.count() / 1000000000.0) / maxIterCPU[n];
			}

			{
				chrono::system_clock::time_point start, end;
				chrono::nanoseconds nanoSec;
				start = chrono::system_clock::now();

				for (si32 iter = 0; iter < maxIterCPU[n]; ++iter)
				{
					data3D.streaming3D_AoS_CPU();
				}

				end = chrono::system_clock::now();
				nanoSec = chrono::duration_cast<chrono::nanoseconds>(end - start);
				dtCPU_strm_AoS = (nanoSec.count() / 1000000000.0) / maxIterCPU[n];
			}

			{
				chrono::system_clock::time_point start, end;
				chrono::nanoseconds nanoSec;
				start = chrono::system_clock::now();

				for (si32 iter = 0; iter < maxIterCPU[n]; ++iter)
				{
					data3D.streaming3D_SoA_CPU();
				}

				end = chrono::system_clock::now();
				nanoSec = chrono::duration_cast<chrono::nanoseconds>(end - start);
				dtCPU_strm_SoA = (nanoSec.count() / 1000000000.0) / maxIterCPU[n];
			}

			cout << setw(10) << nCore
				<< setw(20) << dtCPU_coll_AoS
				<< setw(20) << dtCPU_coll_SoA
				<< setw(20) << dtCPU_strm_AoS
				<< setw(20) << dtCPU_strm_SoA

				<< setw(20) << (dtCPU_coll_AoS + dtCPU_strm_AoS)
				<< setw(20) << (dtCPU_coll_SoA + dtCPU_strm_SoA)
				<< endl;
		}
	}
}