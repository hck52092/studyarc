#include "Arr3D.h"
void copyOp(const Arr3D<fl64>& src, Arr3D<fl64>& dst, const si32 i, const si32 j, const si32 k)
{
    dst(i, j, k) = src(i, j, k);
}
void swapIJ(const Arr3D<fl64>& src, Arr3D<fl64>& dst, const si32 i, const si32 j, const si32 k)
{
    dst(j, i, k) = src(i, j, k);
}
void swapJK(const Arr3D<fl64>& src, Arr3D<fl64>& dst, const si32 i, const si32 j, const si32 k)
{
    dst(i, k, j) = src(i, j, k);
}
void swapKI(const Arr3D<fl64>& src, Arr3D<fl64>& dst, const si32 i, const si32 j, const si32 k)
{
    dst(k, j, i) = src(i, j, k);
}
void compOp(const Arr3D<fl64>& src, Arr3D<fl64>& dst, const si32 i, const si32 j, const si32 k)
{
    const fl64 tmp = src(i, j, k);
    const fl64 f1 = sin(tmp);
    const fl64 f2 = cos(tmp);
    const fl64 f3 = pow(tmp, 2);
    const fl64 f4 = log(tmp + 1);
    const fl64 f5 = sqrt(tmp);

    dst(i, j, k) = (f1 + f2) * f4 + f3 / f5;
}

void init2D(Arr3D<fl64>& gridA, Arr3D<fl64>& gridB, vector<decltype(&copyOp)>& opArr, vector<string>& opStrArr)
{
    const si32 powMax = 14;
    const si32 arrSize = pow(2, powMax);

    gridA.resize(arrSize, arrSize, 1);
    gridB.resize(arrSize, arrSize, 1);

    #pragma omp parallel for schedule(guided)
    for (si32 i = 0; i < gridA.byteData.size(); ++i)
    {
        gridA.byteData[i] = i;
    }
        
    opArr.emplace_back(&copyOp);
    opArr.emplace_back(&swapIJ);
    opArr.emplace_back(&compOp);

    opStrArr.emplace_back("copyOp");
    opStrArr.emplace_back("swapIJ");
    opStrArr.emplace_back("compOp");
}
void init3D(Arr3D<fl64>& gridA, Arr3D<fl64>& gridB, vector<decltype(&copyOp)>& opArr, vector<string>& opStrArr)
{
    const si32 powMax = 9;
    const si32 arrSize = pow(2, powMax);

    gridA.resize(arrSize, arrSize, arrSize);
    gridB.resize(arrSize, arrSize, arrSize);

#pragma omp parallel for schedule(guided)
    for (si32 i = 0; i < gridA.byteData.size(); ++i)
    {
        gridA.byteData[i] = i;
    }

    opArr.emplace_back(&copyOp);
    opArr.emplace_back(&swapIJ);
    opArr.emplace_back(&swapJK);
    opArr.emplace_back(&swapKI);
    opArr.emplace_back(&compOp);

    opStrArr.emplace_back("copyOp");
    opStrArr.emplace_back("swapIJ");
    opStrArr.emplace_back("swapJK");
    opStrArr.emplace_back("swapKI");
    opStrArr.emplace_back("compOp");
}