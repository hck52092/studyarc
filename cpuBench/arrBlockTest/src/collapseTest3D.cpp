#include "func.h"
void collapseTest3D(const vector<si32>& nCoreList)
{
    cout << "collapseTest3D" << endl;

    Arr3D<fl64> gridA;
    Arr3D<fl64> gridB;
    vector<decltype(&copyOp)> opArr;
    vector<string> opStrArr;
    init3D(gridA, gridB, opArr, opStrArr);

    omp_set_nested(1);
    for (si32 tt = 0; tt < opArr.size(); ++tt)
    {
        cout << opStrArr[tt] << endl;

        {
            cout << setw(10) << "#core";
            cout << setw(15) << "linear";
            cout << setw(15) << "loop";
            cout << setw(15) << "collapse";
            cout << endl;
        }

        auto& op = opArr[tt];
        for (si32 nC = 0; nC < nCoreList.size(); ++nC)
        {
            const si32 nCore = nCoreList[nC];
            cout << setw(10) << nCore;

            const si32 nTotal = gridA.nX() * gridA.nY() * gridA.nZ();
            {
                chrono::system_clock::time_point start, end;
                chrono::nanoseconds nanoSec;
                start = chrono::system_clock::now();

#pragma omp parallel for schedule(guided), num_threads(nCore)
                for (si32 s = 0; s < nTotal; ++s)
                {
                    si32 sTmp = s;
                    const si32 i = sTmp % gridA.nX(); sTmp /= gridA.nX();
                    const si32 j = sTmp % gridA.nY(); sTmp /= gridA.nY();
                    const si32 k = sTmp;
                    op(gridA, gridB, i, j, k);
                }

                end = chrono::system_clock::now();
                nanoSec = chrono::duration_cast<chrono::nanoseconds>(end - start);
                const fl64 dt = nanoSec.count() / 1000000000.0;
                cout << setw(15) << dt;
            }

            {
                chrono::system_clock::time_point start, end;
                chrono::nanoseconds nanoSec;
                start = chrono::system_clock::now();

#pragma omp parallel for schedule(guided), num_threads(nCore)
                for (si32 k = 0; k < gridA.nZ(); ++k)
                {
                    for (si32 j = 0; j < gridA.nY(); ++j)
                    {
                        for (si32 i = 0; i < gridA.nX(); ++i)
                        {
                            op(gridA, gridB, i, j, k);
                        }
                    }
                }

                end = chrono::system_clock::now();
                nanoSec = chrono::duration_cast<chrono::nanoseconds>(end - start);
                const fl64 dt = nanoSec.count() / 1000000000.0;
                cout << setw(15) << dt;
            }

            {
                chrono::system_clock::time_point start, end;
                chrono::nanoseconds nanoSec;
                start = chrono::system_clock::now();

#pragma omp parallel for schedule(guided), num_threads(nCore) collapse(3)
                for (si32 k = 0; k < gridA.nZ(); ++k)
                {
                    for (si32 j = 0; j < gridA.nY(); ++j)
                    {
                        for (si32 i = 0; i < gridA.nX(); ++i)
                        {
                            op(gridA, gridB, i, j, k);
                        }
                    }
                }

                end = chrono::system_clock::now();
                nanoSec = chrono::duration_cast<chrono::nanoseconds>(end - start);
                const fl64 dt = nanoSec.count() / 1000000000.0;
                cout << setw(15) << dt;
            }

            cout << endl;
        }

        cout << endl;
    }

    omp_set_nested(0);
}