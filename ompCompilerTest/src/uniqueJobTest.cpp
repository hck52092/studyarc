#include "baseHeader.h"
void uniqueJobTest(const si32 nCore, const si32 iterMax)
{
	const si32 nCoreModified = nCore * 4;
	vector<string> strSet(nCoreModified);

	cout << "testing single" << endl;
	for (si32 iter = 0; iter < iterMax; ++iter)
	{
		cout << "test # " << setw(4) << iter << " : ";
				
		#pragma omp parallel num_threads(nCoreModified)
		{		
			si32 th = omp_get_thread_num();

			#pragma omp for
			for (si32 s = 0; s < nCoreModified; ++s)
			{				
				strSet[th] = " ";
			}

			#pragma omp single nowait
			{
				strSet[th] = "O";
			}			
		}

		for (si32 th = 0; th < nCoreModified; ++th)
		{
			cout << strSet[th];
		}
		cout << endl;
	}
	cout << endl;

	cout << "testing master" << endl;
	for (si32 iter = 0; iter < iterMax; ++iter)
	{
		cout << "test # " << setw(4) << iter << " : ";

		#pragma omp parallel num_threads(nCoreModified)
		{
			si32 th = omp_get_thread_num();

			#pragma omp for
			for (si32 s = 0; s < nCoreModified; ++s)
			{
				strSet[th] = " ";
			}

			#pragma omp master
			{
				strSet[th] = "O";
			}
		}

		for (si32 th = 0; th < nCoreModified; ++th)
		{
			cout << strSet[th];
		}
		cout << endl;
	}
}