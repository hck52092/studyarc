#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <iostream>
#include <iomanip>
#include <vector>
#include <array>
#include <chrono>
#include <string>

#define sym2str(x) #x

using namespace std;

using si32 = int;
using fl64 = double;

using lineArr = vector<fl64>;

const si32 DIMSIZE = 5;
const si32 bufSize = 12;

__global__ void makeSendBuffer_kernel1(fl64* dst, const fl64* src, const int3 dimDst, const int3 dimSrc, const int3 offset, const si32 nTotal)
{
    const si32 indCur = blockDim.x * blockIdx.x + threadIdx.x;

    if (indCur < nTotal)
    {
        const si32 i = indCur % dimDst.x;
        const si32 j = (indCur / dimDst.x) % dimDst.y;
        const si32 k = (indCur / dimDst.x) / dimDst.y;
        const si32 indFullSrc = (i + offset.x) + dimSrc.x * ((j + offset.y) + dimSrc.y * (k + offset.z));

        dst[indCur] = src[indFullSrc];
    }
}
__global__ void takeRecvBuffer_kernel1(fl64* dst, const fl64* src, const int3 dimDst, const int3 dimSrc, const int3 offset, const si32 nTotal)
{
    const si32 indCur = blockDim.x * blockIdx.x + threadIdx.x;

    if (indCur < nTotal)
    {
        const si32 i = indCur % dimSrc.x;
        const si32 j = (indCur / dimSrc.x) % dimSrc.y;
        const si32 k = (indCur / dimSrc.x) / dimSrc.y;
        const si32 indFullDst = (i + offset.x) + dimDst.x * ((j + offset.y) + dimDst.y * (k + offset.z));

        dst[indFullDst] = src[indCur];
    }
}

__global__ void makeSendBuffer_kernel5(
    fl64* dst1, fl64* dst2, fl64* dst3, fl64* dst4, fl64* dst5,
    const fl64* src1, const fl64* src2, const fl64* src3, const fl64* src4, const fl64* src5,
    const int3 dimDst, const int3 dimSrc, const int3 offset, const si32 nTotal)
{
    const si32 indCur = blockDim.x * blockIdx.x + threadIdx.x;

    if (indCur < nTotal)
    {
        const si32 i = indCur % dimDst.x;
        const si32 j = (indCur / dimDst.x) % dimDst.y;
        const si32 k = (indCur / dimDst.x) / dimDst.y;
        const si32 indFullSrc = (i + offset.x) + dimSrc.x * ((j + offset.y) + dimSrc.y * (k + offset.z));

        dst1[indCur] = src1[indFullSrc];
        dst2[indCur] = src2[indFullSrc];
        dst3[indCur] = src3[indFullSrc];
        dst4[indCur] = src4[indFullSrc];
        dst5[indCur] = src5[indFullSrc];
    }
}
__global__ void takeRecvBuffer_kernel5(
    fl64* dst1, fl64* dst2, fl64* dst3, fl64* dst4, fl64* dst5,
    const fl64* src1, const fl64* src2, const fl64* src3, const fl64* src4, const fl64* src5,
    const int3 dimDst, const int3 dimSrc, const int3 offset, const si32 nTotal)
{
    const si32 indCur = blockDim.x * blockIdx.x + threadIdx.x;

    if (indCur < nTotal)
    {
        const si32 i = indCur % dimSrc.x;
        const si32 j = (indCur / dimSrc.x) % dimSrc.y;
        const si32 k = (indCur / dimSrc.x) / dimSrc.y;
        const si32 indFullDst = (i + offset.x) + dimDst.x * ((j + offset.y) + dimDst.y * (k + offset.z));

        dst1[indFullDst] = src1[indCur];
        dst2[indFullDst] = src2[indCur];
        dst3[indFullDst] = src3[indCur];
        dst4[indFullDst] = src4[indCur];
        dst5[indFullDst] = src5[indCur];
    }
}

struct ItemSet
{
    const si32 nGPU;
    const si32 arrSize;
    const si32 swapSize;

    const si32 nx;
    const si32 ny;
    const si32 nz;

    const int3 dimChunk;
    const int3 dimFull;
    const int3 offset;

    const si32 nBlock;
    const si32 nThread;

    vector<array<fl64*, DIMSIZE>> src_dev;
    vector<array<fl64*, DIMSIZE>> dst_dev;

    vector<vector<fl64*>> sendBuffer_dev;
    vector<vector<fl64*>> recvBuffer_dev;

    vector<vector<cudaStream_t>> streams;

    ItemSet(const si32 _nx, const si32 _ny, const si32 _nz, const si32 _bufSize, const si32 _nGPU, const si32 _nThread);
    ~ItemSet();
};

ItemSet::ItemSet(const si32 _nx, const si32 _ny, const si32 _nz, const si32 _bufSize, const si32 _nGPU, const si32 _nThread)
    :nGPU(_nGPU),
    arrSize((_nx + _bufSize * 2 + 1)* (_ny + _bufSize * 2 + 1)* (_nz + _bufSize * 2 + 1)),
    swapSize((_bufSize + 1)* (_bufSize + 1)* (_bufSize + 1)),
    nx(_nx), ny(_ny), nz(_nz),
    dimChunk{ _bufSize + 1, _bufSize + 1, _bufSize + 1 },
    dimFull{ _nx + _bufSize * 2 + 1, _ny + _bufSize * 2 + 1, _nz + _bufSize * 2 + 1 },
    offset{ _bufSize + 1, _bufSize + 1, _bufSize + 1 },
    nBlock((swapSize + _nThread - 1) / _nThread),
    nThread(_nThread)
{
    src_dev.resize(nGPU);
    dst_dev.resize(nGPU);
    sendBuffer_dev.resize(nGPU);
    recvBuffer_dev.resize(nGPU);
    streams.resize(nGPU);
    for (si32 n = 0; n < nGPU; ++n)
    {
        cudaSetDevice(n);
        for (si32 d = 0; d < DIMSIZE; ++d)
        {
            cudaMalloc(&src_dev[n][d], sizeof(fl64) * arrSize);
            cudaMalloc(&dst_dev[n][d], sizeof(fl64) * arrSize);
        }        

        sendBuffer_dev[n].resize(nGPU);
        recvBuffer_dev[n].resize(nGPU);
        streams[n].resize(nGPU);

        for (si32 nn = 0; nn < nGPU; ++nn)
        {
            cudaMalloc(&sendBuffer_dev[n][nn], sizeof(fl64) * swapSize * DIMSIZE);
            cudaMalloc(&recvBuffer_dev[n][nn], sizeof(fl64) * swapSize * DIMSIZE);
            cudaStreamCreate(&streams[n][nn]);
        }
    }
}
ItemSet::~ItemSet()
{    
    for (si32 n = 0; n < nGPU; ++n)
    {
        cudaSetDevice(n);
        for (si32 d = 0; d < DIMSIZE; ++d)
        {
            cudaFree(src_dev[n][d]);
            cudaFree(dst_dev[n][d]);
        }

        for (si32 nn = 0; nn < nGPU; ++nn)
        {
            cudaFree(sendBuffer_dev[n][nn]);
            cudaFree(recvBuffer_dev[n][nn]);
            cudaStreamDestroy(streams[n][nn]);
        }
    }
}

void testType0A(ItemSet& itemSet)
{
    //5 make
    //5 copy
    //5 take

    for (si32 d = 0; d < DIMSIZE; ++d)
    {
#pragma omp parallel for num_threads(itemSet.nGPU)
        for (si32 n = 0; n < itemSet.nGPU; ++n)
        {
            cudaSetDevice(n);
            for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
            {
                makeSendBuffer_kernel1 << < itemSet.nBlock, itemSet.nThread >> > (itemSet.sendBuffer_dev[n][nn], itemSet.src_dev[n][d], itemSet.dimFull, itemSet.dimChunk, itemSet.offset, itemSet.swapSize);
            }
        }
        cudaDeviceSynchronize();

#pragma omp parallel for num_threads(itemSet.nGPU)
        for (si32 n = 0; n < itemSet.nGPU; ++n)
        {
            for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
            {
                cudaMemcpyAsync(itemSet.recvBuffer_dev[nn][n], itemSet.sendBuffer_dev[n][nn], sizeof(fl64) * itemSet.swapSize, cudaMemcpyDeviceToDevice);
            }
        }
        cudaDeviceSynchronize();
    
#pragma omp parallel for num_threads(itemSet.nGPU)
        for (si32 n = 0; n < itemSet.nGPU; ++n)
        {
            cudaSetDevice(n);
            for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
            {
                takeRecvBuffer_kernel1 << < itemSet.nBlock, itemSet.nThread >> > (itemSet.dst_dev[n][d], itemSet.recvBuffer_dev[n][nn], itemSet.dimChunk, itemSet.dimFull, itemSet.offset, itemSet.swapSize);
            }
        }
        cudaDeviceSynchronize();
    }
}

void testType0B(ItemSet& itemSet)
{
    //5 make
    //5 copy
    //5 take

    for (si32 d = 0; d < DIMSIZE; ++d)
    {
#pragma omp parallel for num_threads(itemSet.nGPU)
        for (si32 n = 0; n < itemSet.nGPU; ++n)
        {
            cudaSetDevice(n);
            for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
            {
                makeSendBuffer_kernel1 << < itemSet.nBlock, itemSet.nThread >> > (itemSet.sendBuffer_dev[n][nn], itemSet.src_dev[n][d], itemSet.dimFull, itemSet.dimChunk, itemSet.offset, itemSet.swapSize);
            }
        }
        cudaDeviceSynchronize();

#pragma omp parallel for num_threads(itemSet.nGPU)
        for (si32 n = 0; n < itemSet.nGPU; ++n)
        {
            for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
            {
                cudaMemcpyAsync(itemSet.recvBuffer_dev[nn][n], itemSet.sendBuffer_dev[n][nn], sizeof(fl64) * itemSet.swapSize, cudaMemcpyDeviceToDevice, itemSet.streams[n][nn]);
            }
        }
        cudaDeviceSynchronize();

#pragma omp parallel for num_threads(itemSet.nGPU)
        for (si32 n = 0; n < itemSet.nGPU; ++n)
        {
            cudaSetDevice(n);
            for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
            {
                takeRecvBuffer_kernel1 << < itemSet.nBlock, itemSet.nThread >> > (itemSet.dst_dev[n][d], itemSet.recvBuffer_dev[n][nn], itemSet.dimChunk, itemSet.dimFull, itemSet.offset, itemSet.swapSize);
            }
        }
        cudaDeviceSynchronize();
    }
}

void testType0C(ItemSet& itemSet)
{
    //5 make
    //5 copy
    //5 take

    for (si32 d = 0; d < DIMSIZE; ++d)
    {
#pragma omp parallel for num_threads(itemSet.nGPU)
        for (si32 n = 0; n < itemSet.nGPU; ++n)
        {
            cudaSetDevice(n);
            for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
            {
                makeSendBuffer_kernel1 << < itemSet.nBlock, itemSet.nThread, 0, itemSet.streams[n][nn] >> > (itemSet.sendBuffer_dev[n][nn], itemSet.src_dev[n][d], itemSet.dimFull, itemSet.dimChunk, itemSet.offset, itemSet.swapSize);
            }
        }
        cudaDeviceSynchronize();

#pragma omp parallel for num_threads(itemSet.nGPU)
        for (si32 n = 0; n < itemSet.nGPU; ++n)
        {
            for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
            {
                cudaMemcpyAsync(itemSet.recvBuffer_dev[nn][n], itemSet.sendBuffer_dev[n][nn], sizeof(fl64) * itemSet.swapSize, cudaMemcpyDeviceToDevice);
            }
        }
        cudaDeviceSynchronize();

#pragma omp parallel for num_threads(itemSet.nGPU)
        for (si32 n = 0; n < itemSet.nGPU; ++n)
        {
            cudaSetDevice(n);
            for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
            {
                takeRecvBuffer_kernel1 << < itemSet.nBlock, itemSet.nThread, 0, itemSet.streams[n][nn] >> > (itemSet.dst_dev[n][d], itemSet.recvBuffer_dev[n][nn], itemSet.dimChunk, itemSet.dimFull, itemSet.offset, itemSet.swapSize);
            }
        }
        cudaDeviceSynchronize();
    }
}

void testType0D(ItemSet& itemSet)
{
    //5 make
    //5 copy
    //5 take

    for (si32 d = 0; d < DIMSIZE; ++d)
    {
#pragma omp parallel for num_threads(itemSet.nGPU)
        for (si32 n = 0; n < itemSet.nGPU; ++n)
        {
            cudaSetDevice(n);
            for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
            {
                makeSendBuffer_kernel1 << < itemSet.nBlock, itemSet.nThread,0, itemSet.streams[n][nn] >> > (itemSet.sendBuffer_dev[n][nn], itemSet.src_dev[n][d], itemSet.dimFull, itemSet.dimChunk, itemSet.offset, itemSet.swapSize);
            }
        }
        cudaDeviceSynchronize();

#pragma omp parallel for num_threads(itemSet.nGPU)
        for (si32 n = 0; n < itemSet.nGPU; ++n)
        {
            for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
            {
                cudaMemcpyAsync(itemSet.recvBuffer_dev[nn][n], itemSet.sendBuffer_dev[n][nn], sizeof(fl64) * itemSet.swapSize, cudaMemcpyDeviceToDevice, itemSet.streams[n][nn]);
            }
        }
        cudaDeviceSynchronize();

#pragma omp parallel for num_threads(itemSet.nGPU)
        for (si32 n = 0; n < itemSet.nGPU; ++n)
        {
            cudaSetDevice(n);
            for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
            {
                takeRecvBuffer_kernel1 << < itemSet.nBlock, itemSet.nThread,0, itemSet.streams[n][nn] >> > (itemSet.dst_dev[n][d], itemSet.recvBuffer_dev[n][nn], itemSet.dimChunk, itemSet.dimFull, itemSet.offset, itemSet.swapSize);
            }
        }
        cudaDeviceSynchronize();
    }
}



void testType1A(ItemSet& itemSet)
{
    //5 make
    //1 copy
    //5 take

    for (si32 d = 0; d < DIMSIZE; ++d)
    {
#pragma omp parallel for num_threads(itemSet.nGPU)
        for (si32 n = 0; n < itemSet.nGPU; ++n)
        {
            cudaSetDevice(n);
            for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
            {
                makeSendBuffer_kernel1 << < itemSet.nBlock, itemSet.nThread >> > (itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * d, itemSet.src_dev[n][d], itemSet.dimFull, itemSet.dimChunk, itemSet.offset, itemSet.swapSize);
            }
        }
    }
    cudaDeviceSynchronize();

#pragma omp parallel for num_threads(itemSet.nGPU)
    for (si32 n = 0; n < itemSet.nGPU; ++n)
    {
        for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
        {
            cudaMemcpyAsync(itemSet.recvBuffer_dev[nn][n], itemSet.sendBuffer_dev[n][nn], sizeof(fl64) * itemSet.swapSize * DIMSIZE, cudaMemcpyDeviceToDevice);
        }
    }
    cudaDeviceSynchronize();

    for (si32 d = 0; d < DIMSIZE; ++d)
    {
#pragma omp parallel for num_threads(itemSet.nGPU)
        for (si32 n = 0; n < itemSet.nGPU; ++n)
        {
            cudaSetDevice(n);
            for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
            {
                takeRecvBuffer_kernel1 << < itemSet.nBlock, itemSet.nThread >> > (itemSet.dst_dev[n][d], itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * d, itemSet.dimChunk, itemSet.dimFull, itemSet.offset, itemSet.swapSize);
            }
        }        
    }
    cudaDeviceSynchronize();
}

void testType1B(ItemSet& itemSet)
{
    //5 make
    //1 copy
    //5 take

    for (si32 d = 0; d < DIMSIZE; ++d)
    {
#pragma omp parallel for num_threads(itemSet.nGPU)
        for (si32 n = 0; n < itemSet.nGPU; ++n)
        {
            cudaSetDevice(n);
            for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
            {
                makeSendBuffer_kernel1 << < itemSet.nBlock, itemSet.nThread >> > (itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * d, itemSet.src_dev[n][d], itemSet.dimFull, itemSet.dimChunk, itemSet.offset, itemSet.swapSize);
            }
        }
    }
    cudaDeviceSynchronize();

#pragma omp parallel for num_threads(itemSet.nGPU)
    for (si32 n = 0; n < itemSet.nGPU; ++n)
    {
        for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
        {
            cudaMemcpyAsync(itemSet.recvBuffer_dev[nn][n], itemSet.sendBuffer_dev[n][nn], sizeof(fl64) * itemSet.swapSize * DIMSIZE, cudaMemcpyDeviceToDevice, itemSet.streams[n][nn]);
        }
    }
    cudaDeviceSynchronize();

    for (si32 d = 0; d < DIMSIZE; ++d)
    {
#pragma omp parallel for num_threads(itemSet.nGPU)
        for (si32 n = 0; n < itemSet.nGPU; ++n)
        {
            cudaSetDevice(n);
            for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
            {
                takeRecvBuffer_kernel1 << < itemSet.nBlock, itemSet.nThread >> > (itemSet.dst_dev[n][d], itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * d, itemSet.dimChunk, itemSet.dimFull, itemSet.offset, itemSet.swapSize);
            }
        }
    }
    cudaDeviceSynchronize();
}

void testType1C(ItemSet& itemSet)
{
    //5 make
    //1 copy
    //5 take

    for (si32 d = 0; d < DIMSIZE; ++d)
    {
#pragma omp parallel for num_threads(itemSet.nGPU)
        for (si32 n = 0; n < itemSet.nGPU; ++n)
        {
            cudaSetDevice(n);
            for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
            {
                makeSendBuffer_kernel1 << < itemSet.nBlock, itemSet.nThread, 0, itemSet.streams[n][nn] >> > (itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * d, itemSet.src_dev[n][d], itemSet.dimFull, itemSet.dimChunk, itemSet.offset, itemSet.swapSize);
            }
        }
    }
    cudaDeviceSynchronize();

#pragma omp parallel for num_threads(itemSet.nGPU)
    for (si32 n = 0; n < itemSet.nGPU; ++n)
    {
        for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
        {
            cudaMemcpyAsync(itemSet.recvBuffer_dev[nn][n], itemSet.sendBuffer_dev[n][nn], sizeof(fl64) * itemSet.swapSize * DIMSIZE, cudaMemcpyDeviceToDevice);
        }
    }
    cudaDeviceSynchronize();

    for (si32 d = 0; d < DIMSIZE; ++d)
    {
#pragma omp parallel for num_threads(itemSet.nGPU)
        for (si32 n = 0; n < itemSet.nGPU; ++n)
        {
            cudaSetDevice(n);
            for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
            {
                takeRecvBuffer_kernel1 << < itemSet.nBlock, itemSet.nThread, 0, itemSet.streams[n][nn] >> > (itemSet.dst_dev[n][d], itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * d, itemSet.dimChunk, itemSet.dimFull, itemSet.offset, itemSet.swapSize);
            }
        }
    }
    cudaDeviceSynchronize();
}

void testType1D(ItemSet& itemSet)
{
    //5 make
    //1 copy
    //5 take

    for (si32 d = 0; d < DIMSIZE; ++d)
    {
#pragma omp parallel for num_threads(itemSet.nGPU)
        for (si32 n = 0; n < itemSet.nGPU; ++n)
        {
            cudaSetDevice(n);
            for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
            {
                makeSendBuffer_kernel1 << < itemSet.nBlock, itemSet.nThread, 0, itemSet.streams[n][nn] >> > (itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * d, itemSet.src_dev[n][d], itemSet.dimFull, itemSet.dimChunk, itemSet.offset, itemSet.swapSize);
            }
        }
    }
    cudaDeviceSynchronize();

#pragma omp parallel for num_threads(itemSet.nGPU)
    for (si32 n = 0; n < itemSet.nGPU; ++n)
    {
        for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
        {
            cudaMemcpyAsync(itemSet.recvBuffer_dev[nn][n], itemSet.sendBuffer_dev[n][nn], sizeof(fl64) * itemSet.swapSize * DIMSIZE, cudaMemcpyDeviceToDevice, itemSet.streams[n][nn]);
        }
    }
    cudaDeviceSynchronize();

    for (si32 d = 0; d < DIMSIZE; ++d)
    {
#pragma omp parallel for num_threads(itemSet.nGPU)
        for (si32 n = 0; n < itemSet.nGPU; ++n)
        {
            cudaSetDevice(n);
            for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
            {
                takeRecvBuffer_kernel1 << < itemSet.nBlock, itemSet.nThread, 0, itemSet.streams[n][nn] >> > (itemSet.dst_dev[n][d], itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * d, itemSet.dimChunk, itemSet.dimFull, itemSet.offset, itemSet.swapSize);
            }
        }
    }
    cudaDeviceSynchronize();
}

void testType2A(ItemSet& itemSet)
{
    //1 make
    //1 copy
    //1 take

#pragma omp parallel for num_threads(itemSet.nGPU)
    for (si32 n = 0; n < itemSet.nGPU; ++n)
    {
        cudaSetDevice(n);
        for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
        {
            makeSendBuffer_kernel5 << < itemSet.nBlock, itemSet.nThread >> > (
                itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * 0, 
                itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * 1,
                itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * 2,
                itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * 3,
                itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * 4,
                itemSet.src_dev[n][0], itemSet.src_dev[n][1], itemSet.src_dev[n][2], itemSet.src_dev[n][3], itemSet.src_dev[n][4],
                itemSet.dimFull, itemSet.dimChunk, itemSet.offset, itemSet.swapSize);
        }
    }
    cudaDeviceSynchronize();

#pragma omp parallel for num_threads(itemSet.nGPU)
    for (si32 n = 0; n < itemSet.nGPU; ++n)
    {
        for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
        {
            cudaMemcpyAsync(itemSet.recvBuffer_dev[nn][n], itemSet.sendBuffer_dev[n][nn], sizeof(fl64) * itemSet.swapSize * DIMSIZE, cudaMemcpyDeviceToDevice);
        }
    }
    cudaDeviceSynchronize();

    
#pragma omp parallel for num_threads(itemSet.nGPU)
    for (si32 n = 0; n < itemSet.nGPU; ++n)
    {
        cudaSetDevice(n);
        for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
        {
            takeRecvBuffer_kernel5 << < itemSet.nBlock, itemSet.nThread >> > (
                itemSet.dst_dev[n][0], itemSet.dst_dev[n][1], itemSet.dst_dev[n][2], itemSet.dst_dev[n][3], itemSet.dst_dev[n][4],
                itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * 0,
                itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * 1,
                itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * 2,
                itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * 3,
                itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * 4,
                itemSet.dimChunk, itemSet.dimFull, itemSet.offset, itemSet.swapSize);
        }
    }
    cudaDeviceSynchronize();
}

void testType2B(ItemSet& itemSet)
{
    //1 make
    //1 copy
    //1 take

#pragma omp parallel for num_threads(itemSet.nGPU)
    for (si32 n = 0; n < itemSet.nGPU; ++n)
    {
        cudaSetDevice(n);
        for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
        {
            makeSendBuffer_kernel5 << < itemSet.nBlock, itemSet.nThread >> > (
                itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * 0,
                itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * 1,
                itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * 2,
                itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * 3,
                itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * 4,
                itemSet.src_dev[n][0], itemSet.src_dev[n][1], itemSet.src_dev[n][2], itemSet.src_dev[n][3], itemSet.src_dev[n][4],
                itemSet.dimFull, itemSet.dimChunk, itemSet.offset, itemSet.swapSize);
        }
    }
    cudaDeviceSynchronize();

#pragma omp parallel for num_threads(itemSet.nGPU)
    for (si32 n = 0; n < itemSet.nGPU; ++n)
    {
        for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
        {
            cudaMemcpyAsync(itemSet.recvBuffer_dev[nn][n], itemSet.sendBuffer_dev[n][nn], sizeof(fl64) * itemSet.swapSize * DIMSIZE, cudaMemcpyDeviceToDevice, itemSet.streams[n][nn]);
        }
    }
    cudaDeviceSynchronize();


#pragma omp parallel for num_threads(itemSet.nGPU)
    for (si32 n = 0; n < itemSet.nGPU; ++n)
    {
        cudaSetDevice(n);
        for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
        {
            takeRecvBuffer_kernel5 << < itemSet.nBlock, itemSet.nThread >> > (
                itemSet.dst_dev[n][0], itemSet.dst_dev[n][1], itemSet.dst_dev[n][2], itemSet.dst_dev[n][3], itemSet.dst_dev[n][4],
                itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * 0,
                itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * 1,
                itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * 2,
                itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * 3,
                itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * 4,
                itemSet.dimChunk, itemSet.dimFull, itemSet.offset, itemSet.swapSize);
        }
    }
    cudaDeviceSynchronize();
}

void testType2C(ItemSet& itemSet)
{
    //1 make
    //1 copy
    //1 take

#pragma omp parallel for num_threads(itemSet.nGPU)
    for (si32 n = 0; n < itemSet.nGPU; ++n)
    {
        cudaSetDevice(n);
        for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
        {
            makeSendBuffer_kernel5 << < itemSet.nBlock, itemSet.nThread, 0, itemSet.streams[n][nn] >> > (
                itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * 0,
                itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * 1,
                itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * 2,
                itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * 3,
                itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * 4,
                itemSet.src_dev[n][0], itemSet.src_dev[n][1], itemSet.src_dev[n][2], itemSet.src_dev[n][3], itemSet.src_dev[n][4],
                itemSet.dimFull, itemSet.dimChunk, itemSet.offset, itemSet.swapSize);
        }
    }
    cudaDeviceSynchronize();

#pragma omp parallel for num_threads(itemSet.nGPU)
    for (si32 n = 0; n < itemSet.nGPU; ++n)
    {
        for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
        {
            cudaMemcpyAsync(itemSet.recvBuffer_dev[nn][n], itemSet.sendBuffer_dev[n][nn], sizeof(fl64) * itemSet.swapSize * DIMSIZE, cudaMemcpyDeviceToDevice);
        }
    }
    cudaDeviceSynchronize();


#pragma omp parallel for num_threads(itemSet.nGPU)
    for (si32 n = 0; n < itemSet.nGPU; ++n)
    {
        cudaSetDevice(n);
        for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
        {
            takeRecvBuffer_kernel5 << < itemSet.nBlock, itemSet.nThread, 0, itemSet.streams[n][nn] >> > (
                itemSet.dst_dev[n][0], itemSet.dst_dev[n][1], itemSet.dst_dev[n][2], itemSet.dst_dev[n][3], itemSet.dst_dev[n][4],
                itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * 0,
                itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * 1,
                itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * 2,
                itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * 3,
                itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * 4,
                itemSet.dimChunk, itemSet.dimFull, itemSet.offset, itemSet.swapSize);
        }
    }
    cudaDeviceSynchronize();
}

void testType2D(ItemSet& itemSet)
{
    //1 make
    //1 copy
    //1 take

#pragma omp parallel for num_threads(itemSet.nGPU)
    for (si32 n = 0; n < itemSet.nGPU; ++n)
    {
        cudaSetDevice(n);
        for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
        {
            makeSendBuffer_kernel5 << < itemSet.nBlock, itemSet.nThread, 0, itemSet.streams[n][nn] >> > (
                itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * 0,
                itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * 1,
                itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * 2,
                itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * 3,
                itemSet.sendBuffer_dev[n][nn] + itemSet.swapSize * 4,
                itemSet.src_dev[n][0], itemSet.src_dev[n][1], itemSet.src_dev[n][2], itemSet.src_dev[n][3], itemSet.src_dev[n][4],
                itemSet.dimFull, itemSet.dimChunk, itemSet.offset, itemSet.swapSize);
        }
    }
    cudaDeviceSynchronize();

#pragma omp parallel for num_threads(itemSet.nGPU)
    for (si32 n = 0; n < itemSet.nGPU; ++n)
    {
        for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
        {
            cudaMemcpyAsync(itemSet.recvBuffer_dev[nn][n], itemSet.sendBuffer_dev[n][nn], sizeof(fl64) * itemSet.swapSize * DIMSIZE, cudaMemcpyDeviceToDevice, itemSet.streams[n][nn]);
        }
    }
    cudaDeviceSynchronize();


#pragma omp parallel for num_threads(itemSet.nGPU)
    for (si32 n = 0; n < itemSet.nGPU; ++n)
    {
        cudaSetDevice(n);
        for (si32 nn = 0; nn < itemSet.nGPU; ++nn)
        {
            takeRecvBuffer_kernel5 << < itemSet.nBlock, itemSet.nThread, 0, itemSet.streams[n][nn] >> > (
                itemSet.dst_dev[n][0], itemSet.dst_dev[n][1], itemSet.dst_dev[n][2], itemSet.dst_dev[n][3], itemSet.dst_dev[n][4],
                itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * 0,
                itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * 1,
                itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * 2,
                itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * 3,
                itemSet.recvBuffer_dev[n][nn] + itemSet.swapSize * 4,
                itemSet.dimChunk, itemSet.dimFull, itemSet.offset, itemSet.swapSize);
        }
    }
    cudaDeviceSynchronize();
}

void getResult(const si32 iterMax, ItemSet& itemSet, const string fnName, decltype(testType0A) func)
{
    cout << fnName;
    chrono::system_clock::time_point start = chrono::system_clock::now();
    chrono::system_clock::time_point end;
    chrono::microseconds microSec;

    for (si32 iter = 0; iter < iterMax; ++iter)
    {
        func(itemSet);

        if (cudaGetLastError() != 0)
        {
            cout << cudaGetErrorName(cudaGetLastError()) << endl;
        }
        cudaDeviceSynchronize();
    }

    end = chrono::system_clock::now();
    microSec = chrono::duration_cast<chrono::microseconds>(end - start);

    cout << " calc time elap : " << setw(12) << (microSec.count() / 1000000.0);
    cout << " | iter per sec : " << setw(10) << iterMax / (microSec.count() / 1000000.0);
    cout << endl;
}

int main()
{
    si32 nDevices;
    cudaGetDeviceCount(&nDevices);

    si32 nx = 181;
    si32 ny = 181;
    si32 nz = 181;

    const si32 iterMax = 1000;

    for (si32 nGPU = 1; nGPU <= nDevices; ++nGPU)
    {
        cout << "using " << nGPU << " GPU" << endl;
        ItemSet  itemSet(nx, ny, nz, bufSize, nGPU, 256);

        getResult(iterMax, itemSet, sym2str(testType0A), testType0A);
        getResult(iterMax, itemSet, sym2str(testType0B), testType0B);
        getResult(iterMax, itemSet, sym2str(testType0C), testType0C);
        getResult(iterMax, itemSet, sym2str(testType0D), testType0D);

        getResult(iterMax, itemSet, sym2str(testType1A), testType1A);
        getResult(iterMax, itemSet, sym2str(testType1B), testType1B);
        getResult(iterMax, itemSet, sym2str(testType1C), testType1C);
        getResult(iterMax, itemSet, sym2str(testType1D), testType1D);

        getResult(iterMax, itemSet, sym2str(testType2A), testType2A);
        getResult(iterMax, itemSet, sym2str(testType2B), testType2B);
        getResult(iterMax, itemSet, sym2str(testType2C), testType2C);
        getResult(iterMax, itemSet, sym2str(testType2D), testType2D);

        cout << endl;
    }
    
    return 0;
}
