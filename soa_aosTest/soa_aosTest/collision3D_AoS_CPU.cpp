#include "DataSet3D.h"
fl64 genRhoAoS_CPU(const Cell3D& cell)
{
	fl64 rho = 0;


	rho += cell.f[0];
	rho += cell.f[1];
	rho += cell.f[2];
	rho += cell.f[3];
	rho += cell.f[4];
	rho += cell.f[5];
	rho += cell.f[6];
	rho += cell.f[7];
	rho += cell.f[8];
	rho += cell.f[9];
	rho += cell.f[10];
	rho += cell.f[10];
	rho += cell.f[11];
	rho += cell.f[12];
	rho += cell.f[13];
	rho += cell.f[14];
	rho += cell.f[15];
	rho += cell.f[16];
	rho += cell.f[17];

	return rho;
}
fl64 genUAoS_CPU(const Cell3D& cell, const fl64 rho)
{
	fl64 u = 0;
	u += (cell.f[1] + cell.f[7] + cell.f[9] + cell.f[11] + cell.f[3]);
	u -= (cell.f[2] + cell.f[8] + cell.f[10] + cell.f[12] + cell.f[14]);
	u /= rho;
	return u;
}
fl64 genVAoS_CPU(const Cell3D& cell, const fl64 rho)
{
	fl64 v = 0;
	v += (cell.f[3] + cell.f[7] + cell.f[8] + cell.f[15] + cell.f[17]);
	v -= (cell.f[4] + cell.f[9] + cell.f[10] + cell.f[16] + cell.f[18]);
	v /= rho;
	return v;
}
fl64 genWAoS_CPU(const Cell3D& cell, const fl64 rho)
{
	fl64 w = 0;
	w += (cell.f[5] + cell.f[11] + cell.f[12] + cell.f[15] + cell.f[16]);
	w -= (cell.f[6] + cell.f[13] + cell.f[14] + cell.f[17] + cell.f[18]);
	w /= rho;
	return w;
}

void DataSet3D::collision3D_AoS_CPU()
{
	const fl64  w_rho[3] = {
				12.0 / 36,
				2.0 / 36,
				1.0 / 36 };

	const si32 direcCategory[19] = { 0,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2 };

	const fl64 direcC[19][3] = {
		{0.0,0.0,0.0},
		{1.0,0.0,0.0 },
		{-1.0, 0.0,0.0},
		{0.0,1.0,0.0},
		{0.0,-1.0,0.0 },
		{0.0, 0.0,1.0},
		{0.0, 0.0,-1.0},

		{1.0, 1.0,0.0 },
		{-1.0,1.0,0.0 },
		{1.0,-1.0,0.0 },
		{-1.0,-1.0,0.0 },

		{1.0,0.0,1.0 },
		{-1.0,0.0,1.0 },
		{1.0,0.0,-1.0 },
		{-1.0,0.0,-1.0 },

		{0.0,1.0,1.0 },
		{0.0,-1.0,1.0 },
		{0.0,1.0,-1.0 },
		{0.0,-1.0,-1.0 }
	};

	const fl64 csSq = 1.0 / 3;

#pragma omp parallel for schedule(guided) num_threads(nCore)
	for (si32 indCur = 0; indCur < nNode; ++indCur)
	{
		auto& cellCur = dataCPU_AoS[indCur];
		const si32 indOffset = indCur * 19;

		const fl64 rho = genRhoAoS_CPU(cellCur);

		fl64 vel[3] = { genUAoS_CPU(cellCur,rho), genVAoS_CPU(cellCur,rho), genWAoS_CPU(cellCur,rho) };

		const fl64 rhoRecipocal = 1.0 / rho;

		fl64 u_dot_u = vel[0] * vel[0] + vel[1] * vel[1] + vel[2] * vel[2];

		for (si32 i = 0; i < 19; ++i)
		{
			const fl64 wTemp = w_rho[direcCategory[i]];
			const fl64 c[3] = { direcC[i][0],direcC[i][1],direcC[i][2] };
			const fl64 dotVal = (c[0] * vel[0] + c[1] * vel[1] + c[2] * vel[2]);

			const fl64 eq = wTemp * rho * (1. + 3.0 * dotVal + 4.5 * (dotVal * dotVal) - 1.5 * u_dot_u);

			cellCur.f[i] = (1 - s_v) * eq + s_v * cellCur.f[i];
		}
	}
}
