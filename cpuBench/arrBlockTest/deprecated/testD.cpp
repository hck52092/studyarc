#include "Arr3D.h"

void testD(const si32 nMaxCore)
{
	const si32 arrSizeMax = 512;

	ofstream fout;
	fout.open("testD.txt");

	fout << fixed << setprecision(15);
	fout
		<< setw(5) << "nCore"
		<< setw(10) << "arrSize"
		<< setw(15) << "totalLen"
		<< setw(25) << "dtIJK"
		<< setw(25) << "dtKJI"		
		<< setw(25) << "dtIJK_S"
		<< setw(25) << "dtKJI_S"				
		<< setw(20) << "sum"
		<< setw(20) << "checkSum"
		<< setw(10) << "diff" << endl;

	Arr3D<fl64> gridBase;
	for (si32 arrSize = arrSizeMax; arrSize > 0; --arrSize)
	{
		gridBase.resize(arrSize, arrSize, arrSize);

		for (si32 nCore = 1; nCore <= nMaxCore; ++nCore)
		{
			si32 totalLen = gridBase.byteData.size();	
			si32 iterMax = 1;

			si64 sum[4]{ 0 };
			fl64 dt[4] = { 0 };
			
			{
				chrono::system_clock::time_point start, end;
				chrono::nanoseconds nanoSec;
				start = chrono::system_clock::now();

#pragma omp parallel for schedule(guided) num_threads(nCore) collapse(3)
				for (si32 k = 0; k < arrSize; ++k)
				{
					for (si32 j = 0; j < arrSize; ++j)
					{
						for (si32 i = 0; i < arrSize; ++i)
						{
							gridBase(i, j, k) =
								(i)
								+(j)*arrSize
								+ (k)*arrSize * arrSize;
						}
					}
				}

				end = chrono::system_clock::now();
				nanoSec = chrono::duration_cast<chrono::nanoseconds>(end - start);
				dt[0] = nanoSec.count() / 1000000000.0;

				for (const auto& v : gridBase.byteData)
				{
					sum[0] += v;
				}
			}
			{
				chrono::system_clock::time_point start, end;
				chrono::nanoseconds nanoSec;
				start = chrono::system_clock::now();

#pragma omp parallel for schedule(guided) num_threads(nCore) collapse(3)
				for (si32 i = 0; i < arrSize; ++i)
				{				
					for (si32 j = 0; j < arrSize; ++j)
					{
						for (si32 k = 0; k < arrSize; ++k)
						{						
							gridBase(i, j, k) =
								(i)
								+(j)*arrSize
								+ (k)*arrSize * arrSize;
						}
					}
				}

				end = chrono::system_clock::now();
				nanoSec = chrono::duration_cast<chrono::nanoseconds>(end - start);
				dt[1] = nanoSec.count() / 1000000000.0;

				for (const auto& v : gridBase.byteData)
				{
					sum[1] += v;
				}
			}
			{
				chrono::system_clock::time_point start, end;
				chrono::nanoseconds nanoSec;
				start = chrono::system_clock::now();

#pragma omp parallel for schedule(guided) num_threads(nCore)
				for (si32 ind = 0; ind < totalLen; ++ind)
				{
					si32 iTmp = ind;
					const si32 i = iTmp % arrSize; iTmp /= arrSize;
					const si32 j = iTmp % arrSize; iTmp /= arrSize;
					const si32 k = iTmp;

					gridBase(i, j, k) =
						(i)
						+(j)*arrSize
						+ (k)*arrSize * arrSize;
				}

				end = chrono::system_clock::now();
				nanoSec = chrono::duration_cast<chrono::nanoseconds>(end - start);
				dt[2] = nanoSec.count() / 1000000000.0;

				for (const auto& v : gridBase.byteData)
				{
					sum[2] += v;
				}
			}
			{
				chrono::system_clock::time_point start, end;
				chrono::nanoseconds nanoSec;
				start = chrono::system_clock::now();

#pragma omp parallel for schedule(guided) num_threads(nCore)
				for (si32 ind = 0; ind < totalLen; ++ind)
				{
					si32 iTmp = ind;
					const si32 k = iTmp % arrSize; iTmp /= arrSize;
					const si32 j = iTmp % arrSize; iTmp /= arrSize;
					const si32 i = iTmp;

					gridBase(i, j, k) =
						(i)
						+(j)*arrSize
						+ (k)*arrSize * arrSize;
				}

				end = chrono::system_clock::now();
				nanoSec = chrono::duration_cast<chrono::nanoseconds>(end - start);
				dt[3] = nanoSec.count() / 1000000000.0;

				for (const auto& v : gridBase.byteData)
				{
					sum[3] += v;
				}
			}

			ui64 checkSum = pow(ui64(arrSize), 3) * (pow(ui64(arrSize), 3) - 1) / 2;

			fout
				<< setw(5) << nCore
				<< setw(10) << arrSize
				<< setw(15) << totalLen
				<< setw(25) << dt[0] / iterMax
				<< setw(25) << dt[1] / iterMax
				<< setw(25) << dt[2] / iterMax
				<< setw(25) << dt[3] / iterMax
				<< setw(20) << sum[0]
				<< setw(20) << checkSum
				<< setw(10) << checkSum - sum[0] << endl;
		}
	}
}