#pragma once
#include <array>
#include <vector>
#include <map>
#include <set>
#include <stack>
#include <queue>
#include <tuple>
#include <string>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <chrono>
#include <cmath>

#ifdef _WIN32
#define DELIMITERSTR "\\"
#include <direct.h>
#else
#define DELIMITERSTR "/"
#include <sys/stat.h>
#define _mkdir(fn) mkdir(fn, 0755)
#endif

using namespace std;

typedef uint8_t ui08;
typedef int8_t si08;
typedef uint16_t ui16;
typedef int16_t si16;
typedef uint32_t ui32;
typedef int32_t si32;
typedef uint64_t ui64;
typedef int64_t si64;
typedef float fl32;
typedef double fl64;
