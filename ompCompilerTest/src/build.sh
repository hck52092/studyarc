#!/bin/bash

echo -n "OBJS := " > Makefile

find ./ -name \*.cpp | sed 's/cpp$/o \\/' >> Makefile
echo >> Makefile

(
cat << EOF 
CXXFLAGS := -O2 -DNDEBUG -std=c++1y -fopenmp \$(CXXFLAGS)
LDFLAGS := 

all: omp
	
omp : \$(OBJS)
	g++ \$(CXXFLAGS) \$(LDFLAGS) \$(OBJS) -o ompTest

clean:
	rm -fv \$(OBJS)
EOF
) >> Makefile

make clean
make -j$(nproc)

