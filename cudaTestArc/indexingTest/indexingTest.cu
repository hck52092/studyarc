#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <array>
#include <tuple>
#include <algorithm>
#include <chrono>

using namespace std;

using ui08 = unsigned char;
using si32 = int;
using si64 = long long;
using fl32 = float;
using fl64 = double;


si32 getBlockSize(const si32 totalSize, const si32 stepSize)
{
    return (totalSize + stepSize - 1) / stepSize;
}

template<typename T>
__device__ T calcFunc(const T tmp)
{
    T res = 1;
    
    for (si32 s = 0; s < 512; ++s)
    {
        res *= tmp;
        res += tmp;
        res /= tmp;
        res -= tmp;
    }

    return res;
}
template<typename T>
__global__ void kernel1D(T* dst, const T* src, const int3 dimData, const int3 dimFull, const int3 offset, const si32 nSize)
{
    si32 indCur = blockIdx.x * blockDim.x + threadIdx.x;

    if (indCur < nSize)
    {
        const si32 i = indCur % dimData.x + offset.x;
        indCur /= dimData.x;
        const si32 j = indCur % dimData.y + offset.y;
        const si32 k = indCur / dimData.y + offset.z;
        const si32 indFull = i + dimFull.x * (j + dimFull.y * k);

        dst[indFull] = calcFunc(src[indFull]);
    }
}
template<typename T>
__global__ void kernel3D(T* dst, const T* src, const int3 dimData, const int3 dimFull, const int3 offset, const si32 nSize)
{
    si32 i = blockIdx.x * blockDim.x + threadIdx.x;
    si32 j = blockIdx.y * blockDim.y + threadIdx.y;
    si32 k = blockIdx.z * blockDim.z + threadIdx.z;

    if (i < dimFull.x && j < dimFull.y && k < dimFull.z)
    {
        const si32 indFull = i + dimFull.x * (j + dimFull.y * k);

        dst[indFull] = calcFunc(src[indFull]);
    }
}

template<typename T>
__global__ void kernelStrided(T* dst, const T* src, const int3 dimData, const int3 dimFull, const int3 offset, const si32 nSize)
{
    const si32 indOffset = blockDim.x * gridDim.x;

    for (si32 indCur = blockIdx.x * blockDim.x + threadIdx.x; indCur < nSize; indCur += indOffset)
    {
        const si32 i = indCur % dimData.x + offset.x;
        const si32 tmp = indCur / dimData.x;
        const si32 j = tmp % dimData.y + offset.y;
        const si32 k = tmp / dimData.y + offset.z;

        const si32 indFull = i + dimFull.x * (j + dimFull.y * k);

        dst[indFull] = calcFunc(src[indFull]);
    }
}

using ResultItem = tuple<fl64, fl64, bool>;

template<typename T>
ResultItem execFuncTest(decltype(kernel1D<T>) funcPtr,
    const si32 iterMax,
    const vector<T>& src_h, vector<T>& dst_h,
    const T* src_d, T* dst_d,
    const dim3 nBlock, const dim3 nThread,
    const int3 dimData, const int3 dimFull, const int3 offset, const si32 nSize
)
{
    chrono::system_clock::time_point start, end;

    fill(dst_h.begin(), dst_h.end(), T(0));

    start = chrono::system_clock::now();
    for (si32 iter = 0; iter < iterMax; ++iter)
    {
        funcPtr << <nBlock, nThread >> > (dst_d, src_d, dimData, dimFull, offset, nSize);
    }
    cudaDeviceSynchronize();
    end = chrono::system_clock::now();

    bool isValid = (cudaGetLastError() == 0);
    if (!isValid)
    {
        cout << cudaGetErrorString(cudaGetLastError()) << endl;
    }

    cudaMemcpy(dst_h.data(), dst_d, nSize * sizeof(T), cudaMemcpyDeviceToHost);

    auto timeElap = chrono::duration_cast<chrono::nanoseconds>(end - start).count() / 10e9;

    return make_tuple(timeElap, timeElap / iterMax, isValid);
}

template<typename T>
vector<array<ResultItem, 3>> execSettingItemTest(const si32 dx, const si32 dy, const si32 dz, const si32 iterMax, const si32 numSMs, const si32 warpSize)
{
    const si32 bufSize = 12;
    const si32 totalLen = dx * dy * dz;
    const si32 arrLen = (dx + bufSize * 2 + 1) * (dy + bufSize * 2 + 1) * (dz + bufSize * 2 + 1);
    vector<T> src_h(arrLen);
    vector<T> out_h(arrLen);

    vector<array<ResultItem, 3>> ret;

    T* src_d;
    T* out_d;

    cudaMalloc(&src_d, sizeof(T) * arrLen);
    cudaMalloc(&out_d, sizeof(T) * arrLen);

    for (si32 i = 0; i < arrLen; ++i)
    {
        src_h[i] = i % 4096 + 1;
    }

    cudaMemcpy(src_d, src_h.data(), sizeof(T) * totalLen, cudaMemcpyHostToDevice);

    const int3 dimData{ dx, dy, dz };
    const int3 dimFull{ dx + bufSize * 2 + 1, dy + bufSize * 2 + 1, dz + bufSize * 2 + 1 };
    const int3 offset{ bufSize, bufSize, bufSize };
    for (si32 n = 0; n <= 10; ++n)
    {
        si32 nT = pow(2, n);
        if (nT * nT * nT > 1024)
        {
            break;
        }

        const dim3 nThread1D(nT * nT * nT, 1, 1);
        const dim3 nBlock1D(getBlockSize(totalLen, nT), 1, 1);

        const dim3 nThread3D(nT, nT, nT);
        const dim3 nBlock3D(getBlockSize(dx, nT), getBlockSize(dy, nT), getBlockSize(dz, nT));

        const dim3 nThreadStrided(warpSize * 16, 1, 1);
        const dim3 nBlockStrided(numSMs * warpSize, 1, 1);

        ret.emplace_back();

        //warm-wp
        execFuncTest(kernelStrided<T>, 1,
            src_h, out_h, src_d, out_d, nBlockStrided, nThreadStrided, dimData, dimFull, offset, totalLen);

        ret[n][2] = execFuncTest(kernelStrided<T>, iterMax,
            src_h, out_h, src_d, out_d, nBlockStrided, nThreadStrided, dimData, dimFull, offset, totalLen);

        ret[n][1] = execFuncTest(kernel3D<T>, iterMax,
            src_h, out_h, src_d, out_d, nBlock3D, nThread3D, dimData, dimFull, offset, totalLen);

        ret[n][0] = execFuncTest(kernel1D<T>, iterMax,
            src_h, out_h, src_d, out_d, nBlock1D, nThread1D, dimData, dimFull, offset, totalLen);
    }

    cudaFree(src_d);
    cudaFree(out_d);

    return ret;
}

template<typename T>
void execTypeTest(const string& typeName, const si32 powMax, const si32 iterMax, const si32 numSMs, const si32 warpSize)
{
    cout << typeName << endl;
    for (si32 s = powMax / 2; s <= powMax; ++s)
    {
        //const si32 dx = pow(2, s / 3);
        //const si32 dy = pow(2, s / 3);
        //const si32 dz = pow(2, s / 3);
        const si32 dx = s * 10;
        const si32 dy = s * 10;
        const si32 dz = s * 10;

        auto ret = execSettingItemTest<T>(dx, dy, dz, iterMax, numSMs, warpSize);

        for (si32 n = 0; n < ret.size(); ++n)
        {
            cout << setw(15) << dx << setw(15) << dy << setw(15) << dz
                << setw(15) << si32(pow(pow(2, n), 3))
                << setw(20) << get<0>(ret[n][0])
                << setw(20) << get<0>(ret[n][1])
                << setw(20) << get<0>(ret[n][2])
                << setw(10) << get<2>(ret[n][0])
                << setw(10) << get<2>(ret[n][1])
                << setw(10) << get<2>(ret[n][2])
                << setw(40) << 512 * 4.0 * dx * dy * dz / get<1>(ret[n][0]) / 10e9
                << setw(40) << 512 * 4.0 * dx * dy * dz / get<1>(ret[n][1]) / 10e9
                << setw(40) << 512 * 4.0 * dx * dy * dz / get<1>(ret[n][2]) / 10e9
                << endl;
        }
    }
}
int main()
{
    cout << boolalpha;
    cout << fixed;
    cout << setprecision(12);

    si32 nDevices;
    cudaGetDeviceCount(&nDevices);

    cout << "num Devices = " << nDevices << endl;

    si32 devId = nDevices - 1;
    cudaSetDevice(devId);

    si32 numSMs;
    si32 warpSize;
    cudaDeviceGetAttribute(&numSMs, cudaDevAttrMultiProcessorCount, devId);
    cudaDeviceGetAttribute(&warpSize, cudaDevAttrWarpSize, devId);

    cout << "numSMs = " << numSMs << endl;
    cout << "warpSz = " << warpSize << endl;

    const si32 iterMax = 1;
    const si32 powMax = 24;

    {
        execTypeTest<si32>("si32", powMax, iterMax, numSMs, warpSize);
    }
    {
        execTypeTest<si64>("si64", powMax, iterMax, numSMs, warpSize);
    }
    {
        execTypeTest<fl32>("fl32", powMax, iterMax, numSMs, warpSize);
    }
    {
        execTypeTest<fl64>("fl64", powMax, iterMax, numSMs, warpSize);
    }
}
