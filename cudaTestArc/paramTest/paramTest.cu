#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <array>
#include <tuple>
#include <algorithm>
#include <chrono>

using namespace std;

using ui08 = unsigned char;
using si16 = short;
using si32 = int;
using si64 = long long;
using fl32 = float;
using fl64 = double;


si32 getBlockSize(const si32 totalSize, const si32 stepSize)
{
    return (totalSize + stepSize - 1) / stepSize;
}

template<typename T>
__global__ void testKernel01(const si32 n, const si32 iterMax, T* dst, const T* src01)
{
    si32 i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < n)
    {
        T res = 0;
#pragma unroll
        for (si32 iter = 0; iter < iterMax; ++iter)
        {
            res += src01[i];
        }

        dst[i] += res;
    }
}
template<typename T>
__global__ void testKernel02(const si32 n, const si32 iterMax, T* dst, const T* src01, const T* src02)
{
    si32 i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < n)
    {
        T res = 0;
#pragma unroll
        for (si32 iter = 0; iter < iterMax; ++iter)
        {
            res += src01[i]; res += src02[i];
        }

        dst[i] += res;
    }
}
template<typename T>
__global__ void testKernel04(const si32 n, const si32 iterMax, T* dst, const T* src01, const T* src02, const T* src03, const T* src04)
{
    si32 i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < n)
    {
        T res = 0;
#pragma unroll
        for (si32 iter = 0; iter < iterMax; ++iter)
        {
            res += src01[i]; res += src02[i]; res += src03[i]; res += src04[i];
        }

        dst[i] += res;
    }
}
template<typename T>
__global__ void testKernel08(const si32 n, const si32 iterMax, T* dst, const T* src01, const T* src02, const T* src03, const T* src04, const T* src05, const T* src06, const T* src07, const T* src08)
{
    si32 i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < n)
    {
        T res = 0;

#pragma unroll
        for (si32 iter = 0; iter < iterMax; ++iter)
        {
            res += src01[i]; res += src02[i]; res += src03[i]; res += src04[i]; res += src05[i]; res += src06[i]; res += src07[i]; res += src08[i];
        }

        dst[i] += res;
    }
}
template<typename T>
__global__ void testKernel16(const si32 n, const si32 iterMax, T* dst,
    const T* src01, const T* src02, const T* src03, const T* src04, const T* src05, const T* src06, const T* src07, const T* src08,
    const T* src09, const T* src10, const T* src11, const T* src12, const T* src13, const T* src14, const T* src15, const T* src16)
{
    si32 i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < n)
    {
        T res = 0;
#pragma unroll
        for (si32 iter = 0; iter < iterMax; ++iter)
        {
            res += src01[i]; res += src02[i]; res += src03[i]; res += src04[i]; res += src05[i]; res += src06[i]; res += src07[i]; res += src08[i];
            res += src09[i]; res += src10[i]; res += src11[i]; res += src12[i]; res += src13[i]; res += src14[i]; res += src15[i]; res += src16[i];
        }

        dst[i] += res;
    }
}
template<typename T>
__global__ void testKernel32(const si32 n, const si32 iterMax, T* dst,
    const T* src01, const T* src02, const T* src03, const T* src04, const T* src05, const T* src06, const T* src07, const T* src08,
    const T* src09, const T* src10, const T* src11, const T* src12, const T* src13, const T* src14, const T* src15, const T* src16,
    const T* src17, const T* src18, const T* src19, const T* src20, const T* src21, const T* src22, const T* src23, const T* src24,
    const T* src25, const T* src26, const T* src27, const T* src28, const T* src29, const T* src30, const T* src31, const T* src32)
{
    si32 i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < n)
    {
        T res = 0;
#pragma unroll
        for (si32 iter = 0; iter < iterMax; ++iter)
        {
            res += src01[i]; res += src02[i]; res += src03[i]; res += src04[i]; res += src05[i]; res += src06[i]; res += src07[i]; res += src08[i];
            res += src09[i]; res += src10[i]; res += src11[i]; res += src12[i]; res += src13[i]; res += src14[i]; res += src15[i]; res += src16[i];
            res += src17[i]; res += src18[i]; res += src19[i]; res += src20[i]; res += src21[i]; res += src22[i]; res += src23[i]; res += src24[i];
            res += src25[i]; res += src26[i]; res += src27[i]; res += src28[i]; res += src29[i]; res += src30[i]; res += src31[i]; res += src32[i];
        }

        dst[i] += res;
    }
}
template<typename T>
__global__ void testKernel64(const si32 n, const si32 iterMax, T* dst,
    const T* src01, const T* src02, const T* src03, const T* src04, const T* src05, const T* src06, const T* src07, const T* src08,
    const T* src09, const T* src10, const T* src11, const T* src12, const T* src13, const T* src14, const T* src15, const T* src16,
    const T* src17, const T* src18, const T* src19, const T* src20, const T* src21, const T* src22, const T* src23, const T* src24,
    const T* src25, const T* src26, const T* src27, const T* src28, const T* src29, const T* src30, const T* src31, const T* src32,
    const T* src33, const T* src34, const T* src35, const T* src36, const T* src37, const T* src38, const T* src39, const T* src40,
    const T* src41, const T* src42, const T* src43, const T* src44, const T* src45, const T* src46, const T* src47, const T* src48,
    const T* src49, const T* src50, const T* src51, const T* src52, const T* src53, const T* src54, const T* src55, const T* src56,
    const T* src57, const T* src58, const T* src59, const T* src60, const T* src61, const T* src62, const T* src63, const T* src64)
{
    si32 i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < n)
    {
        T res = 0;
#pragma unroll
        for (si32 iter = 0; iter < iterMax; ++iter)
        {
            res += src01[i]; res += src02[i]; res += src03[i]; res += src04[i]; res += src05[i]; res += src06[i]; res += src07[i]; res += src08[i];
            res += src09[i]; res += src10[i]; res += src11[i]; res += src12[i]; res += src13[i]; res += src14[i]; res += src15[i]; res += src16[i];
            res += src17[i]; res += src18[i]; res += src19[i]; res += src20[i]; res += src21[i]; res += src22[i]; res += src23[i]; res += src24[i];
            res += src25[i]; res += src26[i]; res += src27[i]; res += src28[i]; res += src29[i]; res += src30[i]; res += src31[i]; res += src32[i];
            res += src33[i]; res += src34[i]; res += src35[i]; res += src36[i]; res += src37[i]; res += src38[i]; res += src39[i]; res += src40[i];
            res += src41[i]; res += src42[i]; res += src43[i]; res += src44[i]; res += src45[i]; res += src46[i]; res += src47[i]; res += src48[i];
            res += src49[i]; res += src50[i]; res += src51[i]; res += src52[i]; res += src53[i]; res += src54[i]; res += src55[i]; res += src56[i];
            res += src57[i]; res += src58[i]; res += src59[i]; res += src60[i]; res += src61[i]; res += src62[i]; res += src63[i]; res += src64[i];
        }

        dst[i] += res;
    }
}

const si32 MAXKERNEL = 32;

using ResultItem = tuple<fl64, fl64>;

template<typename T>
class FuncTester
{
    si32 totalSize;
    si32 threadSize;
    si32 nBlock;
    si32 streamSize;
    si32 maxParam;
    si32 iterMax;
    bool isStreamBased;
    bool isPinned;
    bool isOnMemory;

    vector<ResultItem> ret;
    vector<cudaStream_t> streamSet;
    vector<vector<vector<T>>> src_h;
    vector<vector<T*>> src_d;
    vector<vector<T>> out_h;
    vector<T*> out_d;

    void initMem()
    {
        if (isStreamBased)
        {
            streamSet.resize(streamSize);
            for (si32 n = 0; n < streamSize; ++n)
            {
                cudaStreamCreate(&streamSet[n]);
            }
        }

        src_h.resize(streamSize);
        src_d.resize(streamSize);
        out_h.resize(streamSize);
        out_d.resize(streamSize);

        for (si32 n = 0; n < streamSize; ++n)
        {
            src_h[n].resize(maxParam);
            src_d[n].resize(maxParam);
            out_h[n].resize(totalSize);
            cudaMalloc(&out_d[n], sizeof(T) * totalSize);

            for (si32 s = 0; s < maxParam; ++s)
            {
                src_h[n][s].resize(totalSize);
                cudaMalloc(&src_d[n][s], sizeof(T) * totalSize);

                for (si32 i = 0; i < totalSize; ++i)
                {
                    src_h[n][s][i] = i % 4096 + 1;
                }

                cudaMemcpy(src_d[n][s], src_h[n][s].data(), sizeof(T) * totalSize, cudaMemcpyHostToDevice);
            }
        }

        if (isPinned)
        {
            for (si32 n = 0; n < streamSize; ++n)
            {
                for (si32 s = 0; s < maxParam; ++s)
                {
                    cudaHostRegister((void*)src_h[n][s].data(), sizeof(T) * totalSize, cudaHostRegisterDefault);
                }
                cudaHostRegister((void*)out_h[n].data(), sizeof(T) * totalSize, cudaHostRegisterDefault);
            }
        }
    }
    void clearMem()
    {
        if (isStreamBased)
        {
            for (si32 n = 0; n < streamSize; ++n)
            {
                cudaStreamDestroy(streamSet[n]);
            }
        }

        if (isPinned)
        {
            for (si32 n = 0; n < streamSize; ++n)
            {
                for (si32 s = 0; s < maxParam; ++s)
                {
                    cudaHostUnregister((void*)src_h[n][s].data());
                }

                cudaHostUnregister((void*)out_h[n].data());
            }
        }

        for (si32 n = 0; n < streamSize; ++n)
        {
            for (si32 s = 0; s < maxParam; ++s)
            {
                cudaFree(src_d[n][s]);
            }
            cudaFree(out_d[n]);
        }
    }
    void runFunc()
    {
        for (si32 nP = 1; nP <= MAXKERNEL; nP *= 2)
        {
            ret.emplace_back(execFuncTest(nP));
        }

        checkGPUStatus();
    }
    void checkGPUStatus()
    {
        cudaError_t cuda_status = cudaGetLastError();
        if (cudaSuccess != cuda_status)
        {
            printf("Error: cudaStatus fails, %s \n", cudaGetErrorString(cuda_status));
            system("PAUSE");
            exit(0);
        }
    }
    ResultItem execFuncTest(const si32 nParam)
    {
        chrono::system_clock::time_point start, end;

        start = chrono::system_clock::now();

        if (isStreamBased)
        {
            if (!isOnMemory)
            {
                for (si32 n = 0; n < streamSize; ++n)
                {
                    for (si32 s = 0; s < nParam; ++s)
                    {
                        cudaMemcpyAsync(src_d[n][s], src_h[n][s].data(), totalSize * sizeof(T), cudaMemcpyHostToDevice, streamSet[n]);
                    }

                }
            }

            for (si32 n = 0; n < streamSize; ++n)
            {
                if (nParam == 1)
                {
                    for (si32 p = 0; p < maxParam; p += nParam)
                    {
                        testKernel01 << <nBlock, threadSize, 0, streamSet[n] >> > (totalSize, iterMax, out_d[n], src_d[n][p + 0]);
                    }
                }
                if (nParam == 2)
                {
                    for (si32 p = 0; p < maxParam; p += nParam)
                    {
                        testKernel02 << <nBlock, threadSize, 0, streamSet[n] >> > (totalSize, iterMax, out_d[n], src_d[n][p + 0], src_d[n][p + 1]);
                    }
                }
                if (nParam == 4)
                {
                    for (si32 p = 0; p < maxParam; p += nParam)
                    {
                        testKernel04 << <nBlock, threadSize, 0, streamSet[n] >> > (totalSize, iterMax, out_d[n], src_d[n][p + 0], src_d[n][p + 1], src_d[n][p + 2], src_d[n][p + 3]);
                    }
                }
                if (nParam == 8)
                {
                    for (si32 p = 0; p < maxParam; p += nParam)
                    {
                        testKernel08 << <nBlock, threadSize, 0, streamSet[n] >> > (totalSize, iterMax, out_d[n],
                            src_d[n][p + 0], src_d[n][p + 1], src_d[n][p + 2], src_d[n][p + 3], src_d[n][p + 4], src_d[n][p + 5], src_d[n][p + 6], src_d[n][p + 7]
                            );
                    }
                }
                if (nParam == 16)
                {
                    for (si32 p = 0; p < maxParam; p += nParam)
                    {
                        testKernel16 << <nBlock, threadSize, 0, streamSet[n] >> > (totalSize, iterMax, out_d[n],
                            src_d[n][p + 0], src_d[n][p + 1], src_d[n][p + 2], src_d[n][p + 3], src_d[n][p + 4], src_d[n][p + 5], src_d[n][p + 6], src_d[n][p + 7],
                            src_d[n][p + 8], src_d[n][p + 9], src_d[n][p + 10], src_d[n][p + 11], src_d[n][p + 12], src_d[n][p + 13], src_d[n][p + 14], src_d[n][p + 15]
                            );
                    }
                }
                if (nParam == 32)
                {
                    for (si32 p = 0; p < maxParam; p += nParam)
                    {
                        testKernel32 << <nBlock, threadSize, 0, streamSet[n] >> > (totalSize, iterMax, out_d[n],
                            src_d[n][p + 0], src_d[n][p + 1], src_d[n][p + 2], src_d[n][p + 3], src_d[n][p + 4], src_d[n][p + 5], src_d[n][p + 6], src_d[n][p + 7],
                            src_d[n][p + 8], src_d[n][p + 9], src_d[n][p + 10], src_d[n][p + 11], src_d[n][p + 12], src_d[n][p + 13], src_d[n][p + 14], src_d[n][p + 15],
                            src_d[n][p + 16], src_d[n][p + 17], src_d[n][p + 18], src_d[n][p + 19], src_d[n][p + 20], src_d[n][p + 21], src_d[n][p + 22], src_d[n][p + 23],
                            src_d[n][p + 24], src_d[n][p + 25], src_d[n][p + 26], src_d[n][p + 27], src_d[n][p + 28], src_d[n][p + 29], src_d[n][p + 30], src_d[n][p + 31]
                            );
                    }
                }
                if (nParam == 64)
                {
                    for (si32 p = 0; p < maxParam; p += nParam)
                    {
                        testKernel64 << <nBlock, threadSize, 0, streamSet[n] >> > (totalSize, iterMax, out_d[n],
                            src_d[n][p + 0], src_d[n][p + 1], src_d[n][p + 2], src_d[n][p + 3], src_d[n][p + 4], src_d[n][p + 5], src_d[n][p + 6], src_d[n][p + 7],
                            src_d[n][p + 8], src_d[n][p + 9], src_d[n][p + 10], src_d[n][p + 11], src_d[n][p + 12], src_d[n][p + 13], src_d[n][p + 14], src_d[n][p + 15],
                            src_d[n][p + 16], src_d[n][p + 17], src_d[n][p + 18], src_d[n][p + 19], src_d[n][p + 20], src_d[n][p + 21], src_d[n][p + 22], src_d[n][p + 23],
                            src_d[n][p + 24], src_d[n][p + 25], src_d[n][p + 26], src_d[n][p + 27], src_d[n][p + 28], src_d[n][p + 29], src_d[n][p + 30], src_d[n][p + 31],
                            src_d[n][p + 32], src_d[n][p + 33], src_d[n][p + 34], src_d[n][p + 35], src_d[n][p + 36], src_d[n][p + 37], src_d[n][p + 38], src_d[n][p + 39],
                            src_d[n][p + 40], src_d[n][p + 41], src_d[n][p + 42], src_d[n][p + 43], src_d[n][p + 44], src_d[n][p + 45], src_d[n][p + 46], src_d[n][p + 47],
                            src_d[n][p + 48], src_d[n][p + 49], src_d[n][p + 50], src_d[n][p + 51], src_d[n][p + 52], src_d[n][p + 53], src_d[n][p + 54], src_d[n][p + 55],
                            src_d[n][p + 56], src_d[n][p + 57], src_d[n][p + 58], src_d[n][p + 59], src_d[n][p + 60], src_d[n][p + 61], src_d[n][p + 62], src_d[n][p + 63]
                            );
                    }
                }
            }

            if (!isOnMemory)
            {
                for (si32 n = 0; n < streamSize; ++n)
                {
                    cudaMemcpyAsync(out_h[n].data(), out_d[n], totalSize * sizeof(T), cudaMemcpyDeviceToHost, streamSet[n]);
                }
            }
        }
        else
        {
            for (si32 n = 0; n < streamSize; ++n)
            {
                if (!isOnMemory)
                {
                    for (si32 s = 0; s < nParam; ++s)
                    {
                        cudaMemcpy(src_d[n][s], src_h[n][s].data(), totalSize * sizeof(T), cudaMemcpyHostToDevice);
                    }
                }

                if (nParam == 1)
                {
                    for (si32 p = 0; p < maxParam; p += nParam)
                    {
                        testKernel01 << <nBlock, threadSize >> > (totalSize, iterMax, out_d[n], src_d[n][p + 0]);
                    }
                }
                if (nParam == 2)
                {
                    for (si32 p = 0; p < maxParam; p += nParam)
                    {
                        testKernel02 << <nBlock, threadSize >> > (totalSize, iterMax, out_d[n], src_d[n][p + 0], src_d[n][p + 1]);
                    }
                }
                if (nParam == 4)
                {
                    for (si32 p = 0; p < maxParam; p += nParam)
                    {
                        testKernel04 << <nBlock, threadSize >> > (totalSize, iterMax, out_d[n], src_d[n][p + 0], src_d[n][p + 1], src_d[n][p + 2], src_d[n][p + 3]);
                    }
                }
                if (nParam == 8)
                {
                    for (si32 p = 0; p < maxParam; p += nParam)
                    {
                        testKernel08 << <nBlock, threadSize >> > (totalSize, iterMax, out_d[n],
                            src_d[n][p + 0], src_d[n][p + 1], src_d[n][p + 2], src_d[n][p + 3], src_d[n][p + 4], src_d[n][p + 5], src_d[n][p + 6], src_d[n][p + 7]
                            );
                    }
                }
                if (nParam == 16)
                {
                    for (si32 p = 0; p < maxParam; p += nParam)
                    {
                        testKernel16 << <nBlock, threadSize >> > (totalSize, iterMax, out_d[n],
                            src_d[n][p + 0], src_d[n][p + 1], src_d[n][p + 2], src_d[n][p + 3], src_d[n][p + 4], src_d[n][p + 5], src_d[n][p + 6], src_d[n][p + 7],
                            src_d[n][p + 8], src_d[n][p + 9], src_d[n][p + 10], src_d[n][p + 11], src_d[n][p + 12], src_d[n][p + 13], src_d[n][p + 14], src_d[n][p + 15]
                            );
                    }
                }
                if (nParam == 32)
                {
                    for (si32 p = 0; p < maxParam; p += nParam)
                    {
                        testKernel32 << <nBlock, threadSize >> > (totalSize, iterMax, out_d[n],
                            src_d[n][p + 0], src_d[n][p + 1], src_d[n][p + 2], src_d[n][p + 3], src_d[n][p + 4], src_d[n][p + 5], src_d[n][p + 6], src_d[n][p + 7],
                            src_d[n][p + 8], src_d[n][p + 9], src_d[n][p + 10], src_d[n][p + 11], src_d[n][p + 12], src_d[n][p + 13], src_d[n][p + 14], src_d[n][p + 15],
                            src_d[n][p + 16], src_d[n][p + 17], src_d[n][p + 18], src_d[n][p + 19], src_d[n][p + 20], src_d[n][p + 21], src_d[n][p + 22], src_d[n][p + 23],
                            src_d[n][p + 24], src_d[n][p + 25], src_d[n][p + 26], src_d[n][p + 27], src_d[n][p + 28], src_d[n][p + 29], src_d[n][p + 30], src_d[n][p + 31]
                            );
                    }
                }
                if (nParam == 64)
                {
                    for (si32 p = 0; p < maxParam; p += nParam)
                    {
                        testKernel64 << <nBlock, threadSize >> > (totalSize, iterMax, out_d[n],
                            src_d[n][p + 0], src_d[n][p + 1], src_d[n][p + 2], src_d[n][p + 3], src_d[n][p + 4], src_d[n][p + 5], src_d[n][p + 6], src_d[n][p + 7],
                            src_d[n][p + 8], src_d[n][p + 9], src_d[n][p + 10], src_d[n][p + 11], src_d[n][p + 12], src_d[n][p + 13], src_d[n][p + 14], src_d[n][p + 15],
                            src_d[n][p + 16], src_d[n][p + 17], src_d[n][p + 18], src_d[n][p + 19], src_d[n][p + 20], src_d[n][p + 21], src_d[n][p + 22], src_d[n][p + 23],
                            src_d[n][p + 24], src_d[n][p + 25], src_d[n][p + 26], src_d[n][p + 27], src_d[n][p + 28], src_d[n][p + 29], src_d[n][p + 30], src_d[n][p + 31],
                            src_d[n][p + 32], src_d[n][p + 33], src_d[n][p + 34], src_d[n][p + 35], src_d[n][p + 36], src_d[n][p + 37], src_d[n][p + 38], src_d[n][p + 39],
                            src_d[n][p + 40], src_d[n][p + 41], src_d[n][p + 42], src_d[n][p + 43], src_d[n][p + 44], src_d[n][p + 45], src_d[n][p + 46], src_d[n][p + 47],
                            src_d[n][p + 48], src_d[n][p + 49], src_d[n][p + 50], src_d[n][p + 51], src_d[n][p + 52], src_d[n][p + 53], src_d[n][p + 54], src_d[n][p + 55],
                            src_d[n][p + 56], src_d[n][p + 57], src_d[n][p + 58], src_d[n][p + 59], src_d[n][p + 60], src_d[n][p + 61], src_d[n][p + 62], src_d[n][p + 63]
                            );
                    }
                }

                if (!isOnMemory)
                {
                    cudaMemcpy(out_h[n].data(), out_d[n], totalSize * sizeof(T), cudaMemcpyDeviceToHost);
                }
            }
        }

        cudaDeviceSynchronize();
        end = chrono::system_clock::now();

        auto timeElap = chrono::duration_cast<chrono::nanoseconds>(end - start).count() / 10e9;

        return make_tuple(timeElap, timeElap);
    }

public:
    FuncTester(const string& typeName, const si32 _totalSize, const si32 _threadSize, const si32 _streamSize, const si32 _iterMax,
        const bool _isStreamBased, const bool _isPinned, const bool _isOnMemory)
        : totalSize(_totalSize), threadSize(_threadSize),
        streamSize(_streamSize),
        maxParam(MAXKERNEL), iterMax(_iterMax),
        isStreamBased(_isStreamBased), isPinned(_isPinned), isOnMemory(_isOnMemory)
    {
        nBlock = totalSize / threadSize;
        if (threadSize * nBlock < totalSize)
        {
            nBlock++;
        }

        initMem();
        runFunc();
        clearMem();

        cout
            << setw(10) << typeName
            << setw(10) << totalSize
            << setw(10) << nBlock
            << setw(5) << threadSize
            << setw(4) << streamSize
            << setw(5) << iterMax
            << setw(7) << isStreamBased
            << setw(7) << isPinned
            << setw(7) << isOnMemory;

        for (si32 n = 0; n < ret.size(); ++n)
        {
            cout << setw(25) << 1.0 * totalSize * streamSize / get<1>(ret[n]) / 10e9;
        }

        cout << endl;
    }
};

void checkMemInfo(si32 gpuNum)
{
    cudaSetDevice(gpuNum);

    size_t freeByte;
    size_t totalByte;

    cudaError_t cuda_status = cudaMemGetInfo(&freeByte, &totalByte);

    size_t usedByte = totalByte - freeByte;

    cout << "GPU #" << gpuNum << endl;
    cout << "MEM TOTAL : "
        << setw(24) << totalByte / pow(1024.0, 0) << "  B"
        << setw(24) << totalByte / pow(1024.0, 1) << " KB"
        << setw(24) << totalByte / pow(1024.0, 2) << " MB"
        << setw(24) << totalByte / pow(1024.0, 3) << " GB"
        << endl;

    cout << "MEM FREE  : "
        << setw(24) << freeByte / pow(1024.0, 0) << "  B"
        << setw(24) << freeByte / pow(1024.0, 1) << " KB"
        << setw(24) << freeByte / pow(1024.0, 2) << " MB"
        << setw(24) << freeByte / pow(1024.0, 3) << " GB"
        << endl;

    cout << "MEM USED  : "
        << setw(24) << usedByte / pow(1024.0, 0) << "  B"
        << setw(24) << usedByte / pow(1024.0, 1) << " KB"
        << setw(24) << usedByte / pow(1024.0, 2) << " MB"
        << setw(24) << usedByte / pow(1024.0, 3) << " GB"
        << endl;

    cout << endl;

    cudaSetDevice(0);
}
int main()
{
    cout << boolalpha;
    cout << fixed;
    cout << setprecision(12);

    si32 nDevices;
    cudaGetDeviceCount(&nDevices);

    cout << "num Devices = " << nDevices << endl;

    si32 devId = 0;
    cudaSetDevice(devId);

    si32 numSMs;
    si32 warpSize;
    cudaDeviceGetAttribute(&numSMs, cudaDevAttrMultiProcessorCount, devId);
    cudaDeviceGetAttribute(&warpSize, cudaDevAttrWarpSize, devId);

    cout << "numSMs = " << numSMs << endl;
    cout << "warpSz = " << warpSize << endl;

    const si32 powMax = 22;

    for (si32 s = 0; s < 6; ++s)
    {
        const si32 nStream = pow(2, s);
        const si32 totalSize = pow(2, powMax - s);
        
        for (si32 i = 0; i < 14; ++i)
        {
            const si32 iterMax = pow(2, i);

            for (si32 t = 4; t < 11; ++t)
            {
                si32 threadSize = pow(2, t);
                //FuncTester <si16>("si16", totalSize, threadSize, nStream, iterMax, isStreamBased, isPinned, isOnmemory);
                FuncTester <si16>("si16", totalSize, threadSize, nStream, iterMax, true, true, true);
                FuncTester <si16>("si16", totalSize, threadSize, nStream, iterMax, true, true, false);
                FuncTester <si16>("si16", totalSize, threadSize, nStream, iterMax, true, false, true);
                FuncTester <si16>("si16", totalSize, threadSize, nStream, iterMax, true, false, false);
                FuncTester <si16>("si16", totalSize, threadSize, nStream, iterMax, false, true, true);
                FuncTester <si16>("si16", totalSize, threadSize, nStream, iterMax, false, true, false);
                FuncTester <si16>("si16", totalSize, threadSize, nStream, iterMax, false, false, true);
                FuncTester <si16>("si16", totalSize, threadSize, nStream, iterMax, false, false, false);
            }
        }
    }

    for (si32 s = 0; s < 6; ++s)
    {
        const si32 nStream = pow(2, s);
        const si32 totalSize = pow(2, powMax - s);

        for (si32 i = 0; i < 14; ++i)
        {
            const si32 iterMax = pow(2, i);

            for (si32 t = 4; t < 11; ++t)
            {
                si32 threadSize = pow(2, t);
                //FuncTester <si32>("si32", totalSize, threadSize, nStream, iterMax, isStreamBased, isPinned, isOnmemory);
                FuncTester <si32>("si32", totalSize, threadSize, nStream, iterMax, true, true, true);
                FuncTester <si32>("si32", totalSize, threadSize, nStream, iterMax, true, true, false);
                FuncTester <si32>("si32", totalSize, threadSize, nStream, iterMax, true, false, true);
                FuncTester <si32>("si32", totalSize, threadSize, nStream, iterMax, true, false, false);
                FuncTester <si32>("si32", totalSize, threadSize, nStream, iterMax, false, true, true);
                FuncTester <si32>("si32", totalSize, threadSize, nStream, iterMax, false, true, false);
                FuncTester <si32>("si32", totalSize, threadSize, nStream, iterMax, false, false, true);
                FuncTester <si32>("si32", totalSize, threadSize, nStream, iterMax, false, false, false);
            }
        }
    }

    for (si32 s = 0; s < 6; ++s)
    {
        const si32 nStream = pow(2, s);
        const si32 totalSize = pow(2, powMax - s);

        for (si32 i = 0; i < 14; ++i)
        {
            const si32 iterMax = pow(2, i);

            for (si32 t = 4; t < 11; ++t)
            {
                si32 threadSize = pow(2, t);
                //FuncTester <si64>("si64", totalSize, threadSize, nStream, iterMax, isStreamBased, isPinned, isOnmemory);
                FuncTester <si64>("si64", totalSize, threadSize, nStream, iterMax, true, true, true);
                FuncTester <si64>("si64", totalSize, threadSize, nStream, iterMax, true, true, false);
                FuncTester <si64>("si64", totalSize, threadSize, nStream, iterMax, true, false, true);
                FuncTester <si64>("si64", totalSize, threadSize, nStream, iterMax, true, false, false);
                FuncTester <si64>("si64", totalSize, threadSize, nStream, iterMax, false, true, true);
                FuncTester <si64>("si64", totalSize, threadSize, nStream, iterMax, false, true, false);
                FuncTester <si64>("si64", totalSize, threadSize, nStream, iterMax, false, false, true);
                FuncTester <si64>("si64", totalSize, threadSize, nStream, iterMax, false, false, false);
            }
        }
    }

    for (si32 s = 0; s < 6; ++s)
    {
        const si32 nStream = pow(2, s);
        const si32 totalSize = pow(2, powMax - s);

        for (si32 i = 0; i < 14; ++i)
        {
            const si32 iterMax = pow(2, i);

            for (si32 t = 4; t < 11; ++t)
            {
                si32 threadSize = pow(2, t);
                //FuncTester <fl32>("fl32", totalSize, threadSize, nStream, iterMax, isStreamBased, isPinned, isOnmemory);
                FuncTester <fl32>("fl32", totalSize, threadSize, nStream, iterMax, true, true, true);
                FuncTester <fl32>("fl32", totalSize, threadSize, nStream, iterMax, true, true, false);
                FuncTester <fl32>("fl32", totalSize, threadSize, nStream, iterMax, true, false, true);
                FuncTester <fl32>("fl32", totalSize, threadSize, nStream, iterMax, true, false, false);
                FuncTester <fl32>("fl32", totalSize, threadSize, nStream, iterMax, false, true, true);
                FuncTester <fl32>("fl32", totalSize, threadSize, nStream, iterMax, false, true, false);
                FuncTester <fl32>("fl32", totalSize, threadSize, nStream, iterMax, false, false, true);
                FuncTester <fl32>("fl32", totalSize, threadSize, nStream, iterMax, false, false, false);
            }
        }
    }

    for (si32 s = 0; s < 6; ++s)
    {
        const si32 nStream = pow(2, s);
        const si32 totalSize = pow(2, powMax - s);

        for (si32 i = 0; i < 14; ++i)
        {
            const si32 iterMax = pow(2, i);

            for (si32 t = 4; t < 11; ++t)
            {
                si32 threadSize = pow(2, t);
                //FuncTester <fl64>("fl64", totalSize, threadSize, nStream, iterMax, isStreamBased, isPinned, isOnmemory);
                FuncTester <fl64>("fl64", totalSize, threadSize, nStream, iterMax, true, true, true);
                FuncTester <fl64>("fl64", totalSize, threadSize, nStream, iterMax, true, true, false);
                FuncTester <fl64>("fl64", totalSize, threadSize, nStream, iterMax, true, false, true);
                FuncTester <fl64>("fl64", totalSize, threadSize, nStream, iterMax, true, false, false);
                FuncTester <fl64>("fl64", totalSize, threadSize, nStream, iterMax, false, true, true);
                FuncTester <fl64>("fl64", totalSize, threadSize, nStream, iterMax, false, true, false);
                FuncTester <fl64>("fl64", totalSize, threadSize, nStream, iterMax, false, false, true);
                FuncTester <fl64>("fl64", totalSize, threadSize, nStream, iterMax, false, false, false);
            }
        }
    }
}