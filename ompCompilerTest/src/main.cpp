#include "baseHeader.h"

int main()
{		
	si32 test = 16 * 1 + 8 * 1 + 4 * 1 + 2 * 1 + 1 * 1;

	if (test & 1)
	{
		si32 testLen = 10000000;
		si32 iterMax = 50;
		si32 nThread = 1;
		si32 nStep = 4;

		for (si32 nC = 0; nC < nStep; ++nC)
		{
			si32 nCore = pow(2, nC);

			auto res = baseTest(nCore, nThread, testLen, iterMax);

			cout << setw(5) << nCore
				<< setw(20) << get<0>(res)
				<< setw(20) << get<1>(res) << endl;
		}
	}

	if (test & 2)
	{
		nestedTest(4);
	}

	if (test & 4)
	{
		collapseTest(4);
	}

	if (test & 8)
	{
		uniqueJobTest(8, 10);
	}

	if (test & 16)
	{
		taskTest(2);
	}
}