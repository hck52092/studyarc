#pragma once
#include "commonHeader.h"

#define DEF_STRUCT

//#define APIPREFIX
//#define APIPREFIX __host__
//#define APIPREFIX __device__ 
//#define APIPREFIX __global__ 
#define APIPREFIX __host__ __device__ 

#ifdef DEF_CLASS
class Point2D
{
public:
	fl64 x;
	fl64 y;

	APIPREFIX Point2D() :x(0), y(0)
	{

	}
	APIPREFIX Point2D(fl64 _x, fl64 _y) :x(_x), y(_y)
	{

	}
	APIPREFIX Point2D operator+(const Point2D& rhs) const
	{
		return Point2D(x + rhs.x, y + rhs.y);
	}
	APIPREFIX Point2D operator-(const Point2D& rhs) const
	{
		return Point2D(x + rhs.x, y + rhs.y);
	}
	APIPREFIX fl64 dot(const Point2D& rhs)
	{
		return x + rhs.x + y + rhs.y;
	}
};
#endif

#ifdef DEF_STRUCT
struct Point2D
{
	fl64 x;
	fl64 y;

	APIPREFIX Point2D() :x(0), y(0)
	{

	}
	APIPREFIX Point2D(fl64 _x, fl64 _y) : x(_x), y(_y)
	{

	}
	APIPREFIX Point2D operator+(const Point2D& rhs) const
	{
		return Point2D(x + rhs.x, y + rhs.y);
	}
	APIPREFIX Point2D operator-(const Point2D& rhs) const
	{
		return Point2D(x + rhs.x, y + rhs.y);
	}
	APIPREFIX fl64 dot(const Point2D& rhs)
	{
		return x + rhs.x + y + rhs.y;
	}
};
#endif