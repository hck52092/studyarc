#!/bin/bash

echo -n "OBJS := " > Makefile

find ./src -name \*.cpp | sed 's/cpp$/o \\/' >> Makefile
find ./src -name \*.cc | sed 's/cc$/o \\/' >> Makefile
>> Makefile
echo >> Makefile

(
cat << EOF 
CXXFLAGS := -O2 -DNDEBUG -std=c++1z \$(CXXFLAGS) -fopenmp
LDFLAGS := 

all: arrBlockTest
	
arrBlockTest: \$(OBJS)
	g++ \$(OBJS) \$(CXXFLAGS) \$(LDFLAGS) -o arrBlockTest

clean:
	rm -fv \$(OBJS)
EOF
) >> Makefile

make clean
make -j$(nproc)
