#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <array>
#include <tuple>
#include <set>
#include <map>
#include <algorithm>
#include <chrono>

using namespace std;

using ui08 = unsigned char;
using si16 = short;
using si32 = int;
using ui32 = unsigned int;
using si64 = long long;
using fl32 = float;
using fl64 = double;

void imwrite(const string& fn, const vector<ui08>& img,const si32 x,const si32 y);

si32 getBlockSize(const si32 totalSize, const si32 stepSize)
{
    return (totalSize + stepSize - 1) / stepSize;
}

__device__ ui32 get_smid(void) {

    ui32 ret;

    asm("mov.u32 %0, %smid;" : "=r"(ret));

    return ret;

}

__global__ void imgKernel_SM(ui08* img)
{
    const si32 x = threadIdx.x + blockIdx.x * blockDim.x;
    const si32 y = threadIdx.y + blockIdx.y * blockDim.y;
    const si32 z = threadIdx.z + blockIdx.z * blockDim.z;
    const si32 dx = gridDim.x * blockDim.x;

    img[x + y * dx] = get_smid();
}

__global__ void imgKernel_BL(ui08* img)
{
    const si32 x = threadIdx.x + blockIdx.x * blockDim.x;
    const si32 y = threadIdx.y + blockIdx.y * blockDim.y;
    const si32 z = threadIdx.z + blockIdx.z * blockDim.z;
    const si32 dx = gridDim.x * blockDim.x;

    img[x + y * dx] = gridDim.z * blockIdx.z;
}

__global__ void imgKernel_TH(ui08* img)
{
    const si32 x = threadIdx.x + blockIdx.x * blockDim.x;
    const si32 y = threadIdx.y + blockIdx.y * blockDim.y;
    const si32 z = threadIdx.z + blockIdx.z * blockDim.z;
    const si32 dx = gridDim.x * blockDim.x;

    img[x + y * dx] = gridDim.z * threadIdx.z;
}

int main(int argc, char** argv)
{
    const si32 X = 2048;
    const si32 Y = 2048;
    const si32 Z = 256;
    
    vector<ui08> img_h(X * Y);
    ui08* img_d;
    cudaMalloc(&img_d, sizeof(ui08) * X * Y);
   
    cout << "SM_DIV" << endl;
    for (si32 s = 0; s < 4; ++s)
    {
        si32 tMax = pow(2, s);

        dim3 block;
        block.x = tMax;
        block.y = tMax;
        block.z = tMax;

        dim3 grid;
        grid.x = X / tMax;
        grid.y = Y / tMax;
        grid.z = Z / tMax;

        fill(img_h.begin(), img_h.end(), 0);
        cudaMemcpy(img_d, img_h.data(), sizeof(ui08) * X * Y, cudaMemcpyHostToDevice);

        imgKernel_SM << <grid, block >> > (img_d);

        cudaMemcpy(img_h.data(), img_d, sizeof(ui08) * X * Y, cudaMemcpyDeviceToHost);
        cout << cudaGetErrorName(cudaGetLastError()) << endl;

        map<ui08, si32> hist;
        si32 total = 0;
        for (si32 s = 0; s < X * Y; ++s)
        {
            hist[img_h[s]]++;
        }
        for (const auto& item : hist)
        {
            cout << setw(10) << si32(item.first);
        }
        cout << endl;
        for (const auto& item : hist)
        {
            cout << setw(10) << si32(item.second);
            total += si32(item.second);
        }
        cout << endl;
        cout << total << endl;

        char fn[100];
        sprintf(fn, "SM_%04d.png", s);

        imwrite(fn, img_h, X, Y);
    }

    cout << "BL_DIV" << endl;
    for (si32 s = 0; s < 4; ++s)
    {
        si32 tMax = pow(2, s);

        dim3 block;
        block.x = tMax;
        block.y = tMax;
        block.z = tMax;

        dim3 grid;
        grid.x = X / tMax;
        grid.y = Y / tMax;
        grid.z = Z / tMax;
        
        fill(img_h.begin(), img_h.end(), 0);
        cudaMemcpy(img_d, img_h.data(), sizeof(ui08) * X * Y, cudaMemcpyHostToDevice);
        
        imgKernel_BL << <grid, block >> > (img_d);
        //imgKernel_TH << <grid, block >> > (img_d);

        cudaMemcpy(img_h.data(), img_d, sizeof(ui08) * X * Y, cudaMemcpyDeviceToHost);
        cout << cudaGetErrorName(cudaGetLastError()) << endl;

        map<ui08, si32> hist;
        si32 total = 0;
        for (si32 s = 0; s < X*Y; ++s)
        {
            hist[img_h[s]]++;
        }
        for (const auto& item : hist)
        {
            cout << setw(10) << si32(item.first);
        }
        cout << endl;
        for (const auto& item : hist)
        {
            cout << setw(10) << si32(item.second);
            total += si32(item.second);
        }
        cout << endl;
        cout << total << endl;

        char fn[100];
        sprintf(fn, "BL_%04d.png", s);

        imwrite(fn, img_h, X, Y);
    }

    cout << "TH_DIV" << endl;
    for (si32 s = 0; s < 4; ++s)
    {
        si32 tMax = pow(2, s);

        dim3 block;
        block.x = tMax;
        block.y = tMax;
        block.z = tMax;

        dim3 grid;
        grid.x = X / tMax;
        grid.y = Y / tMax;
        grid.z = Z / tMax;

        fill(img_h.begin(), img_h.end(), 0);
        cudaMemcpy(img_d, img_h.data(), sizeof(ui08) * X * Y, cudaMemcpyHostToDevice);

        imgKernel_TH << <grid, block >> > (img_d);

        cudaMemcpy(img_h.data(), img_d, sizeof(ui08) * X * Y, cudaMemcpyDeviceToHost);
        cout << cudaGetErrorName(cudaGetLastError()) << endl;

        map<ui08, si32> hist;
        si32 total = 0;
        for (si32 s = 0; s < X * Y; ++s)
        {
            hist[img_h[s]]++;
        }
        for (const auto& item : hist)
        {
            cout << setw(10) << si32(item.first);
        }
        cout << endl;
        for (const auto& item : hist)
        {
            cout << setw(10) << si32(item.second);
            total += si32(item.second);
        }
        cout << endl;
        cout << total << endl;

        char fn[100];
        sprintf(fn, "TH_%04d.png", s);

        imwrite(fn, img_h, X, Y);
    }

    cudaFree(img_d);
}