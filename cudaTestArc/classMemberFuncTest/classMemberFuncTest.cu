#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "classDef.h"

__global__ void testKernel(Point2D* dst, const Point2D* src, const Point2D a,const si32 nSize)
{
	si32 i = blockIdx.x * blockDim.x + threadIdx.x;

	if (i < nSize)
	{
		dst[i] = src[i] + a;
	}
}

int main()
{
	si32 nSize = 1024;
	vector<Point2D> src_h(nSize);
	vector<Point2D> dst_h(nSize);

	Point2D* src_d;
	Point2D* dst_d;

	for (si32 s = 0; s < nSize; ++s)
	{
		src_h[s].x = s;
		src_h[s].y = s * s;
	}
	
	cudaMalloc(&src_d, sizeof(Point2D) * nSize);
	cudaMalloc(&dst_d, sizeof(Point2D) * nSize);

	cudaMemcpy(src_d, src_h.data(), sizeof(Point2D) * nSize, cudaMemcpyHostToDevice);

	si32 nThread = 256;
	si32 nBlock = (nSize + nThread - 1) / nThread;

	testKernel << <nBlock, nThread >> > (dst_d, src_d, Point2D(1, 1), nSize);

	cudaMemcpy(dst_h.data(), dst_d, sizeof(Point2D) * nSize, cudaMemcpyDeviceToHost);

	cout << setprecision(15);
	for (si32 s = 0; s < nSize; ++s)
	{
		cout << setw(10) << s << setw(20) << dst_h[s].x << setw(20) << dst_h[s].y << endl;
	}
}
