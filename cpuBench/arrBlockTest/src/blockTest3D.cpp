#include "func.h"
void blockTest3D(const vector<si32>& nCoreList)
{
    cout << "blockTest3D" << endl;

    const si32 minBlock = 1;
    const si32 maxBlock = 256;

    Arr3D<fl64> gridA;
    Arr3D<fl64> gridB;
    vector<decltype(&copyOp)> opArr;
    vector<string> opStrArr;
    init3D(gridA, gridB, opArr, opStrArr);

    for (si32 tt = 0; tt < opArr.size(); ++tt)
    {
        cout << opStrArr[tt] << endl;

        {
            cout << setw(10) << "#core";
            for (si32 blockSize = minBlock; blockSize <= maxBlock; blockSize *= 2)
            {
                cout << setw(15) << "block" + to_string(blockSize);
            }
            cout << endl;
        }

        auto& op = opArr[tt];
        for (si32 nC = 0; nC < nCoreList.size(); ++nC)
        {
            const si32 nCore = nCoreList[nC];
            cout << setw(10) << nCore;

            for (si32 blockSize = minBlock; blockSize <= maxBlock; blockSize *= 2)
            {
                chrono::system_clock::time_point start, end;
                chrono::nanoseconds nanoSec;
                start = chrono::system_clock::now();

                const si32 divX = gridA.nX() / blockSize;
                const si32 divY = gridA.nY() / blockSize;
                const si32 divZ = gridA.nZ() / blockSize;
                const si32 nTotal = divX * divY * divZ;

                #pragma omp parallel for schedule(guided) num_threads(nCore)
                for (si32 s = 0; s < nTotal; ++s)
                {
                    si32 sTmp = s;
                    const si32 i = (sTmp % divX) * blockSize; sTmp /= divX;
                    const si32 j = (sTmp % divY) * blockSize; sTmp /= divY;
                    const si32 k = sTmp * blockSize;

                    for (si32 z = k; z < k + blockSize; ++z)
                    {
                        for (si32 y = j; y < j + blockSize; ++y)
                        {
                            for (si32 x = i; x < i + blockSize; ++x)
                            {
                                op(gridA, gridB, x, y, z);
                            }
                        }
                    }
                }

                end = chrono::system_clock::now();
                nanoSec = chrono::duration_cast<chrono::nanoseconds>(end - start);
                const fl64 dt = nanoSec.count() / 1000000000.0;
                cout << setw(15) << dt;
            }
            cout << endl;
        }

        cout << endl;
    }

}