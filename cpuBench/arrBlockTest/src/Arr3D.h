#pragma once
#include "main_dataType.h"
using namespace std;

template<typename T>
class Arr3D
{
	ui64 iX;
	ui64 iY;
	ui64 iZ;
	ui08 status;

public:
	vector<T> byteData;

	Arr3D()
	{
		resize(1, 1, 1);
	}
	Arr3D(ui64 X, ui64 Y, ui64 Z)
	{
		resize(X, Y, Z);
	}
	void setSize(ui64 X, ui64 Y, ui64 Z)
	{
		iX = X;
		iY = Y;
		iZ = Z;
	}
	ui64 nX() const
	{
		return iX;
	}
	ui64 nY() const
	{
		return iY;
	}
	ui64 nZ() const
	{
		return iZ;
	}

	T& at(ui64 i)
	{
		return at(i, 0, 0);
	}
	T& at(ui64 i, ui64 j)
	{
		return at(i, j, 0);
	}
	T& at(ui64 i, ui64 j, ui64 k)
	{
		//return byteData[(j * iX + i) * iZ + k];
		return byteData[i + iX * (j + k * iY)];
	}
	T at(ui64 i) const
	{
		return at(i, 0, 0);
	}
	T at(ui64 i, ui64 j) const
	{
		return at(i, j, 0);
	}
	T at(ui64 i, ui64 j, ui64 k) const
	{
		//return byteData[(j * iX + i) * iZ + k];
		return byteData[i + iX * (j + k * iY)];
	}

	T& operator()(ui64 i)
	{
		return at(i);
	}
	T& operator()(ui64 i, ui64 j)
	{
		return at(i, j);
	}
	T& operator()(ui64 i, ui64 j, ui64 k)
	{
		return at(i, j, k);
	}

	T operator()(ui64 i) const
	{
		return at(i);
	}
	T operator()(ui64 i, ui64 j) const
	{
		return at(i, j);
	}
	T operator()(ui64 i, ui64 j, ui64 k) const
	{
		return at(i, j, k);
	}

	void resize(ui64 X, ui64 Y, ui64 Z)
	{
		setSize(X, Y, Z);
		byteData.resize(X * Y * Z);
		byteData.shrink_to_fit();
	}
};