#include "DataSet3D.h"
#include "device_launch_parameters.h"

__device__ fl64 genRhoSoA_GPU(const Cell3D& cell)
{
	fl64 rho = 0;

	rho += cell.f[0];
	rho += cell.f[1];
	rho += cell.f[2];
	rho += cell.f[3];
	rho += cell.f[4];
	rho += cell.f[5];
	rho += cell.f[6];
	rho += cell.f[7];
	rho += cell.f[8];
	rho += cell.f[9];
	rho += cell.f[10];
	rho += cell.f[10];
	rho += cell.f[11];
	rho += cell.f[12];
	rho += cell.f[13];
	rho += cell.f[14];
	rho += cell.f[15];
	rho += cell.f[16];
	rho += cell.f[17];

	return rho;
}
__device__ fl64 genUSoA_GPU(const Cell3D& cell, const fl64 rho)
{
	fl64 u = 0;
	u += (cell.f[1] + cell.f[7] + cell.f[9] + cell.f[11] + cell.f[3]);
	u -= (cell.f[2] + cell.f[8] + cell.f[10] + cell.f[12] + cell.f[14]);
	u /= rho;
	return u;
}
__device__ fl64 genVSoA_GPU(const Cell3D& cell, const fl64 rho)
{
	fl64 v = 0;
	v += (cell.f[3] + cell.f[7] + cell.f[8] + cell.f[15] + cell.f[17]);
	v -= (cell.f[4] + cell.f[9] + cell.f[10] + cell.f[16] + cell.f[18]);
	v /= rho;
	return v;
}
__device__ fl64 genWSoA_GPU(const Cell3D& cell, const fl64 rho)
{
	fl64 w = 0;
	w += (cell.f[5] + cell.f[11] + cell.f[12] + cell.f[15] + cell.f[16]);
	w -= (cell.f[6] + cell.f[13] + cell.f[14] + cell.f[17] + cell.f[18]);
	w /= rho;
	return w;
}

__global__ void collision3D_SoA_kernel(
	fl64* fValArr0,
	fl64* fValArr1, fl64* fValArr2, fl64* fValArr3,
	fl64* fValArr4, fl64* fValArr5, fl64* fValArr6,
	fl64* fValArr7, fl64* fValArr8, fl64* fValArr9, fl64* fValArr10,
	fl64* fValArr11, fl64* fValArr12, fl64* fValArr13, fl64* fValArr14,
	fl64* fValArr15, fl64* fValArr16, fl64* fValArr17, fl64* fValArr18,
	const fl64 s_v, const si32 nNode)
{
	const fl64  w_rho[3] = {
				12.0 / 36,
				2.0 / 36,
				1.0 / 36 };

	const si32 direcCategory[19] = { 0,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2 };

	const fl64 direcC[19][3] = {
		{0.0,0.0,0.0},
		{1.0,0.0,0.0 },
		{-1.0, 0.0,0.0},
		{0.0,1.0,0.0},
		{0.0,-1.0,0.0 },
		{0.0, 0.0,1.0},
		{0.0, 0.0,-1.0},

		{1.0, 1.0,0.0 },
		{-1.0,1.0,0.0 },
		{1.0,-1.0,0.0 },
		{-1.0,-1.0,0.0 },

		{1.0,0.0,1.0 },
		{-1.0,0.0,1.0 },
		{1.0,0.0,-1.0 },
		{-1.0,0.0,-1.0 },

		{0.0,1.0,1.0 },
		{0.0,-1.0,1.0 },
		{0.0,1.0,-1.0 },
		{0.0,-1.0,-1.0 }
	};

	const fl64 csSq = 1.0 / 3;

	si32 indCur = blockDim.x * blockIdx.x + threadIdx.x;

	if (indCur < nNode)
	{
		Cell3D cellCur;
		cellCur.f[0] = fValArr0[indCur];
		cellCur.f[1] = fValArr1[indCur];
		cellCur.f[2] = fValArr2[indCur];
		cellCur.f[3] = fValArr3[indCur];
		cellCur.f[4] = fValArr4[indCur];
		cellCur.f[5] = fValArr5[indCur];
		cellCur.f[6] = fValArr6[indCur];
		cellCur.f[7] = fValArr7[indCur];
		cellCur.f[8] = fValArr8[indCur];
		cellCur.f[9] = fValArr9[indCur];
		cellCur.f[10] = fValArr10[indCur];
		cellCur.f[11] = fValArr11[indCur];
		cellCur.f[12] = fValArr12[indCur];
		cellCur.f[13] = fValArr13[indCur];
		cellCur.f[14] = fValArr14[indCur];
		cellCur.f[15] = fValArr15[indCur];
		cellCur.f[16] = fValArr16[indCur];
		cellCur.f[17] = fValArr17[indCur];
		cellCur.f[18] = fValArr18[indCur];
		
		const si32 indOffset = indCur * 19;

		const fl64 rho = genRhoSoA_GPU(cellCur);

		fl64 vel[3] = { genUSoA_GPU(cellCur,rho), genVSoA_GPU(cellCur,rho), genWSoA_GPU(cellCur,rho) };

		const fl64 rhoRecipocal = 1.0 / rho;

		fl64 u_dot_u = vel[0] * vel[0] + vel[1] * vel[1] + vel[2] * vel[2];

		for (si32 i = 0; i < 19; ++i)
		{
			const fl64 wTemp = w_rho[direcCategory[i]];
			const fl64 c[3] = { direcC[i][0],direcC[i][1],direcC[i][2] };
			const fl64 dotVal = (c[0] * vel[0] + c[1] * vel[1] + c[2] * vel[2]);

			const fl64 eq = wTemp * rho * (1. + 3.0 * dotVal + 4.5 * (dotVal * dotVal) - 1.5 * u_dot_u);

			cellCur.f[i] = (1 - s_v) * eq + s_v * cellCur.f[i];
		}

		fValArr0[indCur] = cellCur.f[0];
		fValArr1[indCur] = cellCur.f[1];
		fValArr2[indCur] = cellCur.f[2];
		fValArr3[indCur] = cellCur.f[3];
		fValArr4[indCur] = cellCur.f[4];
		fValArr5[indCur] = cellCur.f[5];
		fValArr6[indCur] = cellCur.f[6];
		fValArr7[indCur] = cellCur.f[7];
		fValArr8[indCur] = cellCur.f[8];
		fValArr9[indCur] = cellCur.f[9];
		fValArr10[indCur] = cellCur.f[10];
		fValArr11[indCur] = cellCur.f[11];
		fValArr12[indCur] = cellCur.f[12];
		fValArr13[indCur] = cellCur.f[13];
		fValArr14[indCur] = cellCur.f[14];
		fValArr15[indCur] = cellCur.f[15];
		fValArr16[indCur] = cellCur.f[16];
		fValArr17[indCur] = cellCur.f[17];
		fValArr18[indCur] = cellCur.f[18];
	}
}

void DataSet3D::collision3D_SoA_GPU()
{
	collision3D_SoA_kernel << <blocksPerGrid, threadsPerBlock >> >
		(dataGPU_SoA[0],
			dataGPU_SoA[1], dataGPU_SoA[2], dataGPU_SoA[3],
			dataGPU_SoA[4], dataGPU_SoA[5], dataGPU_SoA[6],
			dataGPU_SoA[7], dataGPU_SoA[8], dataGPU_SoA[9], dataGPU_SoA[10],
			dataGPU_SoA[11], dataGPU_SoA[12], dataGPU_SoA[13], dataGPU_SoA[14],
			dataGPU_SoA[15], dataGPU_SoA[16], dataGPU_SoA[17], dataGPU_SoA[18],
			s_v, nNode);
}