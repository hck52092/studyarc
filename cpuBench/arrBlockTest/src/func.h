#pragma once
#include "Arr3D.h"
#include <omp.h>

#ifdef __clang__
#pragma comment(lib,"libiomp5md.lib")
#pragma comment(lib,"libomp.lib")
#else

#endif

void copyOp(const Arr3D<fl64>& src, Arr3D<fl64>& dst, const si32 i, const si32 j, const si32 k);
void swapIJ(const Arr3D<fl64>& src, Arr3D<fl64>& dst, const si32 i, const si32 j, const si32 k);
void swapJK(const Arr3D<fl64>& src, Arr3D<fl64>& dst, const si32 i, const si32 j, const si32 k);
void swapKI(const Arr3D<fl64>& src, Arr3D<fl64>& dst, const si32 i, const si32 j, const si32 k);
void compOp(const Arr3D<fl64>& src, Arr3D<fl64>& dst, const si32 i, const si32 j, const si32 k);

void init2D(Arr3D<fl64>& gridA, Arr3D<fl64>& gridB, vector<decltype(&copyOp)>& opArr, vector<string>& opStrArr);
void init3D(Arr3D<fl64>& gridA, Arr3D<fl64>& gridB, vector<decltype(&copyOp)>& opArr, vector<string>& opStrArr);