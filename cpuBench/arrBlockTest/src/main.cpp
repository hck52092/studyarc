#include "main_dataType.h"

void unrollTest2D(const vector<si32>& nCoreList);
void unrollTest3D(const vector<si32>& nCoreList);
void orderingTest2D(const vector<si32>& nCoreList);
void orderingTest3D(const vector<si32>& nCoreList);
void schedulingTest2D(const vector<si32>& nCoreList);
void schedulingTest3D(const vector<si32>& nCoreList);
void chunkTest2D(const vector<si32>& nCoreList);
void chunkTest3D(const vector<si32>& nCoreList);
void blockTest2D(const vector<si32>& nCoreList);
void blockTest3D(const vector<si32>& nCoreList);
void collapseTest2D(const vector<si32>& nCoreList);
void collapseTest3D(const vector<si32>& nCoreList);

int main(int argc, char** argv)
{		
	bool dimType[2] = { true,true };
	bool runType[] = { true, true, true, true, true, true };
	const vector<si32> nCoreList{ 1,2,3,4,6,8,12,16,24,32,64 };

	if (argc > 1)
	{
		si32 tmp;
		tmp = atoi(argv[1]);
		dimType[0] = tmp % 2; tmp /= 2;
		dimType[1] = tmp % 2; tmp /= 2;

		tmp = atoi(argv[2]);
		runType[0] = tmp % 2; tmp /= 2;
		runType[1] = tmp % 2; tmp /= 2;
		runType[2] = tmp % 2; tmp /= 2;
		runType[3] = tmp % 2; tmp /= 2;
		runType[4] = tmp % 2; tmp /= 2;
		runType[5] = tmp % 2; tmp /= 2;
	}
	
	if (runType[0])
	{
		cout << "unrollTest" << endl;
		if (dimType[0])unrollTest2D(nCoreList);
		if (dimType[1])unrollTest3D(nCoreList);
	}	
	
	if (runType[1])
	{
		cout << "orderingTest" << endl;
		if (dimType[0])orderingTest2D(nCoreList);
		if (dimType[1])orderingTest3D(nCoreList);
	}
	
	if (runType[2])
	{
		cout << "schedulingTest" << endl;
		if (dimType[0])schedulingTest2D(nCoreList);
		if (dimType[1])schedulingTest3D(nCoreList);
	}

	if (runType[3])
	{
		cout << "chunkTest" << endl;
		if (dimType[0])chunkTest2D(nCoreList);
		if (dimType[1])chunkTest3D(nCoreList);
	}

	if (runType[4])
	{
		cout << "blockTest" << endl;
		if (dimType[0])blockTest2D(nCoreList);
		if (dimType[1])blockTest3D(nCoreList);
	}

	if (runType[5])
	{
		cout << "collapseTest" << endl;
		if (dimType[0])collapseTest2D(nCoreList);
		if (dimType[1])collapseTest3D(nCoreList);
	}
}