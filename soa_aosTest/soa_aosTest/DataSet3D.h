#pragma once
#include "commonHeader.h"
class DataSet3D
{
	const si32 nNode;
	const si32 threadsPerBlock;
	const si32 blocksPerGrid = nNode / threadsPerBlock;

	const fl64 s_v;

	const si32 nCore;

	vector<Cell3D> dataCPU_AoS;
	Cell3D* dataGPU_AoS;

	vector<vector<fl64>> dataCPU_SoA;
	vector<fl64*> dataGPU_SoA;

	vector<fl64> buffer_CPU;
	fl64* buffer_GPU;

	si32 genDirecOffset(const si32 direc);
	bool isValidInd(const si32 ind);

public:
	DataSet3D(const si32 _nNode, const si32 _threadsPerBlock, const fl64 _s_v, const si32 _nCore);
	~DataSet3D();

	void collision3D_AoS_CPU();
	void collision3D_SoA_CPU();

	void collision3D_AoS_GPU();
	void collision3D_SoA_GPU();

	void streaming3D_AoS_CPU();
	void streaming3D_SoA_CPU();

	void streaming3D_AoS_GPU();
	void streaming3D_SoA_GPU();
};