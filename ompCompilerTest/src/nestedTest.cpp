#include "baseHeader.h"
void nestedTestInnerProc(const si32 nCore, const si32 padding,const si32 testLen)
{
	const si32 nThread = nCore * nCore;
	vector<vector<si32>> sumArr(nCore * padding);
	for (si32 th = 0; th < nCore; ++th)
	{
		sumArr[th * padding].resize(nThread * padding);
	}

	vector<si32> ind1Arr(nThread * padding, 0);
	vector<si32> ind2Arr(nThread * padding, 0);

	#pragma omp parallel for num_threads(nCore)
	for (si32 s = 0; s < nCore; ++s)
	{
		si32 th1 = omp_get_thread_num();

		#pragma omp parallel for num_threads(nThread)
		for (si32 t = 0; t < testLen; ++t)
		{
			si32 th2 = omp_get_thread_num();
			sumArr[th1 * padding][th2 * padding]++;
			ind1Arr[th1 * padding] = true;
			ind2Arr[th2 * padding] = true;
		}
	}

	for (si32 th = 0; th < nCore; ++th)
	{
		cout << setw(5) << th
			<< setw(5) << ind1Arr[th * padding]
			<< setw(5) << ind2Arr[th * padding] << endl;

		for (si32 thN = 0; thN < nThread; ++thN)
		{
			cout << setw(20) << thN << setw(20) << sumArr[th * padding][thN * padding] << endl;
		}
	}
}
void nestedTest(const si32 nCore)
{
	const si32 padding = 10;
	const si32 testLen = 100000;
	si32 nested = omp_get_nested();

	cout << "nested Off" << endl;
	omp_set_nested(0);

	nestedTestInnerProc(nCore, padding, testLen);

	cout << "nested On" << endl;
	omp_set_nested(1);

	nestedTestInnerProc(nCore, padding, testLen);
		
	omp_set_nested(nested);
}