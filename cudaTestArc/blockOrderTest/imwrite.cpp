#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"
#include <vector>
#include <string>
using namespace std;

using ui08 = unsigned char;
using si16 = short;
using si32 = int;
using si64 = long long;
using fl32 = float;
using fl64 = double;

void imwrite(const string& fn, const vector<ui08>& img, const si32 X, const si32 Y)
{
	stbi_write_png(fn.c_str(), X, Y, 1, img.data(), X);
}